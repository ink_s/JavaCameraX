package com.ink.javacamerax;

import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import com.google.gson.Gson;
import com.ink.chart.entity.ChartData;
import com.ink.chart.view.XBarChartView;
import com.ink.chart.view.XLineChartView;

public class MainActivity7 extends AppCompatActivity {


    XBarChartView chart1;
    XLineChartView chart2;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main7);
        chart1 = findViewById(R.id.chart_1);
        chart2 = findViewById(R.id.chart_2);

        ChartData testData = getTestData(
                new String[]{"2020", "2021", "2022", "2023", "2024"},
                new String[]{"目", "科", "属", "种"},
                new float[]{55, 55, 55, 55, 55},
                new float[]{330, 330, 330, 330, 330},
                new float[]{1320, 1323, 1327, 1333, 1333},
                new float[]{2173, 2178, 2184, 2192, 2192});

        L.ee(testData);


        chart1.setData(testData, XBarChartView.TagNumberShow.NUMBER, XBarChartView.BarShow.HORIZONTAL);
        chart1.upData();
        chart2.setData(testData, XLineChartView.TagNumberShow.NUMBER);
        chart2.upData();


        String json = "{\"total\":0.0,\"xData\":[\"00日\",\"01日\",\"02日\",\"03日\",\"04日\",\"05日\",\"06日\",\"07日\",\"08日\",\"09日\",\"10日\",\"11日\",\"12日\",\"13日\",\"14日\",\"15日\",\"16日\",\"17日\",\"18日\",\"19日\",\"20日\",\"21日\",\"22日\",\"23日\",\"24日\",\"25日\",\"26日\",\"27日\",\"28日\",\"29日\",\"30日\"],\"yData\":[{\"data\":[{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0}],\"name\":\"黑名单\"},{\"data\":[{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0}],\"name\":\"违禁进入\"},{\"data\":[{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0}],\"name\":\"聚集滞留\"},{\"data\":[{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0}],\"name\":\"违禁车型\"}]}";
        ChartData data2 = new Gson().fromJson(json, ChartData.class);
        chart2.setData(data2, XLineChartView.TagNumberShow.NUMBER);
        chart2.setMaxXsize(8);
        chart2.setFullMaxXsize(20);
        chart2.setyUnit("");
        chart2.setNumberUnit("");
        chart2.setLabelRotationAngle(data2.xData[0].length() > 4 ? 45 : 0);
        chart2.setmExtraRightOffset(20f);
        chart2.setDrawCircles(true);
        chart2.setCircleRadius(2f);
        chart2.setMode(XLineChartView.Mode.LINEAR);
        chart2.setDashedLine(false);
        chart2.setDrawFilled(false);
        chart2.upData();


    }

    public void click(View view) {
        if (view.getId() == R.id.button1) {
            String json = "{\"total\":0.0,\"xData\":[\"00日\",\"01日\",\"02日\",\"03日\",\"04日\",\"05日\",\"06日\",\"07日\",\"08日\",\"09日\",\"10日\",\"11日\",\"12日\",\"13日\",\"14日\",\"15日\",\"16日\",\"17日\",\"18日\",\"19日\",\"20日\",\"21日\",\"22日\",\"23日\",\"24日\",\"25日\",\"26日\",\"27日\",\"28日\",\"29日\",\"30日\"],\"yData\":[{\"data\":[{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0}],\"name\":\"黑名单\"},{\"data\":[{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0}],\"name\":\"违禁进入\"},{\"data\":[{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0}],\"name\":\"聚集滞留\"},{\"data\":[{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0}],\"name\":\"违禁车型\"}]}";
            ChartData data2 = new Gson().fromJson(json, ChartData.class);
            chart2.setData(data2, XLineChartView.TagNumberShow.NUMBER);
            chart2.setMaxXsize(10);
            chart2.setFullMaxXsize(20);
            chart2.setyUnit("");
            chart2.setNumberUnit("");
            chart2.setLabelRotationAngle(data2.xData[0].length() > 4 ? 45 : 0);
            chart2.setmExtraRightOffset(20f);
            chart2.setDrawCircles(true);
            chart2.setCircleRadius(2f);
            chart2.setMode(XLineChartView.Mode.LINEAR);
            chart2.setDashedLine(false);
            chart2.setDrawFilled(false);
            chart2.upData();

        }
    }

    ;


    public ChartData getTestData(String[] x, String[] y, float[] y1, float[] y2, float[] y3, float[] y4) {

        ChartData chartData = new ChartData();
        chartData.xData = x;
        chartData.yData = new ChartData.YData[4];
        for (int j = 0; j < 4; j++) {
            ChartData.YData yData = new ChartData.YData();
            yData.name = y[j];
            ChartData.OneYData[] oneYDatas = new ChartData.OneYData[x.length];
            float[] numbers = y1;
            if (j == 0) {
                numbers = y1;
            } else if (j == 1) {
                numbers = y2;
            } else if (j == 2) {
                numbers = y3;
            } else {
                numbers = y4;
            }


            for (int i = 0; i < numbers.length; i++) {
                ChartData.OneYData oneYData = new ChartData.OneYData();
                oneYData.number = numbers[i];
                oneYData.pro = 0;
                oneYDatas[i] = oneYData;
            }
            yData.data = oneYDatas;


            chartData.yData[j] = yData;
        }


        return chartData;


    }


}