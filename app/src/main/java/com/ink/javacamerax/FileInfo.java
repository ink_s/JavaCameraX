/*
 * Created by inks on  2022/12/8 下午4:53
 * Copyright (c) 2022 北京天恒昕业科技发展有限公司. All rights reserved.
 */

package com.ink.javacamerax;

import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * <pre>
 *     author : inks
 *     time   : 2022/12/08
 *     desc   : 文件
 *     version: 1.0
 * </pre>
 */
public class FileInfo implements Serializable {
    @SerializedName(value = "fileName", alternate = {"name"})
    public String fileName;

    @SerializedName(value = "filePath", alternate = {"path", "url"})
    public String filePath;
    public String fileType;
    public String appPath;

    //    ("文件来源") 1 人工巡护  2无人机  3打卡
    public String fileSource = "1";

    //    ("上传时间")
//    (pattern = "yyyy-MM-dd HH:mm:ss")
    public String uploadDate;

    public FileInfo() {
    }

    public FileInfo(String appPath, String fileSource) {
        this.filePath = null;
        this.appPath = appPath;
        this.fileType = getFileTypeByPath(appPath);
        this.uploadDate = getNowYearTimeHHmmss();
        this.fileSource = fileSource;


    }

    public String getFileTypeByPath(String filepath) {
        if (isImage(filepath)) {
            return "0";
        } else if (isVideo(filepath)) {
            return "1";
        } else {
            return "2";
        }
    }


    public static boolean isVideo(String path) {
        if (TextUtils.isEmpty(path)) {
            return false;
        }
        String extensionName = getExtensionName(path);
        String[] types = new String[]{"3gp", "mp4", "mpeg", "avi", "mov"};
        if (TextUtils.isEmpty(extensionName)) {
            return false;
        } else {
            for (String type : types) {
                if (extensionName.equalsIgnoreCase(type)) {
                    return true;
                }
            }
        }
        return false;
    }

    public static boolean isAudio(String path) {
        if (TextUtils.isEmpty(path)) {
            return false;
        }
        String extensionName = getExtensionName(path);
        String[] types = new String[]{"mp3", "wav"};
        if (TextUtils.isEmpty(extensionName)) {
            return false;
        } else {
            for (String type : types) {
                if (extensionName.equalsIgnoreCase(type)) {
                    return true;
                }
            }
        }
        return false;
    }

    public static String getExtensionName(String filename) {
        if ((filename != null) && (filename.length() > 0)) {
            int dot = filename.lastIndexOf('.');
            if ((dot > -1) && (dot < (filename.length() - 1))) {
                return filename.substring(dot + 1);
            }
        }
        return "";
    }

    public static boolean isImage(String filename) {
        String extensionName = getExtensionName(filename);
        String[] types = new String[]{"image", "png", "jpg", "jpeg"};
        if (TextUtils.isEmpty(extensionName)) {
            return false;
        } else {
            for (String type : types) {
                if (extensionName.equalsIgnoreCase(type)) {
                    return true;
                }
            }
        }
        return false;
    }

    public static String getNowYearTimeHHmmss() {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date();
        return formatter.format(date);
    }
}
