package com.ink.javacamerax;

import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import com.google.gson.Gson;
import com.ink.chart.entity.ChartData;
import com.ink.chart.view.XBarChartHorizontalView;
import com.ink.chart.view.XBarChartView;
import com.ink.chart.view.XChartFullScreenActivity;
import com.ink.util.AnimRotateUtil;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class MainActivity6 extends AppCompatActivity {


    String savePath;
    XBarChartHorizontalView pie1, pie2, pie3, pie4;

    CardView c1,c2;

    View v1, v2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main6);
        pie1 = findViewById(R.id.pie_1);
        pie2 = findViewById(R.id.pie_2);
        pie3 = findViewById(R.id.pie_3);
        pie4 = findViewById(R.id.pie_4);

        c1 = findViewById(R.id.card1);
        c2 = findViewById(R.id.card2);

        pie1.setTagBgColor(0XFF000000);
        pie1.setTagColor(0XFFFFFFFF);
        pie1.setTagTitleColor(0XFFFFFFFF);


        v1 = findViewById(R.id.image_1);
        v2 = findViewById(R.id.image_2);

    }


    public void click(View view) {
        if (view.getId() == R.id.card1) {

            L.ee("card1");
            AnimRotateUtil.FlipAnimatorXViewShow(c1,c2,600, AnimRotateUtil.Direction.LEFT_RIGHT);
        } else if (view.getId() == R.id.card2) {
            L.ee("card2");
            AnimRotateUtil.FlipAnimatorXViewShow(c2,c1,600, AnimRotateUtil.Direction.RIGHT_LEFT);
        }
        if (view.getId() == R.id.image_1) {
            L.ee("card1");
            AnimRotateUtil.FlipAnimatorXViewShow(v1,v2,600, AnimRotateUtil.Direction.LEFT_RIGHT);
        } else if (view.getId() == R.id.image_2) {
            L.ee("card2");
            AnimRotateUtil.FlipAnimatorXViewShow(v2,v1,600, AnimRotateUtil.Direction.RIGHT_LEFT);
        }
        else if (view.getId() == R.id.b1) {

            ChartData data1 = getTestData(new float[]{10400,30256,50753,40459,6236},
                    new String[]{"测试部门1","测试部门2","测试部门3","测试部门4","测试部门5"},
                    new String[]{"第一季度","第二季度","第三季度","第四季度"});

            pie1.setyUnit("分");
            pie1.setNumberUnit("分");
            pie1.setLabelRotationAngle(45);
            pie1.setmExtraRightOffset(30f);
            pie2.setTagGravity(Gravity.CENTER);
            pie1.setData(data1, XBarChartHorizontalView.TagNumberShow.FLOAT_NUMBER, XBarChartHorizontalView.BarShow.HORIZONTAL);
            pie1.upData();



            ChartData data2 = getTestData(new float[]{400,256,753},
                    new String[]{"日常巡护","专项巡护","临时巡护"},
                    new String[]{"1月","2月","3月","4月","5月","6月","7月","8月","9月","10月","11月","12月"});


            for (int i = 0; i <data2.yData.length ; i++) {
                List<ChartData.OneYData> list = Arrays.asList( data2.yData[i].data);
                Collections.reverse(list);
                data2.yData[i].data = (ChartData.OneYData[]) list.toArray();
            }

            pie2.setyUnit("次");
            pie2.setNumberUnit("次");
            pie2.setLabelRotationAngle(0);
            pie2.setTagGravity(Gravity.RIGHT);
            pie2.setMaxXsize(6);
            pie2.setData(data2, XBarChartHorizontalView.TagNumberShow.NUMBER,XBarChartHorizontalView.BarShow.HORIZONTAL);
            pie2.upData();

            ChartData data3 = getTestData(new float[]{753},
                    new String[]{"日常巡护"},
                    new String[]{"1月","2月","3月","4月","5月","6月","7月","8月","9月","10月","11月","12月"});

            pie3.setyUnit("次");
            pie3.setNumberUnit("次");
            pie3.setLabelRotationAngle(0);
            pie3.setTagGravity(Gravity.RIGHT);
            pie3.setData(data3, XBarChartHorizontalView.TagNumberShow.NUMBER,XBarChartHorizontalView.BarShow.STACKING);
            pie3.upData();



            ChartData data4 = getTestData(new float[]{400,256,753},
                    new String[]{"日常巡护","专项巡护","临时巡护"},
                    new String[]{"1月","2月","3月","4月","5月","6月","7月","8月","9月","10月","11月","12月"});


            pie4.setyUnit("次");
            pie4.setNumberUnit("次");
            pie4.setLabelRotationAngle(0);
            pie4.setTagGravity(Gravity.LEFT);
            pie4.setMaxXsize(6);
            pie4.setData(data4, XBarChartHorizontalView.TagNumberShow.NUMBER,XBarChartHorizontalView.BarShow.STACKING);
            pie4.upData();






        }
    }


    public ChartData getTestData(float[] total, String[] y, String[] x) {

        ChartData chartData = new ChartData();
        chartData.xData = x;
        chartData.yData = new ChartData.YData[y.length];
        for (int j = 0; j < y.length; j++) {
            ChartData.YData yData = new ChartData.YData();
            chartData.yData[j] = yData;
            yData.name = y[j];
            ChartData.OneYData[] oneYDatas = new ChartData.OneYData[x.length];
            float[] numbers = generateRandomArray(total[j], x.length);
            for (int i = 0; i < numbers.length; i++) {
                ChartData.OneYData oneYData = new ChartData.OneYData();
                oneYData.number = numbers[i];
                oneYData.pro = numbers[i] / total[j]*100;
                oneYDatas[i] = oneYData;
            }
            yData.data = oneYDatas;
        }

        return chartData;


    }


    public float[] generateRandomArray(float total, int count) {
        Random random = new Random();
        float[] array = new float[count];
        array[0] = total * random.nextFloat(); // 第一个元素在[1, total]范围内
        float sum = array[0];

        for (int i = 1; i < count; i++) {
            // 生成一个随机数，使得它加上之前的数字之和不会超过总数
            array[i] = (total - sum) * random.nextFloat();
            sum += array[i];
        }

        if (sum < total) {
            array[count - 1] += total - sum; // 如果总和小于给定的总数，则在数组末尾加上差值
        }

        return array;
    }


    public String getTextStr(int index) {
        if (index < 0) {
            return "";
        }
        String a = "";
        for (int i = 0; i < index; i++) {
            a = a + "A";
        }
        return a;
    }


}