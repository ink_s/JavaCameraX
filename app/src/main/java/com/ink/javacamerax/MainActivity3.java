package com.ink.javacamerax;

import android.os.Bundle;
import android.view.Gravity;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import com.google.gson.Gson;
import com.ink.camera.entity.MediaBean;
import com.ink.chart.entity.ChartData;
import com.ink.chart.view.XLineChartView;
import com.ink.view.ImageLoopView;

import java.util.ArrayList;
import java.util.Random;

public class MainActivity3 extends AppCompatActivity {


    String savePath;
    XLineChartView pie1, pie2, pie3, pie4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);
        pie1 = findViewById(R.id.pie_1);
        pie2 = findViewById(R.id.pie_2);
        pie3 = findViewById(R.id.pie_3);
        pie4 = findViewById(R.id.pie_4);


        ImageLoopView loopView = findViewById(R.id.loop);
        ArrayList<MediaBean> mediaList = new ArrayList<>();
        MediaBean bean = new MediaBean(MediaBean.TYPE.IMAGE.name(),"https://img0.baidu.com/it/u=707099767,3570364742&fm=253&fmt=auto&app=138&f=JPEG?w=485&h=360","info.sceneryName");
        mediaList.add(bean);

        MediaBean bean2 = new MediaBean(MediaBean.TYPE.IMAGE.name(),"https://img0.baidu.com/it/u=880729862,864612048&fm=253&fmt=auto&app=120&f=JPEG?w=950&h=633","info.");
        mediaList.add(bean2);

        loopView.setData(mediaList,MainActivity3.this);

    }


    public void click(View view) {
        if (view.getId() == R.id.b1) {

            ChartData data1 = getTestData(new float[]{400,256,753,459,236},
                    new String[]{"测试部门1","测试部门2","测试部门3","测试部门4","测试部门5"},
                    new String[]{"第一季度","第二季度","第三季度","第四季度"});

            pie1.setyUnit("分");
            pie1.setNumberUnit("分");
            pie1.setLabelRotationAngle(45);
            pie1.setmExtraRightOffset(20f);
            pie1.setData(data1, XLineChartView.TagNumberShow.FLOAT_NUMBER);
            pie1.setMode(XLineChartView.Mode.HORIZONTAL_BEZIER);
            pie1.setDashedLine(false);
            pie1.setDrawFilled(true);
            pie1.upData();



            ChartData data2 = getTestData(new float[]{0,0,0},
                    new String[]{"日常巡护","专项巡护","临时巡护"},
                    new String[]{"1","2","3","4","5","6","7","8","9","10","11","12","1","2","3","4","5","6","7","8","9","10","11","12"});

            pie2.setMaxXsize(6);
            pie2.setFullMaxXsize(12);
            pie2.setyUnit("次");
            pie2.setxUnit("月");
            pie2.setNumberUnit("次");
            pie2.setLabelRotationAngle(0);
            pie2.setmExtraBottomOffset(0f);
            pie2.setTagGravity(Gravity.RIGHT);
            pie2.setData(data2, XLineChartView.TagNumberShow.NUMBER);
            pie2.setMode(XLineChartView.Mode.LINEAR);
            pie2.setDashedLine(false);
            pie2.setDrawFilled(false);
            pie2.upData();


            String json = "{\"total\":0.0,\"xData\":[\"00\",\"01\",\"02\",\"03\",\"04\",\"05\",\"06\",\"07\",\"08\",\"09\",\"10\",\"11\",\"12\",\"13\",\"14\",\"15\",\"16\",\"17\",\"18\",\"19\",\"20\",\"21\",\"22\",\"23\",\"24\",\"25\",\"26\",\"27\",\"28\",\"29\",\"30\"],\"yData\":[{\"data\":[{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0}],\"name\":\"黑名单\"},{\"data\":[{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0}],\"name\":\"违禁进入\"},{\"data\":[{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0}],\"name\":\"聚集滞留\"},{\"data\":[{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0},{\"number\":0.0,\"pro\":0.0}],\"name\":\"违禁车型\"}]}";

            ChartData data3 = new Gson().fromJson(json, ChartData.class);

//            pie3.setMaxXsize(6);
//            pie3.setFullMaxXsize(12);
//            pie3.setyUnit("次");
//            pie3.setNumberUnit("次");
//            pie3.setLabelRotationAngle(0);
//            pie3.setTagGravity(Gravity.RIGHT);
//            pie3.setData(data3, XLineChartView.TagNumberShow.NUMBER);
//            pie3.setMode(XLineChartView.Mode.LINEAR);
//            pie3.setDashedLine(false);
//            pie3.setDrawFilled(false);
//            pie3.upData();

            pie3.setMaxXsize(6);
            pie3.setFullMaxXsize(31);
            pie3.setyUnit("次");
            pie3.setNumberUnit("次");
            pie3.setLabelRotationAngle( 0);
            pie3.setTagGravity(Gravity.RIGHT);
          //  pie3.setmExtraRightOffset(20f);
//            pie3.setDrawCircles(true);
//            pie3.setCircleRadius(2f);
            pie3.setData(data3, XLineChartView.TagNumberShow.NUMBER);
            pie3.setMode(XLineChartView.Mode.LINEAR);
            pie3.setDashedLine(false);
            pie3.setDrawFilled(false);

            pie3.upData();

        }
    }


    public ChartData getTestData(float[] total, String[] y, String[] x) {

        ChartData chartData = new ChartData();
        chartData.xData = x;
        chartData.yData = new ChartData.YData[y.length];
        for (int j = 0; j < y.length; j++) {
            ChartData.YData yData = new ChartData.YData();
            chartData.yData[j] = yData;
            yData.name = y[j];
            ChartData.OneYData[] oneYDatas = new ChartData.OneYData[x.length];
            float[] numbers = generateRandomArray(total[j], x.length);
            for (int i = 0; i < numbers.length; i++) {
                ChartData.OneYData oneYData = new ChartData.OneYData();
                oneYData.number = numbers[i];
                oneYData.pro = numbers[i] / total[j]*100;
                oneYDatas[i] = oneYData;
            }
            yData.data = oneYDatas;
        }

        return chartData;


    }


    public float[] generateRandomArray(float total, int count) {
        Random random = new Random();
        float[] array = new float[count];
        array[0] = total * random.nextFloat(); // 第一个元素在[1, total]范围内
        float sum = array[0];

        for (int i = 1; i < count; i++) {
            // 生成一个随机数，使得它加上之前的数字之和不会超过总数
            array[i] = (total - sum) * random.nextFloat();
            sum += array[i];
        }

        if (sum < total) {
            array[count - 1] += total - sum; // 如果总和小于给定的总数，则在数组末尾加上差值
        }

        return array;
    }


    public String getTextStr(int index) {
        if (index < 0) {
            return "";
        }
        String a = "";
        for (int i = 0; i < index; i++) {
            a = a + "A";
        }
        return a;
    }


}