package com.ink.javacamerax;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.giftedcat.justifylib.utils.LogUtil;
import com.ink.download.BaseDownloadActivity;
import com.ink.download.UpData;
import com.ink.entity.CheckGroupData;
import com.ink.record.PlayMusicActivity;
import com.ink.view.CheckGroupView;
import com.liulishuo.okdownload.DownloadListener;
import com.liulishuo.okdownload.DownloadMonitor;
import com.liulishuo.okdownload.DownloadTask;
import com.liulishuo.okdownload.OkDownload;
import com.liulishuo.okdownload.SpeedCalculator;
import com.liulishuo.okdownload.core.Util;
import com.liulishuo.okdownload.core.breakpoint.BlockInfo;
import com.liulishuo.okdownload.core.breakpoint.BreakpointInfo;
import com.liulishuo.okdownload.core.cause.EndCause;
import com.liulishuo.okdownload.core.cause.ResumeFailedCause;
import com.liulishuo.okdownload.core.dispatcher.CallbackDispatcher;
import com.liulishuo.okdownload.core.dispatcher.DownloadDispatcher;
import com.liulishuo.okdownload.core.download.DownloadStrategy;
import com.liulishuo.okdownload.core.file.DownloadUriOutputStream;
import com.liulishuo.okdownload.core.file.ProcessFileStrategy;
import com.liulishuo.okdownload.core.listener.DownloadListener4WithSpeed;
import com.liulishuo.okdownload.core.listener.assist.Listener4SpeedAssistExtend;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import es.dmoral.toasty.Toasty;

public class MainActivity5 extends AppCompatActivity {

    private long totalLength;
    private String readableTotalLength;

    private CheckGroupView checkGroupView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main5);

        findViewById(R.id.b2).setOnClickListener(click);
        findViewById(R.id.b3).setOnClickListener(click);
        findViewById(R.id.b4).setOnClickListener(click);
        findViewById(R.id.b5).setOnClickListener(click);
        findViewById(R.id.b6).setOnClickListener(click);
        findViewById(R.id.b7).setOnClickListener(click);
        findViewById(R.id.b8).setOnClickListener(click);

        Toasty.Config.getInstance()
                .tintIcon(true) // 是否配置图标
                .setToastTypeface(Typeface.DEFAULT) // 类型
                .setTextSize(14) // 字体大小sp
                .allowQueue(false) // 防止多个Toasty排队
                .setGravity(-1, 0, 50) // 偏移的角度
                .supportDarkTheme(true) // 是否支持暗夜模式
                .setRTL(false) // 图标是否右侧
                .apply(); // 一定要有这句话


        checkGroupView = findViewById(R.id.group_view);

        List<CheckGroupData> test = new ArrayList<>();
        for (int i = 0; i < 15; i++) {
            CheckGroupData data = new CheckGroupData("测试" + i, "" + i, i == 0);
            test.add(data);
        }
        checkGroupView.initData(test);

        checkGroupView.setCheckChangeListener(new CheckGroupView.OnCheckChange() {
            @Override
            public void onChekChange(View view, int position, boolean checked, List<CheckGroupData> data) {
                if (checked) {
                    return;
                }
                checkGroupView.checkOnlyOne(position);
                checkGroupView.moveTo(position);
            }
        });


//        HashMap<String, String> map = new HashMap<>();
//        map.put("applyId","5041");
//        map.put("versionType","0");
//        String url ="/tyrz/tyrz/version/getLatestVersion";
//        UpData.get(getApplication())
//                .setToast(true)
//                .setCurrentVersionCode("1.0.0")
//                .setUrl(url)
//                .setParams(map)
//                .setBaseUrl("http://59.110.26.156:1026/dev-api")
//                .setDownloadBaseUrl("http://59.110.26.156:1026/dev-api/file")
//                .setAppPath(Environment.getExternalStorageDirectory() + File.separator + "X测试2")
//                .setZip(true,Environment.getExternalStorageDirectory() + File.separator + "X测试2",null)
//                .setListener(new UpData.BackToastListener() {
//                    @Override
//                    public void onBackToast(String msg) {
//                        runOnUiThread(new Runnable() {
//                            @Override
//                            public void run() {
//                                Toast.makeText(MainActivity5.this,msg,Toast.LENGTH_SHORT).show();
//                            }
//                        });
//
//                    }
//                })
//                .start();

        Button button = findViewById(R.id.b1);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//
//                String url = "http://192.168.1.121:1024/dev-api/file/statics/tyrz/version/2023/09/04/1d371c7e-8004-4e40-aa6c-7874a30661ed.apk";
//
//                // url = "https://www.51wendang.com/pic/519cdef3bd6eb3aa0b89d193/3-810-jpg_6-1080-0-0-1080.jpg";
//                String path = Environment.getExternalStorageDirectory() + File.separator + "X测试";
//                String name = "1d371c7e-8004-4e40-aa6c-7874a30661ed.apk";
//
//                Intent intent = new Intent(getBaseContext(), BaseDownloadActivity.class);
//                intent.putExtra(BaseDownloadActivity.LOAD_FILE_URL, url);
//                intent.putExtra(BaseDownloadActivity.LOAD_FILE_NAME, name);
//                intent.putExtra(BaseDownloadActivity.LOAD_FILE_SAVE_PARENT, path);
//                intent.putExtra(BaseDownloadActivity.LOAD_OPEN, false);
//                intent.putExtra(BaseDownloadActivity.LOAD_FILE_RE, false);
//                startActivity(intent);

            }
        });
    }


    View.OnClickListener click = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (v.getId() == R.id.b2) {
                Toasty.error(getBaseContext(), "This is an error toast.", Toast.LENGTH_SHORT, true).show();

            } else if (v.getId() == R.id.b3) {
                Toasty.success(getBaseContext(), "Success!", Toast.LENGTH_SHORT, true).show();

            } else if (v.getId() == R.id.b4) {
                Toasty.info(getBaseContext(), "Here is some info for you.", Toast.LENGTH_SHORT, true).show();

            } else if (v.getId() == R.id.b5) {
                Toasty.warning(getBaseContext(), "Beware of the dog.", Toast.LENGTH_SHORT, true).show();

            } else if (v.getId() == R.id.b6) {
                Toasty.normal(getBaseContext(), "Normal toast w/ icon").show();

            } else if (v.getId() == R.id.b7) {
                Toasty.normal(getBaseContext(), "Normal toast w/ icon", R.drawable.icon_reverse).show();

            } else if (v.getId() == R.id.b8) {
                Toasty.custom(getBaseContext(), "I'm a custom Toast", R.drawable.icon_reverse, R.color.purple_200, 2000, true,

                        true).show();

            }

        }
    };


    /**
     * 创建下载任务实例
     *
     * @param url
     * @param parentPath
     * @param fileName
     * @return
     */
    public static DownloadTask createTasK(String url, String parentPath, String fileName) {
        return new DownloadTask.Builder(url, parentPath, fileName)
                .setFilenameFromResponse(false)//是否使用 response header or url path 作为文件名，此时会忽略指定的文件名，默认false
                .setPassIfAlreadyCompleted(true)//如果文件已经下载完成，再次下载时，是否忽略下载，默认为true(忽略)，设为false会从头下载
                .setConnectionCount(1)  //需要用几个线程来下载文件，默认根据文件大小确定；如果文件已经 split block，则设置后无效
                .setPreAllocateLength(false) //在获取资源长度后，设置是否需要为文件预分配长度，默认false
                .setMinIntervalMillisCallbackProcess(1000) //通知调用者的频率，避免anr，默认3000
                .setWifiRequired(false)//是否只允许wifi下载，默认为false
                .setAutoCallbackToUIThread(true) //是否在主线程通知调用者，默认为true
                //.setHeaderMapFields(new HashMap<String, List<String>>())//设置请求头
                //.addHeader(String key, String value)//追加请求头
                .setPriority(0)//设置优先级，默认值是0，值越大下载优先级越高
                .setReadBufferSize(4096)//设置读取缓存区大小，默认4096
                .setFlushBufferSize(16384)//设置写入缓存区大小，默认16384
                .setSyncBufferSize(65536)//写入到文件的缓冲区大小，默认65536
                .setSyncBufferIntervalMillis(2000) //写入文件的最小时间间隔，默认2000
                .build();

    }


    void initOkDownload() {
        OkDownload.Builder builder = new OkDownload.Builder(this)
                .downloadStore(Util.createDefaultDatabase(this))// //断点信息存储的位置，默认是SQLite数据库
                .callbackDispatcher(new CallbackDispatcher() {
                })//监听回调分发器，默认在主线程回调
                .downloadDispatcher(new DownloadDispatcher() {
                })//下载管理机制，最大下载任务数、同步异步执行下载任务的处理
                .connectionFactory(Util.createDefaultConnectionFactory())//选择网络请求框架，默认是OkHttp
                .outputStreamFactory(new DownloadUriOutputStream.Factory())// //构建文件输出流DownloadOutputStream，是否支持随机位置写入
                .downloadStrategy(new DownloadStrategy())//下载策略，文件分为几个线程下载
                .processFileStrategy(new ProcessFileStrategy())//多文件写文件的方式，默认是根据每个线程写文件的不同位置，支持同时写入
                .monitor(new DownloadMonitor() {
                    //开始任务
                    @Override
                    public void taskStart(DownloadTask task) {
                        LogUtil.i("taskStart");
                    }

                    //断点下载任务
                    @Override
                    public void taskDownloadFromBreakpoint(@NonNull DownloadTask task, @NonNull BreakpointInfo info) {
                        LogUtil.i("断点任务下载taskDownloadFromBreakpoint:" + task.getFilename());
                    }

                    //新任务
                    @Override
                    public void taskDownloadFromBeginning(@NonNull DownloadTask task, @NonNull BreakpointInfo info,
                                                          @Nullable ResumeFailedCause cause) {
                        LogUtil.i("新建任务下载taskDownloadFromBeginning:" + task.getFilename());
                    }

                    //任务结束
                    @Override
                    public void taskEnd(DownloadTask task, EndCause cause, @Nullable Exception realCause) {
                        LogUtil.i("下载完成taskEnd");
                        // 此处可以实现自己的逻辑,比如修改下载记录的任务状态,对下载文件进行加密

                    }
                });
        OkDownload.setSingletonInstance(builder.build());
//        DownloadDispatcher.setMaxParallelRunningCount(3);
        //设置监视器
//        OkDownload.with().setMonitor(new DownloadMonitor() {});
        //最大并行下载数
//        DownloadDispatcher.setMaxParallelRunningCount(3);
        //
//        RemitStoreOnSQLite.setRemitToDBDelayMillis(3000);
        //取消全部任务
//        OkDownload.with().downloadDispatcher().cancelAll();
        //移除某个任务
//        OkDownload.with().breakpointStore().remove(taskId);
    }


}