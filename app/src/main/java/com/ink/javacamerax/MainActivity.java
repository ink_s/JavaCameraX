package com.ink.javacamerax;

import static android.widget.Toast.makeText;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.LinearInterpolator;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import com.ink.camera.CameraActivity;
import com.ink.camera.ExoPlayerActivity;
import com.ink.camera.ImagePreviewActivity;
import com.ink.camera.MediaPreviewActivity;
import com.ink.camera.SystemCameraActivity;
import com.ink.camera.entity.EditImageInfo;
import com.ink.camera.entity.MediaBean;
import com.ink.camera.fragment.ImageLoopFragment;
import com.ink.camera.view.WaveView;
import com.ink.record.PlayMusicActivity;
import com.ink.util.MusicFileUtils;
import com.ink.view.MultipleProBar;
import com.ink.view.MultipleRoundProBar;
import com.luck.picture.lib.PictureSelector;
import com.luck.picture.lib.config.PictureConfig;
import com.luck.picture.lib.config.PictureMimeType;
import com.luck.picture.lib.entity.LocalMedia;
import com.luck.picture.lib.listener.OnResultCallbackListener;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    TestImageListAdapter adapter;
    ArrayList<String> pathList = new ArrayList<>();
    private Toast toast;

    ActivityResultLauncher<Intent> editResultLauncher;

    ActivityResultLauncher<Intent> cameraResultLauncher;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setupActivityResultLaunchers();
        recyclerView = findViewById(R.id.RecyclerView);
        adapter = new TestImageListAdapter(getApplicationContext(), pathList);
        StaggeredGridLayoutManager layoutManager = new StaggeredGridLayoutManager(3, StaggeredGridLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
        adapter.setOnItemClickListener(new TestImageListAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                previewFileList(pathList, position);
            }
        });

        WaveView waveView = findViewById(R.id.w1);
        initAnimation(waveView);
        if (mAnimatorSet != null) {
            mAnimatorSet.start();
        }
        ArrayList<MediaBean> mediaList = new ArrayList<>();
        MediaBean bean = new MediaBean(MediaBean.TYPE.IMAGE.name(),"https://img0.baidu.com/it/u=707099767,3570364742&fm=253&fmt=auto&app=138&f=JPEG?w=485&h=360","info.sceneryName");
        mediaList.add(bean);

        MediaBean bean2 = new MediaBean(MediaBean.TYPE.IMAGE.name(),"https://img0.baidu.com/it/u=880729862,864612048&fm=253&fmt=auto&app=120&f=JPEG?w=950&h=633","info.");
        mediaList.add(bean2);

        ImageLoopFragment fragment = new ImageLoopFragment(mediaList);

        getSupportFragmentManager().beginTransaction().add(R.id.fragment, fragment).commit();

        List<WaveView.WaveText> textList = new ArrayList<>();
        WaveView.WaveText waveText = new WaveView.WaveText();
        waveText.text = "AAAA";
        waveText.gravity = Gravity.CENTER;
        waveText.size = 14;
        waveText.lineHeight = 16;
        textList.add(waveText);

        textList.add(new WaveView.WaveText("BBB",18,20,Gravity.START,0.2f));

        textList.add(new WaveView.WaveText("CCC",14,16,Gravity.START,0.2f));


        waveView.setWaveTextList(textList);




    }


    private AnimatorSet mAnimatorSet;
    TextView textView;


    private void initAnimation(WaveView mWaveView) {
        List<Animator> animators = new ArrayList<>();
        ObjectAnimator waveShiftAnim = ObjectAnimator.ofFloat(
                mWaveView, "move", 0f, mWaveView.getCycleLength());
        waveShiftAnim.setRepeatCount(ValueAnimator.INFINITE);
        waveShiftAnim.setDuration(1000);
        waveShiftAnim.setInterpolator(new LinearInterpolator());
        animators.add(waveShiftAnim);


        ObjectAnimator waterLevelAnim = ObjectAnimator.ofFloat(
                mWaveView, "pro", 0f, 0.5f);
        waterLevelAnim.setDuration(5000);
        waterLevelAnim.setInterpolator(new DecelerateInterpolator());
        animators.add(waterLevelAnim);


        ObjectAnimator amplitudeAnim = ObjectAnimator.ofFloat(
                mWaveView, "amplitude", 0.02f, 0.05f);
        amplitudeAnim.setDuration(5000);
        amplitudeAnim.setInterpolator(new LinearInterpolator());
        animators.add(amplitudeAnim);

        mAnimatorSet = new AnimatorSet();
        mAnimatorSet.playTogether(animators);
        mWaveView.setAnimatorSet(mAnimatorSet);
    }


    public void click(View view) {
        if (view.getId() == R.id.b1) {


//            String path =  Environment.getExternalStorageDirectory() + File.separator + "a1.mp3";
//            MusicFileUtils.decodeMusicFile(path);

//            List< MultipleRoundProBar.MultipleRoundProData>  list= new ArrayList<>();
//            MultipleRoundProBar.MultipleRoundProData proData1 = new MultipleRoundProBar.MultipleRoundProData(new int[]{0XffFF0000},50);
//            list.add(proData1);
//
//            MultipleRoundProBar.MultipleRoundProData proData2 = new MultipleRoundProBar.MultipleRoundProData(new int[]{0XffFF0000,0XFFf330e3},80,false);
//            list.add(proData2);
//
//
//            MultipleRoundProBar.MultipleRoundProData proData3 = new MultipleRoundProBar.MultipleRoundProData(new int[]{0XFF0000},0);
//            list.add(proData3);
//
//            MultipleRoundProBar multipleRoundProBar =findViewById(R.id.round_bar);
//            multipleRoundProBar.setProDataList(list);




//            com.ink.camera.view.WaveView waveView = findViewById(R.id.w1);
//            waveView.setAnimatorDuration(5000);
//            Intent intent = new Intent(MainActivity.this, SystemCameraActivity.class);
//            intent.putExtra("onlyImage",true);
//            cameraResultLauncher.launch(intent);

//            Intent intent = new Intent(MainActivity.this, SystemCameraActivity.class);
//            intent.putExtra("saveDir", getExternalFilesDir(null).getPath());
//            startActivityForResult(intent, 100);
//            Intent intentRecord = new Intent();
//                //添加这一句表示对目标应用临时授权该Uri所代表的文件
//            intentRecord.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
//            intentRecord.setAction(MediaStore.Audio.Media.RECORD_SOUND_ACTION);
//            startActivityForResult(intentRecord, 1001);

//
            Intent intent = new Intent(MainActivity.this,SystemCameraActivity.class);
            intent.putExtra("audioEnable",true);
            cameraResultLauncher.launch(intent);


        } else if (view.getId() == R.id.b2) {
            String path = "https://www.jiangzhezhidian.com:29300/statics/biodiversity/monitorData/2023/03/28/7fb1e382-53e9-40e2-8d93-f12f692ecf5e.mp3";

//            Intent intent = new Intent(MainActivity.this, PlayMusicActivity.class);
//            intent.putExtra(PlayMusicActivity.PLAY_FILE_PATH, path);
//            startActivity(intent);

            ArrayList<MediaBean> data = new ArrayList<>();
            MediaBean bean1 = new MediaBean(MediaBean.TYPE.IMAGE.name(),"http://59.110.26.156:1026/dev-api/file/statics/patrol/application/2022/09/28/df5d019a-96aa-449c-9e24-7ba08895015f.jpg");
            data.add(bean1);

            MediaBean bean2 = new MediaBean(MediaBean.TYPE.VIDEO.name(),"http://59.110.26.156:1026/dev-api/file/statics/patrol/uav/2022/09/28/e0a4259d-ef56-40fe-a214-39360bd7ebbc.mp4");
            data.add(bean2);

            MediaBean bean3 = new MediaBean(MediaBean.TYPE.MUSIC.name(),"http://m10.music.126.net/20230504111212/db2d35d88c3249391f5961da5e2c0883/ymusic/5353/0f0f/0358/d99739615f8e5153d77042092f07fd77.mp3");

            data.add(bean3);


//            Intent intent = new Intent(MainActivity.this, ImagePreviewActivity.class);
//            intent.putExtra("imageUrl", "http://59.110.26.156:1026/dev-api/file/statics/patrol/application/2022/09/28/df5d019a-96aa-449c-9e24-7ba08895015f.jpg");
//            startActivity(intent);


            L.ee("d",data);

            Intent intent = new Intent(MainActivity.this, MediaPreviewActivity.class);
            intent.putParcelableArrayListExtra(MediaPreviewActivity.MEDIA_DATA, data);
            intent.putExtra(MediaPreviewActivity.INDEX,1);
            startActivity(intent);


//            intent.putExtra("saveDir", Environment.getExternalStorageDirectory() + File.separator + "XTEST");
//            startActivityForResult(intent,101);

//
//            if (pathList.size() < 1) {
//                showToast("请先选择图片");
//                return;
//            }
//
//            ArrayList<EditImageInfo> list = new ArrayList<>();
//            for (int i = 0; i < pathList.size(); i++) {
//                EditImageInfo editImageInfo = new EditImageInfo(pathList.get(i));
//                list.add(editImageInfo);
//
//            }
//
//            Intent intent = new Intent(MainActivity.this, EditImagesActivity.class);
//            intent.putParcelableArrayListExtra("paths", list);
//            editResultLauncher.launch(intent);
        } else if (view.getId() == R.id.b3) {
            ArrayList<MediaBean> data = new ArrayList<>();
            MediaBean bean1 = new MediaBean(MediaBean.TYPE.IMAGE.name(),"http://59.110.26.156:1026/dev-api/file/statics/patrol/application/2022/09/28/df5d019a-96aa-449c-9e24-7ba08895015f.jpg");
            data.add(bean1);

            MediaBean bean2 = new MediaBean(MediaBean.TYPE.VIDEO.name(),"http://59.110.26.156:1026/dev-api/file/statics/patrol/uav/2022/09/28/e0a4259d-ef56-40fe-a214-39360bd7ebbc.mp4");
            data.add(bean2);

            MediaBean bean3 = new MediaBean(MediaBean.TYPE.MUSIC.name(),"http://m10.music.126.net/20230504111212/db2d35d88c3249391f5961da5e2c0883/ymusic/5353/0f0f/0358/d99739615f8e5153d77042092f07fd77.mp3");
            data.add(bean3);

            MediaBean bean4 = new MediaBean(MediaBean.TYPE.IMAGE.name(),"storage/emulated/0/DCIM/CameraX/1.jpg");
            data.add(bean4);

//            Intent intent = new Intent(MainActivity.this, ImagePreviewActivity.class);
//            intent.putExtra("imageUrl", "http://59.110.26.156:1026/dev-api/file/statics/patrol/application/2022/09/28/df5d019a-96aa-449c-9e24-7ba08895015f.jpg");
//            startActivity(intent);


            L.ee("d",data);

            Intent intent = new Intent(MainActivity.this, MediaPreviewActivity.class);
            intent.putParcelableArrayListExtra(MediaPreviewActivity.MEDIA_DATA, data);
            intent.putExtra(MediaPreviewActivity.INDEX,1);
            startActivity(intent);


            //  selectImages();
        }
    }


    private void selectImages() {
        PictureSelector.create(this)
                .openGallery(PictureMimeType.ofImage())
                .imageEngine(GlideEngine.createGlideEngine())
                .selectionMode(PictureConfig.MULTIPLE)
                .maxSelectNum(8)
                .isCamera(false)
                .forResult(new OnResultCallbackListener<LocalMedia>() {
                    @SuppressLint("NotifyDataSetChanged")
                    @Override
                    public void onResult(List<LocalMedia> result) {
                        if (result == null || result.size() == 0) {
                            return;
                        }

                        ArrayList<String> list = new ArrayList<>();
                        for (LocalMedia l : result) {
                            list.add(l.getRealPath());
                        }
                        if (list.size() > 0) {
                            pathList.clear();
                            pathList.addAll(list);
                            adapter.notifyDataSetChanged();
                            Log.e("aa", pathList.get(0));
                        }
                    }

                    @Override
                    public void onCancel() {

                    }
                });

    }


    private void setupActivityResultLaunchers() {
        editResultLauncher = registerForActivityResult(
                new ActivityResultContracts.StartActivityForResult(),
                result -> {
                    if (result.getResultCode() == Activity.RESULT_OK) {
                        Intent data = result.getData();
                        handleEditorImage(data);
                    }
                });

        cameraResultLauncher = registerForActivityResult(
                new ActivityResultContracts.StartActivityForResult(),
                result -> {
                    if (result.getResultCode() == Activity.RESULT_OK) {
                        Intent data = result.getData();
                        Log.e("d", (data == null) + "");
                        handleCameraResult(data);
                    }
                });
    }

    @SuppressLint("NotifyDataSetChanged")
    private void handleCameraResult(Intent data) {
        String savePath = data.getStringExtra("savePath");
        Log.e("savePath", savePath);
        if (savePath != null) {
            File file = new File(savePath);
            if (file.exists()) {
                Log.e("exists", "1");
                pathList.add(savePath);
                adapter.notifyDataSetChanged();
            }
            Log.e("exists", "2");
        }
    }


    @SuppressLint("NotifyDataSetChanged")
    private void handleEditorImage(Intent data) {

        ArrayList<EditImageInfo> list = data.getParcelableArrayListExtra("backPaths");


        pathList.clear();
        if (list != null) {
            for (int i = 0; i < list.size(); i++) {
                if (!list.get(i).isDeleted()) {
                    if (list.get(i).isEdited()) {
                        pathList.add(list.get(i).getNewPath());
                    } else {
                        pathList.add(list.get(i).getOldPath());

                    }
                }
            }

        }
        Log.e("pathList", pathList.get(0));
        adapter.notifyDataSetChanged();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 100 && resultCode == RESULT_OK) {
            // SearchAddressInfo info = (SearchAddressInfo) data.getParcelableExtra("position");
            String savePath = data.getStringExtra("savePath");
            Log.e("onActivityResult", savePath);
            if (savePath.endsWith(".mp4")) {
                Intent intent = new Intent(MainActivity.this, ExoPlayerActivity.class);
                intent.putExtra("playPath", savePath);
                startActivity(intent);

//                Intent intent = new Intent(MainActivity.this, PreviewVideoActivity.class);
//                intent.putExtra("videoPath", savePath);
//                startActivity(intent);
            }

        } else if (requestCode == 101 && resultCode == RESULT_OK) {
            String savePath = data.getStringExtra("savePath");
            Log.e("onActivityResult", savePath);

            Intent intent = new Intent(MainActivity.this, PlayMusicActivity.class);
            intent.putExtra(PlayMusicActivity.PLAY_FILE_PATH, savePath);
            startActivity(intent);
        }
    }


    @SuppressLint("ShowToast")
    protected void showToast(String text) {
        if (TextUtils.isEmpty(text)) {
            return;
        }
        if (toast == null) {
            toast = makeText(getApplicationContext(), text, Toast.LENGTH_LONG);
        } else {
            toast.setText(text);
        }
        toast.show();
    }

    public void previewFileList(List<String> mList, int position) {
        ArrayList<MediaBean> mediaList = new ArrayList<>();
        for (int i = 0; i < mList.size(); i++) {
            String filePath = mList.get(i);

            if (!TextUtils.isEmpty(filePath)) {
                if (FileInfo.isVideo(filePath)) {
                    MediaBean bean = new MediaBean(MediaBean.TYPE.VIDEO.name(), filePath);
                    mediaList.add(bean);
                } else if (FileInfo.isImage(filePath)) {
                    MediaBean bean = new MediaBean(MediaBean.TYPE.IMAGE.name(), filePath);
                    mediaList.add(bean);
                } else if (FileInfo.isAudio(filePath)) {
                    MediaBean bean = new MediaBean(MediaBean.TYPE.MUSIC.name(), filePath);
                    mediaList.add(bean);
                }

            }
        }

        if (mediaList.size() > 0) {
            if (position >= mediaList.size()) {
                position = 0;
            }
            Intent intent = new Intent(getBaseContext(), MediaPreviewActivity.class);
            intent.putParcelableArrayListExtra(MediaPreviewActivity.MEDIA_DATA, mediaList);
            intent.putExtra(MediaPreviewActivity.INDEX, position);
            startActivity(intent);
        }


    }

}