package com.ink.javacamerax;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;

import com.github.gzuliyujiang.oaid.DeviceIdentifier;
import com.ink.chart.adapter.TagListAdapter;
import com.ink.chart.entity.TagBean;
import com.ink.chart.view.XPieChartView;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class MainActivity2 extends AppCompatActivity {


    String savePath;
    XPieChartView pie1, pie2,pie3,pie4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        pie1 = findViewById(R.id.pie_1);
        pie2 = findViewById(R.id.pie_2);
        pie3 = findViewById(R.id.pie_3);
        pie4 = findViewById(R.id.pie_4);

    }


    public void click(View view) {
        if (view.getId() == R.id.b1) {

            Random random = new Random();
            float total1 = 10 + (564500 - 10) * random.nextFloat();
            List<TagBean> tagBeans1 = getTestData(total1);
            List<TagBean> tagBeans2 = getTestData(total1);
            List<TagBean> tagBeans3 = getTestData(total1);
            List<TagBean> tagBeans4 = getTestData(total1);


            for(TagBean tag : tagBeans1){
                tag.numberUnit = "m";
                tag.showNumberUnit = true;
                tag.showProUnit = false;
                tag.showPro = false;
            }
            pie1.setHoleRadius(70f);
            pie1.setTransparentCircleRadius(70f);
            pie1.setData(tagBeans1, total1, XPieChartView.AllNumberShow.FLOAT_NUMBER);
            pie1.setRoundColor(0XFFFF0000);
            pie1.setRoundWidth(10);
            pie1.upData();



            for(TagBean tag : tagBeans2){
                tag.numberUnit = "m";
                tag.showNumberUnit = true;
                tag.showProUnit = true;
                tag.showPro = true;
            }

            pie2.setChangeCenter(false);
            pie2.setDrawHoleEnabled(false);
            pie2.setShowCenter(false);
            pie2.setOnItemClickListener(new TagListAdapter.OnItemClickListener() {
                @Override
                public void onItemClick(View view, TagBean info, int position) {

                }

                @Override
                public void onItemChoose(View view, TagBean info, int position) {

                }
            });
            pie2.setData(tagBeans2, total1, XPieChartView.AllNumberShow.NUMBER);
            pie2.upData();

            for(TagBean tag : tagBeans3){
                tag.numberColor = 0XFF1862F6;
                tag.numberUnit = "个";
                tag.showNumberUnit = true;
                tag.showPro = false;
                tag.showProUnit = false;
            }


            pie3.setHoleRadius(60f);
            pie3.setTransparentCircleRadius(70f);
            pie3.setNumberSize(15);
            pie3.setData(tagBeans3, total1, XPieChartView.AllNumberShow.NUMBER_PRO);
            pie3.upData();


            pie4.setHoleRadius(60f);
            pie4.setTransparentCircleRadius(50f);
            pie4.setAllNumberColor(0XFF2DD2A4);
            pie4.setAllNumberDp(20);
            pie4.setHideTag(true);
            pie4.setAllNumberUnit("个");
            pie4.setData(tagBeans4, total1, XPieChartView.AllNumberShow.NUMBER);
            pie4.upData();


        }
    }


    public List<TagBean> getTestData(float total) {
        List<TagBean> tagList = new ArrayList<>();
        Random random = new Random();
        int randomNum = random.nextInt(9) + 2;

        float[] numbers = generateRandomArray(total, randomNum);

        for (int i = 0; i < randomNum; i++) {
            TagBean tagBean = new TagBean();
            tagBean.code = "" + i;
            tagBean.name = "测试图例" + getTextStr(i);
            tagBean.number = numbers[i] + "";
            tagBean.pro = total == 0 ? "0" : String.format("%.2f", (Float.parseFloat(tagBean.number) * 100f / total));
            tagBean.proFloat = Float.parseFloat(tagBean.pro);
            tagBean.showPro = true;
            tagBean.proUnit = "%";
            tagBean.showProUnit = true;
            tagList.add(tagBean);
        }
        float all = 0;
        for (TagBean tagBean : tagList) {
            all += tagBean.proFloat;
        }
        if (all != 100) {
            for (int i = tagList.size() - 1; i >= 0; i--) {
                if (tagList.get(i).proFloat != 0) {
                    tagList.get(i).proFloat = tagList.get(i).proFloat + (100 - all);
                    tagList.get(i).pro = String.format("%.2f", tagList.get(i).proFloat);
                    break;

                }
            }
        }
        return tagList;
    }


    public float[] generateRandomArray(float total, int count) {
        Random random = new Random();
        float[] array = new float[count];
        array[0] = total * random.nextFloat(); // 第一个元素在[1, total]范围内
        float sum = array[0];

        for (int i = 1; i < count; i++) {
            // 生成一个随机数，使得它加上之前的数字之和不会超过总数
            array[i] = (total - sum) * random.nextFloat();
            sum += array[i];
        }

        if (sum < total) {
            array[count - 1] += total - sum; // 如果总和小于给定的总数，则在数组末尾加上差值
        }

        return array;
    }


    public  String getTextStr(int index){
        if(index<0){
            return "";
        }
        String a = "";
        for (int i = 0; i < index; i++) {
            a = a+"A";
        }
        return a;
    }


}