
package com.ink.javacamerax;


import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Shader;
import android.util.AttributeSet;
import android.view.View;

import androidx.annotation.Nullable;


/**
 * <pre>
 *     author : inks
 *     time   : 2023/1/9
 *     desc   : 圆环进度条
 *     version: 1.0
 * </pre>
 */
public class CircularProgressView extends View {
    private Context context;
    private int width;

    //圆环宽度
    private int ringWidth = 15;
    //圆环背景色
    private int bgColor = 0XFFE6F3FA;

    //进度颜色
    private int proColor = 0XFF00C13A;
    //开始角度
    private int startAngle = 90;
    //cap
    private Paint.Cap cap = Paint.Cap.ROUND;

    //数据0-100
    private float progress = 60;

    //背景画笔
    private Paint bgPaint;
    //圆环画笔
    private Paint ringPaint;
    //圆环画笔
    private Paint pointPaint;

    private boolean showPoint = false;
    private int pointWidth = 20;


    public CircularProgressView(Context context) {
        this(context, null);
    }

    public CircularProgressView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CircularProgressView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        this(context, attrs, defStyleAttr, 0);
    }

    public CircularProgressView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        this.context = context;
        init(attrs);
    }

    public void init(AttributeSet attrs) {


        bgPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        bgPaint.setStrokeWidth(ringWidth);
        bgPaint.setStyle(Paint.Style.STROKE);
        bgPaint.setColor(bgColor);


        ringPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        ringPaint.setStrokeWidth(ringWidth);
        ringPaint.setStyle(Paint.Style.STROKE);
        ringPaint.setStrokeCap(cap);
        ringPaint.setColor(proColor);


        pointPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        pointPaint.setStyle(Paint.Style.FILL);
        pointPaint.setColor(proColor);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int specHeightSize = MeasureSpec.getSize(heightMeasureSpec);//高
        int specWidthSize = MeasureSpec.getSize(widthMeasureSpec);
        width = Math.min(specHeightSize, specWidthSize);
       // setMeasuredDimension(width, width);
    }


    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        RectF rectBlackBg = new RectF( Math.max(ringWidth,pointWidth) , Math.max(ringWidth,pointWidth), width - Math.max(ringWidth,Math.max(ringWidth,pointWidth)), width - Math.max(ringWidth,pointWidth));
        canvas.drawArc(rectBlackBg, 0, 360, false, bgPaint);

        canvas.drawArc(rectBlackBg, startAngle, progress * 360 / 100, false, ringPaint);



        canvas.drawLine(0,100,width,100,ringPaint);
        canvas.drawLine(100,20,100,800,ringPaint);

        if (showPoint) {

            float angle = startAngle + progress * 360 / 100;
//            if(angle>360){
//                angle = angle%360;
//            }else if(angle<-360){
//                angle = - (Math.abs(angle)%360);
//            }
//
//            if(angle<0){
//                angle = 360+angle;
//            }

            float radian = (float) Math.toRadians(angle);
            float x0 = rectBlackBg.centerX();
            float y0 = rectBlackBg.centerY();
            float r0 = rectBlackBg.width() * 0.5f;

            float x = (float) (x0 + r0 * Math.cos(radian));
            float y = (float) (y0 + r0 * Math.sin(radian));

            canvas.drawCircle(x, y, pointWidth/2.0f, pointPaint);


        }


    }

    public int getRingWidth() {
        return ringWidth;
    }

    public void setRingWidth(int ringWidth) {
        this.ringWidth = ringWidth;
        if (bgPaint != null) {
            bgPaint.setStrokeWidth(ringWidth);
        }
        if (ringPaint != null) {
            ringPaint.setStrokeWidth(ringWidth);
        }
        if (pointPaint != null) {
            pointPaint.setStrokeWidth(ringWidth);
        }
    }

    public int getBgColor() {
        return bgColor;
    }

    public void setBgColor(int bgColor) {
        this.bgColor = bgColor;
        if (bgPaint != null) {
            bgPaint.setColor(bgColor);
        }
    }

    public int getProColor() {
        return proColor;
    }

    public void setProColor(int proColor) {
        this.proColor = proColor;
        if (ringPaint != null) {
            ringPaint.setColor(proColor);
        }
    }

    public void setProColorAndPoint(int startColor, int endColor, boolean showPoint, int pointWidth) {
        this.showPoint = showPoint;
        this.pointWidth = pointWidth;
        if (ringPaint != null) {
            Shader shader = new LinearGradient(0, 0, 0, 800,
                    startColor, endColor,
                    Shader.TileMode.REPEAT);
            ringPaint.setShader(shader);


            if (pointPaint != null) {
                pointPaint.setShader(shader);
            }
        }


        invalidate();
    }


    public int getStartAngle() {
        return startAngle;
    }

    public void setStartAngle(int startAngle) {
        this.startAngle = startAngle;
    }

    public Paint.Cap getCap() {
        return cap;
    }

    public void setCap(Paint.Cap cap) {
        this.cap = cap;
    }

    public float getProgress() {
        return progress;
    }

    public void setProgress(float progress) {
        this.progress = progress;
        invalidate();
    }


}