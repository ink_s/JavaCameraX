package com.ink.javacamerax;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;

/**
 * 巡护数据
 *
 * @author fcp
 * @time 2022/2/23
 */
public class ImageMaskView extends RelativeLayout {

    private TextView time;
    private TextView lon;
    private TextView lat;
    private TextView alt;

    private String timeStr;
    private String lonStr;
    private String latStr;
    private String altStr;

    public ImageMaskView(Context context) {
        super(context);
        init(null);
    }

    public ImageMaskView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public ImageMaskView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }

    public ImageMaskView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(attrs);
    }

    private void init(AttributeSet attrs) {
        View view = LayoutInflater.from(getContext()).inflate(R.layout.patrol_mask_view, null);
        time = view.findViewById(R.id.time);
        lon = view.findViewById(R.id.lon);
        lat = view.findViewById(R.id.lat);
        alt = view.findViewById(R.id.alt);
        time.setText(timeStr);
        lon.setText(lonStr);
        lat.setText(latStr);
        alt.setText(altStr);
        addView(view);
    }


    public void setMask(String timeStr, String lonStr, String latStr, String altStr) {
        this.timeStr = timeStr;
        this.lonStr = lonStr;
        this.latStr = latStr;
        this.altStr = altStr;
        if (time != null) {
            time.setText(timeStr);
        }
        if (lon != null) {
            lon.setText(lonStr);
        }
        if (lat != null) {
            lat.setText(latStr);
        }
        if (alt != null) {
            alt.setText(altStr);
        }
    }

}
