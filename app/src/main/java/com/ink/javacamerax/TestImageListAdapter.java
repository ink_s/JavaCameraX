package com.ink.javacamerax;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;


public class TestImageListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<String> dataList = new ArrayList<>();

    private Context context;

    public TestImageListAdapter(Context context,List<String> dataList){
        this.dataList = dataList;
        this.context = context;
    }

    public interface OnItemClickListener {
        void onItemClick(View view,  int position);
    }

    public OnItemClickListener onItemClickListener;

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public void setData(List<String> dataList) {
        this.dataList = dataList;
        notifyDataSetChanged();
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.test_image_item, parent, false);
        return new RecyclerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        RecyclerViewHolder vh = (RecyclerViewHolder) holder;

        Glide.with(context)
                .load(dataList.get(position))
                //.override(100)
                .into(vh.image);

        vh.image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(onItemClickListener != null){
                    onItemClickListener.onItemClick(v,position);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        //return dataList.size() + 1;

        return dataList.size() ;
    }

    private class RecyclerViewHolder extends RecyclerView.ViewHolder {
        ImageView image;
        ImageView delete;

        RecyclerViewHolder(View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.image_view);
            delete = itemView.findViewById(R.id.delete_view);
        }
    }
}