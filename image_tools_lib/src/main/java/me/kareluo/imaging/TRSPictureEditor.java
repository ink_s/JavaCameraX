package me.kareluo.imaging;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.View;

import androidx.activity.result.ActivityResultLauncher;

import me.kareluo.imaging.core.util.IMGUtils;


/**
 * 入口类
 * 直接使用edit方法传入需要编辑的bitmap
 * 最后在editListener中的onComplete方法获取编辑后的图片
 * 框架不再保存图片到设备上。
 */
public class TRSPictureEditor {
    public static final int BOX_ENABLE = 0x00000001;//方形选择框
    public static final int CIRCLE_ENABLE = 0x00000002;//圆形选择框
    public static final int TXT_ENABLE = 0x00000004;//文字
    public static final int PAINT_ENABLE = 0x00000010;//画笔
    public static final int ARROW_ENABLE = 0x00000020;//箭头
    public static final int MOSAIC_ENABLE = 0x00000040;//马赛克
    public static final int CLIP_ENABLE = 0x00000100;//裁剪

    public static final int ALL_ENABLE = BOX_ENABLE | CIRCLE_ENABLE | TXT_ENABLE | PAINT_ENABLE | ARROW_ENABLE | MOSAIC_ENABLE | CLIP_ENABLE;
    /**
     * 定义开启的功能
     */
    private static int style = ALL_ENABLE;

    public static final int[] ENABLE_ARRAY = new int[]{BOX_ENABLE, CIRCLE_ENABLE, TXT_ENABLE, PAINT_ENABLE, ARROW_ENABLE, MOSAIC_ENABLE, CLIP_ENABLE};

    public static int getStyle() {
        return style;
    }

    public static void setStyle(int style) {
        TRSPictureEditor.style = style;
    }

    public static void disable(int mask) {
        TRSPictureEditor.style &= ~mask;
    }

    public static void enable(int mask) {
        TRSPictureEditor.style &= mask;
    }


    public static void edit(Context context, Bitmap bitmap, EditListener editListener) {
        if (editListener == null) {
            return;
        }
        if (bitmap == null) {
            editListener.onError(new RuntimeException("图片为空"));
            return;
        }
        if (context == null) {
            editListener.onError(new RuntimeException("context为空"));
            return;
        }
        ImageHolder.getInstance().reset();
        ImageHolder.getInstance().setEditListener(editListener);
        ImageHolder.getInstance().setBitmap(bitmap);
        context.startActivity(new Intent(context, IMGEditActivity.class));
    }


    public static void edit(Context context, String path, EditListener editListener) {
        Bitmap bitmap = getBitmap(path);
        if (editListener == null) {
            return;
        }
        if (bitmap == null) {
            editListener.onError(new RuntimeException("图片为空"));
            return;
        }
        if (context == null) {
            editListener.onError(new RuntimeException("context为空"));
            return;
        }
        ImageHolder.getInstance().reset();
        ImageHolder.getInstance().setEditListener(editListener);
        ImageHolder.getInstance().setImagePath(path);
        ImageHolder.getInstance().setBitmap(bitmap);
        context.startActivity(new Intent(context, IMGEditActivity.class));
    }





    public static void edit(Context context,ActivityResultLauncher<Intent> resultLauncher, String path, EditListener editListener) {
        Bitmap bitmap = getBitmap(path);
        if (editListener == null) {
            return;
        }
        if (bitmap == null) {
            editListener.onError(new RuntimeException("图片为空"));
            return;
        }
        if (context == null) {
            editListener.onError(new RuntimeException("context为空"));
            return;
        }
        ImageHolder.getInstance().reset();
        ImageHolder.getInstance().setEditListener(editListener);
        ImageHolder.getInstance().setImagePath(path);
        ImageHolder.getInstance().setBitmap(bitmap);
        ImageHolder.getInstance().setLauncher(true);
        resultLauncher.launch(new Intent(context, IMGEditActivity.class));
    }

    public interface EditListener {
        void onCancel();

        void onComplete(Bitmap bitmap, String inPath);

        void onError(Throwable throwable);
        //失败
    }

    /**
     * 一个空的实现，方便调用者只实现部分方法
     */
    public static class EditAdapter implements EditListener {

        @Override
        public void onCancel() {

        }

        @Override
        public void onComplete(Bitmap bitmap, String inPath) {

        }


        @Override
        public void onError(Throwable throwable) {

        }
    }


    public static  Bitmap getBitmap(String path) {
        try {
            int MAX_WIDTH = 1024;

            int MAX_HEIGHT = 1024;

            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inSampleSize = 1;
            options.inJustDecodeBounds = true;

            if (options.outWidth > MAX_WIDTH) {
                options.inSampleSize = IMGUtils.inSampleSize(Math.round(1f * options.outWidth / MAX_WIDTH));
            }

            if (options.outHeight > MAX_HEIGHT) {
                options.inSampleSize = Math.max(options.inSampleSize,
                        IMGUtils.inSampleSize(Math.round(1f * options.outHeight / MAX_HEIGHT)));
            }

            options.inJustDecodeBounds = false;
            return BitmapFactory.decodeFile(path, options);
        } catch (Exception e) {
            return null;
        }
    }


}
