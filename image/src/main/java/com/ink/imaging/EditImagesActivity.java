package com.ink.imaging;

import static android.widget.Toast.makeText;
import static com.ink.imaging.IMGEditActivity.EXTRA_IMAGE_SAVE_PATH;
import static com.ink.imaging.IMGEditActivity.EXTRA_IMAGE_URI;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContract;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityOptionsCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import com.facebook.common.file.FileUtils;
import com.ink.imaging.adapter.ImageListAdapter;
import com.ink.imaging.core.util.ClickUtil;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import cn.hzw.doodle.DoodleActivity;
import cn.hzw.doodle.DoodleParams;
import iamutkarshtiwari.github.io.ananas.editimage.EditImageActivity;
import iamutkarshtiwari.github.io.ananas.editimage.ImageEditorIntentBuilder;

public class EditImagesActivity extends AppCompatActivity {


    private RecyclerView recyclerView;
    //传入的
    private List<String> oldPathList = new ArrayList<>();

    private List<String> pathList = new ArrayList<>();

    private ImageListAdapter adapter;


    ActivityResultLauncher<Intent> activityResultLauncher;

    private Toast toast;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.image_activity_edit_images);

        recyclerView = findViewById(R.id.RecyclerView);

        StaggeredGridLayoutManager layoutManager = new StaggeredGridLayoutManager(3, StaggeredGridLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);

        initData();
        activityResultLauncher = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), result -> {
        });

    }

    private void initData() {
        oldPathList = getIntent().getStringArrayListExtra("paths");


        if (oldPathList == null) {
            oldPathList = new ArrayList<>();
        }
        oldPathList.add("/storage/emulated/0/DCIM/CameraX/1656322022954.jpg");


        pathList.addAll(oldPathList);
        adapter = new ImageListAdapter(this, pathList);
        recyclerView.setAdapter(adapter);
        adapter.setItemClickListener(adapterClick);

    }


    ImageListAdapter.OnItemClickListener adapterClick = new ImageListAdapter.OnItemClickListener() {
        @Override
        public void onAddClick() {

        }

        @Override
        public void onDeleteClick(String path, int position) {

        }

        @Override
        public void onImageClick(String path, int position) {
//            Intent intent = new Intent(EditImagesActivity.this,IMGEditActivity.class);
            Uri uri = Uri.parse(path);
//            String savePath = path;
//            intent.putExtra(EXTRA_IMAGE_URI,uri);
//            intent.putExtra(EXTRA_IMAGE_SAVE_PATH,savePath);
//            startActivity(intent);

//            DoodleParams params = new DoodleParams(); // 涂鸦参数
//            params.mImagePath = path; // the file path of image
//            DoodleActivity.startActivityForResult(EditImagesActivity.this, params, 0);

//            Intent intent = new Intent("com.android.camera.action.CROP");
//            intent.setDataAndType(uri, "image/*");
//            intent.putExtra("crop", "true");
//            startActivityForResult(intent,0);

    //        canCrop(path);


            try {
                Intent intent = new ImageEditorIntentBuilder(EditImagesActivity.this, path, path)
                        .withAddText() // Add the features you need
                        .withPaintFeature()
                        .withFilterFeature()
                        .withRotateFeature()
                        .withCropFeature()
                        .withBrightnessFeature()
                        .withSaturationFeature()
                        .withBeautyFeature()
                        .withStickerFeature()
                        .forcePortrait(true)  // Add this to force portrait mode (It's set to false by default)
                        .setSupportActionBarVisibility(false) // To hide app's default action bar
                        .build();


                EditImageActivity.start(activityResultLauncher, intent, EditImagesActivity.this);
            } catch (Exception e) {
                Log.e("Demo App", e.getMessage()); // This could throw if either `sourcePath` or `outputPath` is blank or Null
            }


        }
    };


    public void click(View view) {
        if (!ClickUtil.isFastDoubleClick((long) 500)) {
            if (view.getId() == R.id.cancel) {
            } else if (view.getId() == R.id.done) {
            }
        }


    }


    private boolean canCrop(String imagePath) {

        File file = new File(imagePath);

        Intent intent = new Intent("com.android.camera.action.CROP");

        intent.setDataAndType(Uri.parse(imagePath), "image/*");

        intent.putExtra("crop", "true");


        intent.putExtra("scale", true);

        intent.putExtra("scaleUpIfNeeded", true);

        intent.putExtra("return-data", false);

        intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(file));

        intent.putExtra("outputFormat", Bitmap.CompressFormat.JPEG.toString());

        intent.putExtra("noFaceDetection", true);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(intent, 0);
            return true;
        } else {//没有安装所需应用

            return false;

        }
    }



    @SuppressLint("ShowToast")
    protected void showLongToast(String text) {
        if (TextUtils.isEmpty(text)) {
            return;
        }
        if (toast == null) {
            toast = makeText(getApplicationContext(), text, Toast.LENGTH_LONG);
        } else {
            toast.setText(text);
        }
        toast.show();
    }

    @SuppressLint("ShowToast")
    protected void showShortToast(String text) {
        if (TextUtils.isEmpty(text)) {
            return;
        }
        if (toast == null) {
            toast = makeText(getApplicationContext(), text, Toast.LENGTH_SHORT);
        } else {
            toast.setText(text);
        }
        toast.show();
    }

}