package com.ink.imaging.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.ink.imaging.R;

import java.util.ArrayList;
import java.util.List;


public class ImageListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<String> dataList = new ArrayList<>();

    protected OnItemClickListener mItemClickListener;

    //最大图片个数
    private int maxNumber = 8;

    private Context context;

    public ImageListAdapter(Context context,List<String> dataList){
        this.context = context;
        this.dataList = dataList;
    }

    public void setItemClickListener(OnItemClickListener itemClickListener) {
        mItemClickListener = itemClickListener;
    }


    public interface OnItemClickListener {
        void onAddClick();
        void onDeleteClick(String path, int position);
        void onImageClick(String path, int position);
    }

    public void setData(List<String> dataList) {
        this.dataList = dataList;
        notifyDataSetChanged();
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.image_item_edit_image, parent, false);
        return new RecyclerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        RecyclerViewHolder vh = (RecyclerViewHolder) holder;
        if (position == dataList.size()) {
            //最后一个，添加图标
            vh.delete.setVisibility(View.GONE);

            if (dataList.size() >= maxNumber) {
                //到了最大的个数，不能添加了
                vh.image.setVisibility(View.GONE);
            } else {
                vh.image.setVisibility(View.VISIBLE);
                vh.image.setImageResource(R.drawable.image_image_add_image);
            }
        } else {
            vh.image.setVisibility(View.VISIBLE);

            Glide.with(context)
                    .load(dataList.get(position))
                    //.override(100)
                    .into(vh.image);
            vh.delete.setVisibility(View.VISIBLE);
        }

        vh.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mItemClickListener!=null){
                    mItemClickListener.onDeleteClick(dataList.get(position),position);
                }

            }
        });
        vh.image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mItemClickListener!=null){
                    if(position == dataList.size()){
                        mItemClickListener.onAddClick();
                    }else{
                        mItemClickListener.onImageClick(dataList.get(position),position);
                    }

                }
            }
        });


    }

    @Override
    public int getItemCount() {
        //return dataList.size() + 1;

        return dataList.size() ;
    }

    private class RecyclerViewHolder extends RecyclerView.ViewHolder {
        ImageView image;
        ImageView delete;

        RecyclerViewHolder(View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.image_view);
            delete = itemView.findViewById(R.id.delete_view);
        }
    }
}