package com.ink.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Shader;
import android.util.AttributeSet;
import android.view.View;

import androidx.annotation.Nullable;

import com.ink.camera.R;
import com.inks.inkslibrary.Utils.L;

import java.util.List;

/**
 * <pre>
 *     author : inks
 *     time   : 2023/06/09
 *     desc   : MultipleProBar
 *     version: 1.0
 * </pre>
 */

public class MultipleRoundProBar extends View {
    private Context context;

    //外圈宽度
    private int circleWidth = 2;
    private boolean showCircle = false;
    private int circleColor;

    //圆环宽度
    private int roundWidth = 5;

    //圆环间隔
    private int roundIntervalWidth = 5;


    //view宽高
    private int viewSize;

    private int bgColor;

    private int startAngle = 0;
    private int endAngle = 270;

    //cap
    private Paint.Cap cap = Paint.Cap.BUTT ;

    private Paint paint;
    private Paint bgPaint;
    private Paint circlePaint;

    private List<MultipleRoundProData> proDataList;

    public MultipleRoundProBar(Context context) {
        this(context, null);
    }

    public MultipleRoundProBar(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public MultipleRoundProBar(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        this(context, attrs, 0, 0);
    }

    public MultipleRoundProBar(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        this.context = context;
        init(attrs);
    }

    @SuppressLint("DrawAllocation")
    public void init(AttributeSet attrs) {
        if (attrs != null) {
            @SuppressLint("CustomViewStyleable")
            TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.XMultipleRoundProBar);
            roundWidth = (int) typedArray.getDimension(R.styleable.XMultipleRoundProBar_mr_round_width, 10);
            roundIntervalWidth = (int) typedArray.getDimension(R.styleable.XMultipleRoundProBar_mr_round_interval_width, 10);
            circleWidth= (int) typedArray.getDimension(R.styleable.XMultipleRoundProBar_mr_circle_width, 2);
            showCircle = typedArray.getBoolean(R.styleable.XMultipleRoundProBar_mr_show_circle, false);
            bgColor = typedArray.getColor(R.styleable.XMultipleRoundProBar_mr_bg_color,0XFFeaeaea);
            circleColor = typedArray.getColor(R.styleable.XMultipleRoundProBar_mr_circle_color,0XFFeaeaea);
            startAngle = typedArray.getInt(R.styleable.XMultipleRoundProBar_mr_start_angle,-90);
            endAngle = typedArray.getInt(R.styleable.XMultipleRoundProBar_mr_end_angle,-360);
            typedArray.recycle();
        }

        paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setStrokeWidth(roundWidth);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeCap(cap);

        bgPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        bgPaint.setStrokeWidth(roundWidth);
        bgPaint.setStyle(Paint.Style.STROKE);
        bgPaint.setStrokeCap(cap);
        bgPaint.setColor(bgColor);


        circlePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        circlePaint.setStrokeWidth(circleWidth);
        circlePaint.setStyle(Paint.Style.STROKE);
        circlePaint.setColor(circleColor);


    }


    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int specHeightSize = MeasureSpec.getSize(heightMeasureSpec);//高
        int specWidthSize = MeasureSpec.getSize(widthMeasureSpec);//宽
        viewSize = Math.min(specHeightSize, specWidthSize);
        setMeasuredDimension(viewSize, viewSize);
    }

    @SuppressLint("DrawAllocation")
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        if(showCircle){
            RectF rectCircle = new RectF(circleWidth/2.0f, circleWidth/2.0f, viewSize-circleWidth/2.0f, viewSize-circleWidth/2.0f);
            canvas.drawArc(rectCircle, 0, 360, false,circlePaint );
        }


        if (proDataList == null || proDataList.size() == 0) {
            return;
        }


        for (int i = 0; i < proDataList.size(); i++) {
            float interval = roundIntervalWidth*(i+1)+i*roundWidth+roundWidth*0.5f+circleWidth;
            //画背景环
            RectF rectBg = new RectF(interval, interval, viewSize-interval, viewSize-interval);
            canvas.drawArc(rectBg, startAngle, endAngle-startAngle, false,bgPaint );


            float   end = (endAngle-startAngle)*proDataList.get(i).pro/100.f;
            LinearGradient shader;
            if (proDataList.get(i).colorEnd) {
                shader = new LinearGradient(interval, interval, viewSize-interval, viewSize-interval , proDataList.get(i).proColors, null,
                        Shader.TileMode.CLAMP);
            } else {
                shader = new LinearGradient(interval, interval, viewSize-interval, (viewSize-interval)*proDataList.get(i).pro/100, proDataList.get(i).proColors, null,
                        Shader.TileMode.CLAMP);
            }
            paint.setShader(shader);
            canvas.drawArc(rectBg, startAngle, end, false,paint );

        }

    }

    public void setCap(Paint.Cap cap) {
        this.cap = cap;
        paint.setStrokeCap(cap);
        bgPaint.setStrokeCap(cap);
        invalidate();
    }

    public void setShowCircle(boolean showCircle) {
        this.showCircle = showCircle;
        invalidate();
    }

    public void setProDataList(List<MultipleRoundProData> proDataList) {
        this.proDataList = proDataList;
        invalidate();
    }

    public List<MultipleRoundProData> getProDataList() {
        return proDataList;
    }

    public static class MultipleRoundProData {
        public int[] proColors;
        public float pro;
        //渐变时按100渐变，false 按进度渐变
        public boolean colorEnd = true;

        public MultipleRoundProData(int[] proColors, float pro ) {
            this.proColors =  reviseColors(proColors);
            this.pro = pro;
            this.colorEnd = true;

        }

        public   MultipleRoundProData(int[] proColors, float pro,  boolean colorEnd) {
            this.proColors = reviseColors(proColors);
            this.pro = pro;
            this.colorEnd = colorEnd;

        }
    }

    private static int[] reviseColors(int[] proColors) {
        if(proColors.length==1){
           return new int[]{proColors[0],proColors[0]};
        }else{
            return proColors;
        }
    }


}
