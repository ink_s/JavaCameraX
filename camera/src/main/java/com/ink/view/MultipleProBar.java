package com.ink.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Shader;
import android.util.AttributeSet;
import android.view.View;

import androidx.annotation.Nullable;

import com.ink.camera.R;
import com.inks.inkslibrary.Utils.L;

import java.util.List;

/**
 * <pre>
 *     author : inks
 *     time   : 2023/06/09
 *     desc   : MultipleProBar
 *     version: 1.0
 * </pre>
 */

public class MultipleProBar extends View {
    private Context context;

    //进度条高度
    private int proHeight = 5;
    //图片宽高大小
    private int imageSize;
    //view宽高，高度为图片高度
    private int viewWidth;
    private int viewHeight;

    private int bgColor;

    private Paint paint;
    private Paint bgPaint;

    private List<MultipleProData> proDataList;

    public MultipleProBar(Context context) {
        this(context, null);
    }

    public MultipleProBar(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public MultipleProBar(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        this(context, attrs, 0, 0);
    }

    public MultipleProBar(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        this.context = context;
        init(attrs);
    }

    @SuppressLint("DrawAllocation")
    public void init(AttributeSet attrs) {
        if (attrs != null) {
            @SuppressLint("CustomViewStyleable")
            TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.XMultipleProBar);
            proHeight = (int) typedArray.getDimension(R.styleable.XMultipleProBar_mp_pro_height, 10);
            imageSize = (int) typedArray.getDimension(R.styleable.XMultipleProBar_mp_image_size, 40);
            bgColor = typedArray.getColor(R.styleable.XMultipleProBar_mp_bg_color,0XFFeaeaea);
            typedArray.recycle();
        }

        paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setStrokeWidth(proHeight);
        paint.setStyle(Paint.Style.FILL);
        paint.setStrokeCap(Paint.Cap.ROUND);

        bgPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        bgPaint.setStrokeWidth(proHeight);
        bgPaint.setStyle(Paint.Style.FILL);
        bgPaint.setStrokeCap(Paint.Cap.ROUND);
        bgPaint.setColor(bgColor);


    }


    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int specHeightSize = MeasureSpec.getSize(heightMeasureSpec);//高
        int specWidthSize = MeasureSpec.getSize(widthMeasureSpec);
        viewHeight = imageSize;
        viewWidth = specWidthSize;
        L.ee(viewWidth, viewHeight);
        setMeasuredDimension(viewWidth, viewHeight);
    }

    @SuppressLint("DrawAllocation")
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        canvas.drawLine(imageSize / 2.0f, viewHeight / 2.0f, viewWidth-imageSize*0.5f, viewHeight / 2.0f, bgPaint);

        if (proDataList == null || proDataList.size() == 0) {
            return;
        }


        for (int i = 0; i < proDataList.size(); i++) {
            float endY = (viewWidth - imageSize) * proDataList.get(i).pro/100.0f + imageSize * 0.5f;
            LinearGradient shader;
            if (proDataList.get(i).colorEnd) {
                shader = new LinearGradient(0, 0, viewWidth,0 , proDataList.get(i).proColors, null,
                        Shader.TileMode.CLAMP);
            } else {
                shader = new LinearGradient(0, 0, endY,0 , proDataList.get(i).proColors, null,
                        Shader.TileMode.CLAMP);
            }
            paint.setShader(shader);
            canvas.drawLine(imageSize / 2.0f, viewHeight / 2.0f, endY, viewHeight / 2.0f, paint);
        }


        for (int i = 0; i < proDataList.size(); i++) {
            float endY = (viewWidth - imageSize) * proDataList.get(i).pro/100.0f + imageSize * 0.5f;
            Bitmap bitmap = BitmapFactory.decodeResource(getContext().getResources(), proDataList.get(i).drawableId);
            Rect rect = new Rect((int) (endY - imageSize * 0.5f), 0, (int) (endY + imageSize * 0.5f), imageSize);
            canvas.drawBitmap(bitmap, null, rect, null);

        }

    }


    public void setProDataList(List<MultipleProData> proDataList) {
        this.proDataList = proDataList;
        invalidate();
    }

    public List<MultipleProData> getProDataList() {
        return proDataList;
    }

    public static class MultipleProData {
        public int[] proColors;
        public float pro;
        public int drawableId;
        //渐变时按100渐变，false 按进度渐变
        public boolean colorEnd = true;

        public MultipleProData(int[] proColors, float pro, int drawableId) {
            this.proColors =  reviseColors(proColors);
            this.pro = pro;
            this.drawableId = drawableId;
            this.colorEnd = true;

        }

        public   MultipleProData(int[] proColors, float pro, int drawableId, boolean colorEnd) {
            this.proColors = reviseColors(proColors);
            this.pro = pro;
            this.drawableId = drawableId;
            this.colorEnd = colorEnd;

        }
    }

    private static int[] reviseColors(int[] proColors) {
        if(proColors.length==1){
           return new int[]{proColors[0],proColors[0]};
        }else{
            return proColors;
        }
    }


}
