/*
 * Created by inks on  2023/4/17 下午1:39
 * Copyright (c) 2022 北京天恒昕业科技发展有限公司. All rights reserved.
 */

package com.ink.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

/**
 * <pre>
 *     author : inks
 *     time   : 2023/04/17
 *     desc   :
 *     version: 1.0
 * </pre>
 */
public  class InterceptRelativeLayout extends RelativeLayout {
    public InterceptRelativeLayout(Context context) {
        super(context);
    }

    public InterceptRelativeLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public InterceptRelativeLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public InterceptRelativeLayout(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }


    @Override
    protected void dispatchSetPressed(boolean pressed) {
       // super.dispatchSetPressed(pressed);
    }
}
