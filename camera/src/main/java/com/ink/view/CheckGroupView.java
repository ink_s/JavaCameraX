package com.ink.view;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.widget.CheckedTextView;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;

import androidx.core.content.ContextCompat;

import com.ink.camera.R;
import com.ink.camera.util.ClickUtil;
import com.ink.entity.CheckGroupData;

import java.util.ArrayList;
import java.util.List;

/**
 * 单选、多选 按钮组
 *
 *     <com.ink.view.CheckGroupView
 *         android:id="@+id/group_view"
 *         android:layout_width="wrap_content"
 *         android:layout_height="wrap_content"
 *         android:layout_margin="10dp"/>
 *
 *
 *         List<CheckGroupData> test = new ArrayList<>();
 *         for (int i = 0; i < 15; i++) {
 *             CheckGroupData data = new CheckGroupData("测试" + i,""+i,i==0);
 *         }
 *         checkGroupView.initData(test);
 *
 *         checkGroupView.setCheckChangeListener(new CheckGroupView.OnCheckChange() {
 *             @Override
 *             public void onChekChange(View view, int position, boolean checked, List<CheckGroupData> data) {
 *                 if(checked){
 *                     return;
 *                 }
 *                 checkGroupView.checkOnlyOne(position);
 *                 checkGroupView.moveTo(position);
 *             }
 *         });
 *
 *
 */
public class CheckGroupView extends HorizontalScrollView {

    private LinearLayout linearLayout;

    private List<CheckGroupData> dataList;

    private List<CheckedTextView> viewList;

    private int background;
    private ColorStateList textColor;
    private int textSize;
    private int paddingTop;
    private int paddingBottom;
    private int paddingLeft;
    private int paddingRight;
    private int interval;


    public interface OnCheckChange {
        //checked 点击之前的状态
        public void onChekChange(View view, int position, boolean checked, List<CheckGroupData> data);
    }

    private OnCheckChange checkChangeListener;

    public void setCheckChangeListener(OnCheckChange checkChangeListener) {
        this.checkChangeListener = checkChangeListener;
    }

    public CheckGroupView(Context context) {
        this(context, null);
    }

    public CheckGroupView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CheckGroupView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(attrs, defStyle);
    }

    private void init(AttributeSet attrs, int defStyle) {
        // Load attributes
        final TypedArray a = getContext().obtainStyledAttributes(
                attrs, R.styleable.CheckGroupView, defStyle, 0);

        background = a.getResourceId(R.styleable.CheckGroupView_CGV_background, R.drawable.x_common_switch_text_selector_bg_1);
        textColor = a.getColorStateList(R.styleable.CheckGroupView_CGV_textColor);
        textSize = a.getDimensionPixelSize(R.styleable.CheckGroupView_CGV_textSize, sp2px(14));
        paddingTop = a.getDimensionPixelSize(R.styleable.CheckGroupView_CGV_paddingTop, dp2px(3));
        paddingBottom = a.getDimensionPixelSize(R.styleable.CheckGroupView_CGV_paddingBottom, dp2px(3));
        paddingLeft = a.getDimensionPixelSize(R.styleable.CheckGroupView_CGV_paddingLeft, dp2px(16));
        paddingRight = a.getDimensionPixelSize(R.styleable.CheckGroupView_CGV_paddingRight, dp2px(16));
        interval = a.getDimensionPixelSize(R.styleable.CheckGroupView_CGV_div, dp2px(3));
        a.recycle();

        linearLayout = new LinearLayout(getContext());
        linearLayout.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
        linearLayout.setOrientation(LinearLayout.HORIZONTAL);

        addView(linearLayout);

    }

    public void initData(List<CheckGroupData> dataList) {
        this.dataList = dataList;
        initViewList();
    }


    //单选某一项
    public void checkOnlyOne(int position) {
        if (dataList != null) {
            for (int i = 0; i < dataList.size(); i++) {
                if (i != position) {
                    dataList.get(i).checked = false;
                } else {
                    dataList.get(i).checked = true;
                }
            }

        }

        upUI();
    }


    //只更新选中状态(多选或清空选中状态调用），需要更新数据请调用initData
    public void setDataList(List<CheckGroupData> dataList) {
        this.dataList = dataList;
        upUI();
    }

    //重新添加UI并设置选中状态
    private void initViewList() {
        if (dataList == null) {
            dataList = new ArrayList<>();
        }
        linearLayout.removeAllViews();
        viewList = new ArrayList<>();

        for (int i = 0; i < dataList.size(); i++) {
            CheckedTextView textView = new CheckedTextView(getContext());
            textView.setText(dataList.get(i).name);
            textView.setChecked(dataList.get(i).checked);
            textView.setTag(i);
            textView.setOnClickListener(click);
            if (textColor == null) {
                textView.setTextColor(ContextCompat.getColorStateList(getContext(), R.color.x_common_switch_text_selector_color_1));
            } else {
                textView.setTextColor(textColor);
            }
            textView.setBackgroundResource(background);
            textView.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
            textView.setPadding(paddingLeft, paddingTop, paddingRight, paddingBottom);


            LayoutParams layoutParams = new LayoutParams(
                    LayoutParams.WRAP_CONTENT,
                    LayoutParams.WRAP_CONTENT,
                    Gravity.CENTER_VERTICAL);
            layoutParams.setMargins(interval, 2, interval, 2);
            linearLayout.addView(textView, layoutParams);
            viewList.add(textView);
        }
    }


    //只更新选中状态，不重新添加UI
    public void upUI() {
        if (dataList == null) {
            dataList = new ArrayList<>();
        }
        if(viewList == null || viewList.size() != dataList.size()){
            initViewList();
            return;
        }
        for (int i = 0; i < dataList.size(); i++) {
            viewList.get(i).setChecked(dataList.get(i).checked);
        }
    }




    public void moveTo(int index){
        int viewWidth = 0;
        for (int i = 0; i < index; i++) {
            viewWidth = viewWidth + linearLayout.getChildAt(i).getMeasuredWidth()+ interval*2;
        }
        float haveScroll =  getScrollX();
        int allWidth = getMeasuredWidth();
        int nowViewWidth = linearLayout.getChildAt(index).getMeasuredWidth()+interval*2;
        if(((allWidth+haveScroll)<viewWidth+nowViewWidth) || haveScroll>viewWidth){
            //显示不下||左边滚动多了
            scrollTo(viewWidth, 0);
        }
    }





    OnClickListener click = new OnClickListener() {
        @Override
        public void onClick(View v) {
            if (!ClickUtil.isFastDoubleClick((long) (200))) {
                int index = (int) v.getTag();
                //L.ee(index);
                clickIndex(index);
            }
        }
    };


    private void clickIndex(int index) {
        if (checkChangeListener != null) {
            checkChangeListener.onChekChange(this, index, dataList.get(index).checked, dataList);
        }
    }

    public int dp2px(final float dpValue) {
        final float scale = Resources.getSystem().getDisplayMetrics().density;
        return (int) (dpValue * scale + 0.5f);
    }

    public int sp2px(final float spValue) {
        final float fontScale = Resources.getSystem().getDisplayMetrics().scaledDensity;
        return (int) (spValue * fontScale + 0.5f);
    }


}