package com.ink.view;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.viewpager.widget.ViewPager;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.ink.camera.MediaPreviewActivity;
import com.ink.camera.R;
import com.ink.camera.adapter.ViewLoopAdapter;
import com.ink.camera.entity.MediaBean;
import com.ink.camera.util.ClickUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * <pre>
 *     author : inks
 *     time   : 2024/04/28
 *     desc   :
 *     version: 1.0
 * </pre>
 */
public class ImageLoopView extends FrameLayout {


    private ArrayList<MediaBean> data = new ArrayList<MediaBean>();
    private List<View> views = new ArrayList<View>();

    private ViewPager viewPager;
    private TextView noDta;
    private IndicatorGroup indicatorGroup;

    public ViewLoopAdapter viewLoopAdapter;
    private int mViewPagerIndex;

    private boolean isInit = false;

    private Activity activity;


    public void setData(ArrayList<MediaBean> data,Activity activity) {
        this.data = data;
        this.activity = activity;
        if(isInit){
            try {
                activity.runOnUiThread(new Runnable(){
                    @Override
                    public void run() {
                        initData();
                    }
                });
            }catch (Exception e){
                e.printStackTrace();
            }


        }

    }


    public ImageLoopView(@NonNull Context context) {
        this(context, null);
    }

    public ImageLoopView(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ImageLoopView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        this(context, attrs, defStyleAttr, 0);
    }

    public ImageLoopView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    public void init() {
        View mView = LayoutInflater.from(getContext()).inflate(R.layout.x_view_image_loop, null);
        viewPager = mView.findViewById(R.id.view_page);
        noDta = mView.findViewById(R.id.no_data);
        indicatorGroup = mView.findViewById(R.id.indicator_group);


        removeAllViews();
        addView(mView);
        initData();
        isInit = true;
    }




    @SuppressLint("CheckResult")
    private void initData() {
        views.clear();
        if (data == null || data.size() == 0) {
            noDta.setVisibility(View.VISIBLE);
            viewPager.setVisibility(View.GONE);
            indicatorGroup.setVisibility(View.GONE);
            return;
        } else if (data.size() == 1) {
            //一张图片，不能滑动
            ImageView view = null;
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                view = (ImageView) LayoutInflater.from(getContext()).inflate(R.layout.x_image_item, null);
            }else{
                view = new ImageView(getContext());
                view.setScaleType(ImageView.ScaleType.CENTER_CROP);
            }
            Glide.with(getContext())
                    .load(data.get(0).path)
                    .skipMemoryCache(true)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .placeholder(R.drawable.x_dialong_loading_mid)
                    .error(R.drawable.x_basic_no_image)
                    .centerCrop()
                    .into(view);
            view.setTag(0);
            views.add(view);

        } else {
            ImageView viewHead = null;
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                viewHead =(ImageView) LayoutInflater.from(getContext()).inflate(R.layout.x_image_item, null);
            }else{
                viewHead = new ImageView(getContext());
                viewHead.setScaleType(ImageView.ScaleType.CENTER_CROP);
            }
            Glide.with(getContext())
                    .load(data.get(data.size() - 1).path)
                    .skipMemoryCache(true)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .placeholder(R.drawable.x_dialong_loading_mid)
                    .error(R.drawable.x_basic_no_image)
                    .centerCrop()
                    .into(viewHead);
            viewHead.setTag(data.size() - 1);
            views.add(viewHead);
            for (int i = 0; i < data.size(); i++) {
                ImageView view = null;
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                    view = (ImageView) LayoutInflater.from(getContext()).inflate(R.layout.x_image_item, null);
                }else{
                    view = new ImageView(getContext());
                    view.setScaleType(ImageView.ScaleType.CENTER_CROP);
                }
                Glide.with(getContext())
                        .load(data.get(i).path)
                        .skipMemoryCache(true)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .placeholder(R.drawable.x_dialong_loading_mid)
                        .error(R.drawable.x_basic_no_image)
                        .centerCrop()
                        .into(view);
                view.setTag(i);
                views.add(view);
            }

            ImageView viewLast = null;
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                viewLast = (ImageView) LayoutInflater.from(getContext()).inflate(R.layout.x_image_item, null);
            }else{
                viewLast = new ImageView(getContext());
                viewLast.setScaleType(ImageView.ScaleType.CENTER_CROP);
            }
            Glide.with(getContext())
                    .load(data.get(0).path)
                    .skipMemoryCache(true)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .placeholder(R.drawable.x_dialong_loading_mid)
                    .error(R.drawable.x_basic_no_image)
                    .centerCrop()
                    .into(viewLast);
            viewLast.setTag(0);
            views.add(viewLast);


        }


        noDta.setVisibility(View.GONE);
        viewPager.setVisibility(View.VISIBLE);


        viewLoopAdapter = new ViewLoopAdapter(views, viewPager, getContext());
        viewPager.setAdapter(viewLoopAdapter);


        if (views.size() == 1) {
            viewPager.setCurrentItem(0);
            indicatorGroup.setVisibility(View.GONE);
        } else {
            viewPager.setCurrentItem(1);
            indicatorGroup.setVisibility(View.VISIBLE);
            mViewPagerIndex = 1;
            indicatorGroup.setCount(views.size() - 2);
            indicatorGroup.setSelectIndex(mViewPagerIndex - 1);
            mHandler.removeMessages(111);
            mHandler.sendEmptyMessageDelayed(111, 2000);
            viewPager.setOnPageChangeListener(pageChangeListener1);
        }

        for (int i = 0; i < views.size(); i++) {
            views.get(i).setOnClickListener(click);

        }
    }

    View.OnClickListener click = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (ClickUtil.isFastDoubleClick((long) (500))) {
                return;
            }
            if (views == null || views.size() == 0) {
                return;
            }

            int index = 0;
            try {
                index = (int) v.getTag();
            } catch (Exception e) {
                e.printStackTrace();
            }
            Intent intent = new Intent(getContext(), MediaPreviewActivity.class);
            intent.putParcelableArrayListExtra(MediaPreviewActivity.MEDIA_DATA, data);
            intent.putExtra(MediaPreviewActivity.INDEX, index);
            getContext().startActivity(intent);

        }
    };


    ViewPager.OnPageChangeListener pageChangeListener1 = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {


            //position等于当前选中的，向右滑（下一张是右边的）
            //position小于当前选中的，向左滑（下一张是左边的）
            if (position == mViewPagerIndex) {
                if (position == views.size() - 2) {
                    if (positionOffset == 0.0f) {
                        indicatorGroup.setProgress(positionOffset, 2, 0);
                    }
                } else {
                    indicatorGroup.setProgress(positionOffset, 2, mViewPagerIndex - 1);
                }

            } else if (position < mViewPagerIndex) {
                if (mViewPagerIndex - position == 1) {
                    if (position == 0) {
                        if (positionOffset == 0.0f) {
                            indicatorGroup.setProgress(positionOffset, 2, views.size() - 2);
                        }
                    } else {
                        indicatorGroup.setProgress(1 - positionOffset, 1, mViewPagerIndex - 1);
                    }
                } else {
                    indicatorGroup.setProgress(1 - positionOffset, 1, position);
                }

            } else {
                indicatorGroup.setProgress(1 - positionOffset, 1, position);
            }
        }

        @Override
        public void onPageSelected(int position) {
        }

        @Override
        public void onPageScrollStateChanged(int state) {
            if (state == 1) {
                mViewPagerIndex = viewPager.getCurrentItem();
                mHandler.removeMessages(111);
            } else if (state == 0) {
                mViewPagerIndex = viewPager.getCurrentItem();
                //滑动结束
                mHandler.sendEmptyMessage(222);
                mHandler.removeMessages(111);
                mHandler.sendEmptyMessageDelayed(111, 3000);

            }

        }
    };


    @SuppressLint("HandlerLeak")
    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            try {
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        switch (msg.what) {
                            case 111:
                                viewPager.setCurrentItem(viewPager.getCurrentItem() + 1, true);
                                break;
                            case 222:
                                int positions = viewPager.getCurrentItem();
                                //Log.e("position", positions + "");

                                if (positions == 0) {
                                    positions = views.size() - 2;
                                    viewPager.setCurrentItem(positions, false);
                                } else if (positions == views.size() - 1) {
                                    positions = 1;
                                    viewPager.setCurrentItem(positions, false);
                                }

                                break;
                        }
                    }
                });
            }catch (Exception e){
                e.printStackTrace();
            }

        }
    };


    @Override
    protected void onDetachedFromWindow() {
        mHandler.removeCallbacksAndMessages(null);
        super.onDetachedFromWindow();

    }
}
