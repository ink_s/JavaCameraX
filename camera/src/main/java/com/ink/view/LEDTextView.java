package com.ink.view;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

import androidx.annotation.Nullable;

/**
 * <pre>
 *     author : inks
 *     time   : 2023/06/09
 *     desc   : LEDTextView
 *     version: 1.0
 * </pre>
 */
public class LEDTextView extends androidx.appcompat.widget.AppCompatTextView {

  private   Typeface typeface;//定义字体

    public LEDTextView(Context context) {
        this(context, null);
    }

    public LEDTextView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, android.R.attr.textViewStyle);
    }

    public LEDTextView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        typeface = Typeface.createFromAsset(context.getAssets(), "fonts/digital-7-4.ttf");//初始化字体
        setTypeface(typeface);

    }

}
