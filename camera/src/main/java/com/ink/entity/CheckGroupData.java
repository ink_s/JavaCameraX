package com.ink.entity;

/**
 * <pre>
 *     author : inks
 *     time   : 2024/02/19
 *     desc   :
 *     version: 1.0
 * </pre>
 */
public class CheckGroupData {
    public String name;
    public String code;
    public boolean checked;

    public CheckGroupData() {
    }

    public CheckGroupData(String name) {
        this.name = name;
    }

    public CheckGroupData(String name, String code){
        this.name = name;
        this.code = code;
    }

    public CheckGroupData(String name, String code,boolean checked){
        this.name = name;
        this.code = code;
        this.checked = checked;
    }
}
