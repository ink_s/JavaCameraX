package com.ink.camera;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.camera.core.Camera;
import androidx.camera.core.CameraSelector;
import androidx.camera.core.FocusMeteringAction;
import androidx.camera.core.ImageCapture;
import androidx.camera.core.ImageCaptureException;
import androidx.camera.core.Preview;
import androidx.camera.core.VideoCapture;
import androidx.camera.core.impl.utils.executor.CameraXExecutors;
import androidx.camera.lifecycle.ProcessCameraProvider;
import androidx.camera.view.PreviewView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.google.common.util.concurrent.ListenableFuture;
import com.ink.camera.popup.PopupView;
import com.ink.camera.view.LongClickView;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.concurrent.ExecutionException;

public class CameraActivity extends AppCompatActivity {
    public static final int ACTIVITY_LOAD = 1;
    private ListenableFuture<ProcessCameraProvider> mProcessCameraProviderListenableFuture;
    private ProcessCameraProvider mProcessCameraProvider;
    private Preview mPreview;
    private CameraSelector mCameraSelector;
    private ImageCapture mImageCapture;
    private VideoCapture mVideoCapture;
    private Camera mCamera;

    private boolean isFront = false;
    private boolean mIsLessOneMin = false;

    private PreviewView mPreviewView;
    private LongClickView mLongClickView;

    private PopupView popupView;

    private String saveDir;
    private String savePath;
    private String saveType;
    //录屏开始时间
    private long startTime;
    private boolean haveImageCapture = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ink_activity_camera);
        initView();
        initData();
        popupView = new PopupView();
        mHandler.removeMessages(ACTIVITY_LOAD);
        mHandler.sendEmptyMessageDelayed(ACTIVITY_LOAD, 200);
    }


    @SuppressLint("ClickableViewAccessibility")
    private void initView() {
        mPreviewView = findViewById(R.id.preview);
        mLongClickView = findViewById(R.id.LongClickView);
        mPreviewView.setOnTouchListener(mTouchListener);
        mLongClickView.setMyClickListener(mClickListener);


    }

    private void initData() {
        savePath = getIntent().getStringExtra("savePath");
        if (TextUtils.isEmpty(savePath)) {
            saveDir = getAutoPath();
        }

    }

    public void initWindow() {
        // 请求相机权限
        if (allPermissionsGranted()) {
            initCamera();
        } else {
            ActivityCompat.requestPermissions(this, Configuration.REQUIRED_PERMISSIONS,
                    Configuration.REQUEST_CODE_PERMISSIONS);
        }

    }

    private void initCamera() {

        mProcessCameraProviderListenableFuture = ProcessCameraProvider.getInstance(this);
        mProcessCameraProviderListenableFuture.addListener(() -> {
            try {
                mProcessCameraProvider = mProcessCameraProviderListenableFuture.get();
                bindPreview(mProcessCameraProvider);
            } catch (ExecutionException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }, ContextCompat.getMainExecutor(this));
    }


    @SuppressLint("RestrictedApi")
    private void bindPreview(ProcessCameraProvider processCameraProvider) {
        //创建preview
        mPreview = new Preview.Builder().build();
        //指定所需的相机选项,设置摄像头镜头切换
        mCameraSelector = new CameraSelector.Builder().requireLensFacing(isFront ? CameraSelector.LENS_FACING_FRONT :
                CameraSelector.LENS_FACING_BACK).build();
        //将 Preview 连接到 PreviewView。
        mPreview.setSurfaceProvider(mPreviewView.getSurfaceProvider());
        //将所选相机和任意用例绑定到生命周期。

        mImageCapture = new ImageCapture.Builder()
                .setTargetRotation(mPreviewView.getDisplay().getRotation())
                .build();

        mVideoCapture = new VideoCapture.Builder()
                .setTargetRotation(mPreviewView.getDisplay().getRotation())
                .setVideoFrameRate(25)//每秒的帧数
                .setBitRate(3 * 1024 * 1024)//设置每秒的比特率
                .build();

        try {
            processCameraProvider.unbindAll();
            mCamera = processCameraProvider.bindToLifecycle(this, mCameraSelector,
                    mImageCapture, mVideoCapture, mPreview);
            haveImageCapture = true;
        } catch (Exception e) {
            processCameraProvider.unbindAll();
            mCamera = processCameraProvider.bindToLifecycle(this, mCameraSelector,
                    mVideoCapture, mPreview);
            haveImageCapture = false;
        }

    }


    @SuppressLint("RestrictedApi")
    private void takePhoto() {
        String path = getFilePath(true);
        ImageCapture.OutputFileOptions outputFileOptions = new ImageCapture.OutputFileOptions
                .Builder(new File(path)).build();
        mImageCapture.takePicture(outputFileOptions, CameraXExecutors.mainThreadExecutor(), new ImageCapture.OnImageSavedCallback() {
            @Override
            public void onImageSaved(@NonNull ImageCapture.OutputFileResults outputFileResults) {
                sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.fromFile(new File(path))));
                savePath = path;
                popup();
            }

            @Override
            public void onError(@NonNull ImageCaptureException exception) {
                Log.e("exception", exception.getMessage().toString());
                Toast.makeText(CameraActivity.this, exception.toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }


    private void takePhoto2() {
        String path = getFilePath(true);
        Bitmap bitmap = mPreviewView.getBitmap();
        saveBitmap(bitmap, path);
    }


    /**
     * 保存Bitmap
     */
    public void saveBitmap(Bitmap bm, String filepath) {
        File f = new File(filepath);
        if (f.exists()) {
            f.delete();
        }
        try {
            FileOutputStream out = new FileOutputStream(f);
            bm.compress(Bitmap.CompressFormat.JPEG, 90, out);
            out.flush();
            out.close();
            sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.fromFile(new File(filepath))));
            savePath = filepath;
            popup();
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        bm.recycle();
    }

    @SuppressLint("RestrictedApi")
    private void startVideo() {
        String path = getFilePath(false);
        VideoCapture.OutputFileOptions build = new VideoCapture.OutputFileOptions.Builder(new File(path)).build();
        mVideoCapture.startRecording(build, CameraXExecutors.mainThreadExecutor(), new VideoCapture.OnVideoSavedCallback() {
            @Override
            public void onVideoSaved(@NonNull VideoCapture.OutputFileResults outputFileResults) {
                if (mIsLessOneMin) {
                    new File(path).delete();
                } else {
                    Log.e("视频已保存", outputFileResults.getSavedUri().getPath());
                    sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.fromFile(new File(path))));
                    savePath = path;
                    popup();

                }
            }

            @Override
            public void onError(int videoCaptureError, @NonNull String message, @Nullable Throwable cause) {
                Log.e("TAG", "onError: " + message);
                new File(path).delete();//视频不足一秒会走到这里来，但是视频依然生成了，所以得删掉
            }
        });
    }

    @SuppressLint("RestrictedApi")
    private void stopVideo() {
        mVideoCapture.stopRecording();
    }

    View.OnTouchListener mTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {
            float x = motionEvent.getX();
            float y = motionEvent.getY();
            FocusMeteringAction focusMeteringAction = new FocusMeteringAction.Builder(mPreviewView.getMeteringPointFactory()
                    .createPoint(x, y)).build();
            if (mCamera != null) {
                mCamera.getCameraControl().startFocusAndMetering(focusMeteringAction);
            }
            return true;
        }
    };
    LongClickView.MyClickListener mClickListener = new LongClickView.MyClickListener() {
        @Override
        public void longClickFinish() {
            Log.e("mClickListener", "longClickFinish");
            if ((System.currentTimeMillis() - startTime) < 1000) {
                mIsLessOneMin = true;
            }
            stopVideo();
        }

        @Override
        public void longClickStart() {
            Log.e("mClickListener", "longClickStart");
            mIsLessOneMin = false;
            startTime = System.currentTimeMillis();
            startVideo();
        }

        @Override
        public void singleClickFinish() {
            Log.e("mClickListener", "singleClickFinish");
            // takePhoto();
            if(haveImageCapture){
                Log.e("takePhoto1","takePhoto1");
                takePhoto();
            }else{
                Log.e("takePhoto2","takePhoto2");
                takePhoto2();
            }
        }
    };


    private void popup() {
        popupView.popupView(getWindow(), getBaseContext(), null, popupClick);
    }

    PopupView.OnClickListener popupClick = new PopupView.OnClickListener() {
        @Override
        public void onYesBack() {
            Intent intent = getIntent();
            intent.putExtra("savePath", savePath);
            setResult(RESULT_OK, intent);
            finish();
        }

        @Override
        public void onCancelBack() {
            Intent intent = getIntent();
            setResult(RESULT_CANCELED, intent);
            finish();
        }

        @Override
        public void onAgainBack() {
        }
    };


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == Configuration.REQUEST_CODE_PERMISSIONS) {
            if (allPermissionsGranted()) {// 申请权限通过
                initCamera();
            } else {// 申请权限失败
                Toast.makeText(this, "用户拒绝授予权限！", Toast.LENGTH_LONG).show();
                finish();
            }
        }
    }

    private boolean allPermissionsGranted() {
        for (String permission : Configuration.REQUIRED_PERMISSIONS) {
            if (ContextCompat.checkSelfPermission(this, permission)
                    != PackageManager.PERMISSION_GRANTED) {
                return false;
            }
        }
        return true;
    }

    static class Configuration {
        public static final String TAG = "CameraxBasic";
        public static final String FILENAME_FORMAT = "yyyy-MM-dd-HH-mm-ss-SSS";
        public static final int REQUEST_CODE_PERMISSIONS = 10;
        public static final String[] REQUIRED_PERMISSIONS = new String[]{Manifest.permission.CAMERA,
                Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.RECORD_AUDIO};
    }


    private String getAutoPath() {
        String path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM) + File.separator + "CameraX";
        File file = new File(path);
        if (!file.exists()) {
            file.mkdirs();
        }
        return file.getPath();
    }


    // private String getSystem
    private String getFilePath(boolean isImage) {
        String path;
        File file = new File(saveDir);
        if (!file.exists()) {
            file.mkdirs();
        }
        if (isImage) {
            path = saveDir + File.separator + System.currentTimeMillis() + ".jpg";
        } else {
            path = saveDir + File.separator + System.currentTimeMillis() + ".mp4";
        }
        return path;

    }

    @SuppressLint("HandlerLeak")
    private Handler mHandler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (msg.what == ACTIVITY_LOAD) {
                if (null != getWindow().getDecorView().getWindowToken()) {
                    mHandler.removeMessages(ACTIVITY_LOAD);
                    //初始化完成
                    initWindow();

                } else {
                    mHandler.removeMessages(ACTIVITY_LOAD);
                    mHandler.sendEmptyMessageDelayed(ACTIVITY_LOAD, 200);
                }
            }
        }
    };

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}