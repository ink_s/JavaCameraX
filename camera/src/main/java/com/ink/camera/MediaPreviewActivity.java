/*
 * Created by inks on  2022/11/14 下午1:50
 *
 */

package com.ink.camera;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager2.widget.ViewPager2;

import com.ink.camera.adapter.MediaPagerAdapter;
import com.ink.camera.entity.MediaBean;
import com.ink.camera.fragment.VideoPreviewFragment;
import com.ink.download.AppDownloadActivity;
import com.ink.download.BaseDownloadActivity;
import com.ink.util.ZipUtils;
import com.inks.inkslibrary.Utils.ClickUtil;
import com.inks.inkslibrary.Utils.L;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class MediaPreviewActivity extends AppCompatActivity {
    public static final String MEDIA_DATA = "data";
    public static final String INDEX = "index";

    private boolean sensorFlag = true;
    private boolean stretchFlag = true;
    private SensorManager sm;
    private OrientationSensorListener listener;
    private Sensor sensor;

    private SensorManager sm1;
    private Sensor sensor1;
    private OrientationSensorListener2 listener1;


    private MediaPagerAdapter adapter;

    private List<MediaBean> data = new ArrayList<MediaBean>();
    private int initIndex;
    private ViewPager2 viewPager;

    private ImageView back;
    private TextView indexText;
    private TextView pageTitle;

    private    String savePath;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.x_activity_media_preview);

         savePath  = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM) + File.separator + "download";

        data = getIntent().getParcelableArrayListExtra(MEDIA_DATA);

        initIndex = getIntent().getIntExtra(INDEX, 0);
        L.ee("initIndex", initIndex);
        if (data == null || data.size() == 0) {
            finish();
            return;
        }

        back = findViewById(R.id.back);
        back.setOnClickListener(click);
        indexText = findViewById(R.id.page_index);
        indexText.setText((initIndex + 1) + "/" + data.size());

        pageTitle = findViewById(R.id.page_title);
        pageTitle.setText(data.get(initIndex).title);

        viewPager = findViewById(R.id.view_page);
        viewPager.registerOnPageChangeCallback(pageChange);
        adapter = new MediaPagerAdapter(this, data);
        viewPager.setAdapter(adapter);
        viewPager.setCurrentItem(initIndex, false);


        //注册重力感应器  屏幕旋转
        sm = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        sensor = sm.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        listener = new OrientationSensorListener(handler);
        sm.registerListener(listener, sensor, SensorManager.SENSOR_DELAY_UI);


        //根据  旋转之后 点击 符合之后 激活sm
        sm1 = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        sensor1 = sm1.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        listener1 = new OrientationSensorListener2();
        sm1.registerListener(listener1, sensor1, SensorManager.SENSOR_DELAY_UI);


    }


    View.OnClickListener click = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (!ClickUtil.isFastDoubleClick((long) (500))) {
                if (v.getId() == R.id.back) {
                    onBackPressed();
                }

            }
        }
    };
    ViewPager2.OnPageChangeCallback pageChange = new ViewPager2.OnPageChangeCallback() {
        @Override
        public void onPageSelected(int position) {
            super.onPageSelected(position);
            indexText.setText((position + 1) + "/" + data.size());
            pageTitle.setText(data.get(position).title);
        }
    };


    @SuppressLint("HandlerLeak")
    private Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 888:
                    int orientation = msg.arg1;
                    if (orientation > 45 && orientation < 135) {

                    } else if (orientation > 135 && orientation < 225) {

                    } else if (orientation > 225 && orientation < 315) {
                        System.out.println("切换成横屏");
                        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                        sensorFlag = false;
                        stretchFlag = false;

                    } else if ((orientation > 315 && orientation < 360) || (orientation > 0 && orientation < 45)) {
                        System.out.println("切换成竖屏");
                        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                        sensorFlag = true;
                        stretchFlag = true;
                    }
                    break;
                default:
                    break;
            }

        }
    };


    public void fullscreenClick() {
        sm.unregisterListener(listener);
        if (stretchFlag) {
            stretchFlag = false;
            //切换成横屏
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        } else {
            stretchFlag = true;
            //切换成竖屏
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
    }

    public void download(String path) {
        L.ee("download", path);
        if(TextUtils.isEmpty(path)){
            return;
        }
        int lastSlashIndex = path.lastIndexOf('/');
        String filename;

        try {
            filename   =   path.substring(lastSlashIndex + 1);
        }catch (Exception e){
            e.printStackTrace();
            filename = System.currentTimeMillis()+"."+ ZipUtils.getExtensionName(path);
        }


        File file = new File(savePath);
        if (!file.exists()) {
            file.mkdirs();
        }
        Intent intent = new Intent(this, BaseDownloadActivity.class);
        intent.putExtra(BaseDownloadActivity.LOAD_FILE_SAVE_PARENT, savePath);
        intent.putExtra(BaseDownloadActivity.LOAD_FILE_URL, path);
        intent.putExtra(BaseDownloadActivity.LOAD_FILE_NAME, filename );
        intent.putExtra(BaseDownloadActivity.LOAD_FILE_RE, true);
        intent.putExtra(BaseDownloadActivity.LOAD_OPEN, false);
        intent.putExtra(BaseDownloadActivity.LOAD_SHOW_PATH, true);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
       startActivity(intent);

    }


    /**
     * 重力感应监听者
     */
    public class OrientationSensorListener implements SensorEventListener {
        private static final int _DATA_X = 0;
        private static final int _DATA_Y = 1;
        private static final int _DATA_Z = 2;

        public static final int ORIENTATION_UNKNOWN = -1;

        private Handler rotateHandler;

        public OrientationSensorListener(Handler handler) {
            rotateHandler = handler;
        }

        public void onAccuracyChanged(Sensor arg0, int arg1) {
            // TODO Auto-generated method stub

        }

        public void onSensorChanged(SensorEvent event) {

            if (sensorFlag != stretchFlag)  //只有两个不相同才开始监听行为
            {
                float[] values = event.values;
                int orientation = ORIENTATION_UNKNOWN;
                float X = -values[_DATA_X];
                float Y = -values[_DATA_Y];
                float Z = -values[_DATA_Z];
                float magnitude = X * X + Y * Y;
                // Don't trust the angle if the magnitude is small compared to the y value
                if (magnitude * 4 >= Z * Z) {
                    //屏幕旋转时
                    float OneEightyOverPi = 57.29577957855f;
                    float angle = (float) Math.atan2(-Y, X) * OneEightyOverPi;
                    orientation = 90 - (int) Math.round(angle);
                    // normalize to 0 - 359 range
                    while (orientation >= 360) {
                        orientation -= 360;
                    }
                    while (orientation < 0) {
                        orientation += 360;
                    }
                }
                if (rotateHandler != null) {
                    rotateHandler.obtainMessage(888, orientation, 0).sendToTarget();
                }

            }
        }
    }


    public class OrientationSensorListener2 implements SensorEventListener {
        private static final int _DATA_X = 0;
        private static final int _DATA_Y = 1;
        private static final int _DATA_Z = 2;

        public static final int ORIENTATION_UNKNOWN = -1;

        public void onAccuracyChanged(Sensor arg0, int arg1) {
            // TODO Auto-generated method stub

        }

        public void onSensorChanged(SensorEvent event) {

            float[] values = event.values;

            int orientation = ORIENTATION_UNKNOWN;
            float X = -values[_DATA_X];
            float Y = -values[_DATA_Y];
            float Z = -values[_DATA_Z];

            float magnitude = X * X + Y * Y;
            // Don't trust the angle if the magnitude is small compared to the y value
            if (magnitude * 4 >= Z * Z) {
                //屏幕旋转时
                float OneEightyOverPi = 57.29577957855f;
                float angle = (float) Math.atan2(-Y, X) * OneEightyOverPi;
                orientation = 90 - (int) Math.round(angle);
                // normalize to 0 - 359 range
                while (orientation >= 360) {
                    orientation -= 360;
                }
                while (orientation < 0) {
                    orientation += 360;
                }
            }

            if (orientation > 225 && orientation < 315) {  //横屏
                sensorFlag = false;
            } else if ((orientation > 315 && orientation < 360) || (orientation > 0 && orientation < 45)) {  //竖屏
                sensorFlag = true;
            }

            if (stretchFlag == sensorFlag) {  //点击变成横屏  屏幕 也转横屏 激活
                sm.registerListener(listener, sensor, SensorManager.SENSOR_DELAY_UI);

            }
        }
    }


    @Override
    public void onDestroy() {
        if (sm != null) {
            sm.unregisterListener(listener);
        }
        if (sm1 != null) {
            sm1.unregisterListener(listener1);
        }
        if(viewPager!=null){
            viewPager.unregisterOnPageChangeCallback(pageChange);
        }

        super.onDestroy();
    }
}