package com.ink.camera;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.MediaController;
import android.widget.VideoView;

public class PreviewVideoActivity extends AppCompatActivity {

    private VideoView videoView;
    private String videoPath;
    private boolean showMediaController = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preview_video);
        videoPath = getIntent().getStringExtra("videoPath");
        showMediaController = getIntent().getBooleanExtra("showMediaController",true);
        if(TextUtils.isEmpty(videoPath)){
            finish();
        }

        videoView = findViewById(R.id.VideoView);
        videoView.setVideoPath(videoPath);
        if(showMediaController){
            videoView.setMediaController(new MediaController(this));
        }
        videoView.start();
    }



    public void click(View view) {

        onBackPressed();


    }

    @Override
    protected void onResume() {
        super.onResume();
        videoView.start();
    }
}