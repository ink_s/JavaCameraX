package com.ink.camera;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;

import com.ink.record.SoundRecordingActivity;
import com.inks.inkslibrary.Utils.ClickUtil;
import com.inks.inkslibrary.Utils.L;
import com.inks.inkslibrary.Utils.T;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;


/**
 * <pre>
 *     author : inks
 *     time   : 2022/7/18
 *     desc   : 调用系统相机，选择拍照或录视频
 *     version: 1.0
 * </pre>
 */
public class SystemCameraActivity extends AppCompatActivity {
    public static final int ACTIVITY_LOAD = 1;

    public static final int CAMERA_REQUEST_IMAGE_CODE = 101;
    public static final int CAMERA_REQUEST_VIDEO_CODE = 102;

    public static String MAX_FILE_SIZE = "MaxFileSize";
    //大小限制（KB）
    private long maxFileSize = 200 * 1024;

    //完成后保存的地址
    private String savePath;

    //设置保存的文件夹，不带文件名
    private String saveDir;
    //只是拍照
    private boolean onlyImage = false;

    //可以录音
    private boolean audioEnable = false;


    //回调
    ActivityResultLauncher<Intent> imageResultLauncher;
    ActivityResultLauncher<Intent> videoResultLauncher;
    ActivityResultLauncher<Intent> audioResultLauncher;

    private LinearLayout chooseLayout;
    private LinearLayout audioLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.x_activity_system_camera);
        setupActivityResultLaunchers();
        saveDir = getIntent().getStringExtra("saveDir");
        onlyImage = getIntent().getBooleanExtra("onlyImage", false);
        audioEnable = getIntent().getBooleanExtra("audioEnable", false);
        maxFileSize = getIntent().getLongExtra(MAX_FILE_SIZE, 200 * 1024);

        audioLayout = findViewById(R.id.layout_audio);
        if (audioEnable) {
            audioLayout.setVisibility(View.VISIBLE);
        } else {
            audioLayout.setVisibility(View.GONE);
        }

        mHandler.removeMessages(ACTIVITY_LOAD);
        mHandler.sendEmptyMessageDelayed(ACTIVITY_LOAD, 100);
        chooseLayout = findViewById(R.id.choose_layout);

        if (onlyImage) {
            chooseLayout.setVisibility(View.GONE);
        }


        // 请求相机权限
        if (allPermissionsGranted()) {
            if (onlyImage) {
                openCameraImage();
            }
        } else {
            ActivityCompat.requestPermissions(this, CameraActivity.Configuration.REQUIRED_PERMISSIONS,
                    CameraActivity.Configuration.REQUEST_CODE_PERMISSIONS);
        }


    }


    public void initWindow() {


    }


    public void click(View view) {
        if (!ClickUtil.isFastDoubleClick((long) 500)) {
            if (view.getId() == R.id.layout_image) {
                //拍照
                openCameraImage();
            } else if (view.getId() == R.id.layout_video) {
                //录像
                openCameraVideo();
            } else if (view.getId() == R.id.layout_audio) {
                //录音
                openCameraAudio();
            }
        }
    }


    /**
     * @throws
     * @Description: 调用系统相机拍照
     * @param:
     * @return:
     * @date: 2022/7/18
     */
    private void openCameraImage() {
        Intent captureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        try {
            File photoFile = null;
            Uri photoUri = null;

            photoFile = getFilePath(true);
            savePath = photoFile.getPath();

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                //适配Android 7.0文件权限，通过FileProvider创建一个content类型的Uri
                photoUri = FileProvider.getUriForFile(getApplicationContext(), getApplicationContext().getPackageName() + ".fileProvider", photoFile);
            } else {
                photoUri = Uri.fromFile(photoFile);
            }


            if (photoUri != null) {
                captureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
                captureIntent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                imageResultLauncher.launch(captureIntent);
            }
        } catch (Exception e) {
            e.printStackTrace();
            finish();
        }
    }


    /**
     * @throws
     * @Description: 调用系统相机录像
     * @param:
     * @return:
     * @date: 2022/7/18
     */
    private void openCameraVideo() {
        Intent captureIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
        try {
            File videoFile = null;
            Uri videoUri = null;

            videoFile = getFilePath(false);
            savePath = videoFile.getPath();

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                //适配Android 7.0文件权限，通过FileProvider创建一个content类型的Uri
                videoUri = FileProvider.getUriForFile(getApplicationContext(), getApplicationContext().getPackageName() + ".fileProvider", videoFile);
            } else {
                videoUri = Uri.fromFile(videoFile);
            }


            if (videoUri != null) {
                captureIntent.putExtra(MediaStore.EXTRA_OUTPUT, videoUri);
                captureIntent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                //设置视频录制的最长时间
                captureIntent.putExtra(MediaStore.EXTRA_DURATION_LIMIT, 15);
                //设置视频录制的画质
                captureIntent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 1);
                videoResultLauncher.launch(captureIntent);
            }
        } catch (Exception e) {
            e.printStackTrace();
            finish();
        }
    }


    private void openCameraAudio() {

        if (TextUtils.isEmpty(saveDir)) {
            saveDir = getAutoPath();
        }

        savePath = saveDir + File.separator + System.currentTimeMillis() + "_rc.wav";

        Intent intent = new Intent(this, SoundRecordingActivity.class);
        intent.putExtra(SoundRecordingActivity.RECORD_FILE_PATH, savePath);
        audioResultLauncher.launch(intent);
    }


    /**
     * @throws
     * @Description: 获取默认的保存地址
     * @param: isImage 是否是图片
     * @return: File File
     * @date: 2022/7/18
     */
    private File getFilePath(boolean isImage) {

        String dir = saveDir;
        if (TextUtils.isEmpty(dir)) {
            dir = getAutoPath();
        }

        String path;
        if (isImage) {
            path = dir + File.separator + System.currentTimeMillis() + ".jpg";
        } else {
            path = dir + File.separator + System.currentTimeMillis() + ".mp4";
        }
        return new File(path);

    }

    private String getAutoPath() {
        String path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM) + File.separator + "CameraX";
        File file = new File(path);
        if (!file.exists()) {
            file.mkdirs();
        }
        return file.getPath();
    }


    /**
     * @throws
     * @Description: 设置回调监听
     * @param:
     * @return:
     * @date: 2022/7/18
     */
    private void setupActivityResultLaunchers() {
        imageResultLauncher = registerForActivityResult(
                new ActivityResultContracts.StartActivityForResult(),
                result -> {
                    if (result.getResultCode() == Activity.RESULT_OK) {
                        //设置了保存的Url，就不会返回照片路径
                        Intent data = result.getData();
                        handleImage(data);
                    } else {
                        finish();
                    }
                });

        videoResultLauncher = registerForActivityResult(
                new ActivityResultContracts.StartActivityForResult(),
                result -> {
                    if (result.getResultCode() == Activity.RESULT_OK) {
                        Intent data = result.getData();
                        handleVideo(data);
                    } else {
                        finish();
                    }
                });

        audioResultLauncher = registerForActivityResult(
                new ActivityResultContracts.StartActivityForResult(),
                result -> {
                    if (result.getResultCode() == Activity.RESULT_OK) {
                        Intent data = result.getData();
                        handleAudio(data);
                    } else {
                        finish();
                    }
                });
    }


    /**
     * @throws
     * @Description: 拍照回调
     * @param:
     * @return:
     * @date: 2022/7/18
     */
    private void handleImage(Intent data) {
        activityBack(true);
    }

    /**
     * @throws
     * @Description: 录像回调
     * @param:
     * @return:
     * @date: 2022/7/18
     */
    private void handleVideo(Intent data) {

        activityBack(false);
    }

    private void handleAudio(Intent data) {

        activityBack(false);
    }


    private void activityBack(boolean isImage) {
        L.ee(savePath);
        if (savePath != null) {

            String newFilePath = copyNewFile(savePath);
            L.ee(newFilePath);
            sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.fromFile(new File(newFilePath))));

            File file = new File(newFilePath);
            if (!file.exists() || !file.isFile()) {
                finish();
                return;
            }
            if (file.length() > maxFileSize * 1024) {
                T.showShort(this, "文件大小超过限制！");
                finish();
                return;
            }


            getIntent().putExtra("savePath", newFilePath);
            setResult(RESULT_OK, getIntent());
        } else {
            setResult(RESULT_CANCELED);
        }
        finish();
    }


    private String copyNewFile(String path) {
        File originalFile = new File(path);
        if (!originalFile.exists() || !originalFile.isFile()) {
            return path;
        }

        // 获取文件所在目录
        String parentDirectory = originalFile.getParent();
        // 获取文件名（不带路径）
        String fileName = originalFile.getName();
        // 获取文件扩展名
        String fileExtension = "";
        int lastDotIndex = fileName.lastIndexOf('.');
        if (lastDotIndex != -1) {
            fileExtension = fileName.substring(lastDotIndex);
            fileName = fileName.substring(0, lastDotIndex);
        }
        // 新文件名
        String newFileName = fileName + "_c" + fileExtension;
        // 新文件路径
        String newFilePath = parentDirectory + File.separator + newFileName;
        File newFile = new File(newFilePath);
        try {
            FileUtils.copyFile(originalFile, newFile);

            //删除旧文件
            originalFile.delete();

            return newFilePath;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return path;
    }


    @SuppressLint("HandlerLeak")
    private Handler mHandler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (msg.what == ACTIVITY_LOAD) {
                if (null != getWindow().getDecorView().getWindowToken()) {
                    mHandler.removeMessages(ACTIVITY_LOAD);
                    //初始化完成
                    initWindow();

                } else {
                    mHandler.removeMessages(ACTIVITY_LOAD);
                    mHandler.sendEmptyMessageDelayed(ACTIVITY_LOAD, 100);
                }
            }
        }
    };

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == CameraActivity.Configuration.REQUEST_CODE_PERMISSIONS) {
            if (allPermissionsGranted()) {// 申请权限通过
                if (onlyImage) {
                    openCameraImage();
                }

            } else {// 申请权限失败
                Toast.makeText(this, "用户拒绝授予权限！请前往“设置>权限管理”开启权限。", Toast.LENGTH_LONG).show();
                finish();
            }
        }
    }


    private boolean allPermissionsGranted() {
        for (String permission : CameraActivity.Configuration.REQUIRED_PERMISSIONS) {
            if (ContextCompat.checkSelfPermission(this, permission)
                    != PackageManager.PERMISSION_GRANTED) {
                return false;
            }
        }
        return true;
    }

    static class Configuration {
        public static final String TAG = "CameraxBasic";
        public static final String FILENAME_FORMAT = "yyyy-MM-dd-HH-mm-ss-SSS";
        public static final int REQUEST_CODE_PERMISSIONS = 10;
        public static final String[] REQUIRED_PERMISSIONS = new String[]{Manifest.permission.CAMERA,
                Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.RECORD_AUDIO};
    }

}