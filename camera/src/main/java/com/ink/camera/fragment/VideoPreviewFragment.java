package com.ink.camera.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.android.exoplayer2.MediaItem;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.ProgressiveMediaSource;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.ink.camera.MediaPreviewActivity;
import com.ink.camera.R;
import com.inks.inkslibrary.Utils.ClickUtil;

/**
 * <pre>
 *     author : inks
 *     time   : 2023/04/28
 *     desc   : 视频
 *     version: 1.0
 * </pre>
 */
public class VideoPreviewFragment extends Fragment {

    private String playPath  ;
    private PlayerView playerView;
    private SimpleExoPlayer player;
    private ImageView fullscreen, back,download;
    private MediaPreviewActivity activity;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = (MediaPreviewActivity) getActivity();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
      View mView = inflater.inflate(R.layout.x_fragment_video_preview, container, false);

        Bundle bundle  = this.getArguments();
        if(bundle!=null){
            playPath = bundle.getString("playPath");
        }


        playerView = mView.findViewById(R.id.exo_player);
        fullscreen = mView.findViewById(R.id.exo_fullscreen_button);
        download= mView.findViewById(R.id.exo_download);
        fullscreen.setOnClickListener(click);
        download.setOnClickListener(click);
        back = mView.findViewById(R.id.exo_back);
        back.setOnClickListener(click);
        initializePlayer();



        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            //当前横屏
            fullscreen.setImageResource(R.drawable.exo_icon_fullscreen_exit);
        } else {
            //当前竖屏
            fullscreen.setImageResource(R.drawable.exo_icon_fullscreen_enter);
        }





        return mView;
    }


    View.OnClickListener click = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (!ClickUtil.isFastDoubleClick((long) (500))) {
                if (v.getId() == R.id.exo_fullscreen_button) {
                    activity.fullscreenClick();
                }else if (v.getId() == R.id.exo_download) {
                    activity.download(playPath);
                }

            }
        }
    };


    @Override
    public void onConfigurationChanged(@NonNull Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (requireActivity().getRequestedOrientation() == ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE) {
            //当前横屏
            fullscreen.setImageResource(R.drawable.exo_icon_fullscreen_exit);
        } else {
            //当前竖屏
            fullscreen.setImageResource(R.drawable.exo_icon_fullscreen_enter);
        }
    }


    private void initializePlayer() {
        if(TextUtils.isEmpty(playPath)){
            return;
        }
        player = new SimpleExoPlayer.Builder(requireContext()).build();
        playerView.setPlayer(player);
        DataSource.Factory dataSourceFactory = new DefaultDataSourceFactory(requireContext(), Util.getUserAgent(requireContext(), "CameraX"));
        MediaSource mediaSource = new ProgressiveMediaSource.Factory(dataSourceFactory)
                .createMediaSource(MediaItem.fromUri(Uri.parse(playPath)) );
        player.prepare(mediaSource);
    }


    @Override
    public void onPause() {
        super.onPause();
        player.setPlayWhenReady(false);
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroy() {
        player.release();
        super.onDestroy();
    }
}
