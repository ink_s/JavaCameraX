package com.ink.camera.fragment;

import android.annotation.SuppressLint;
import android.content.res.Configuration;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.google.android.exoplayer2.MediaItem;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.ProgressiveMediaSource;
import com.google.android.exoplayer2.ui.PlayerControlView;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.ink.camera.MediaPreviewActivity;
import com.ink.camera.R;
import com.ink.camera.view.ScaleView;
import com.inks.inkslibrary.Utils.ClickUtil;
import com.inks.inkslibrary.Utils.L;

/**
 * <pre>
 *     author : inks
 *     time   : 2023/04/28
 *     desc   : 音频
 *     version: 1.0
 * </pre>
 */
public class MusicPreviewFragment extends Fragment {

    private String playPath ;
    private PlayerControlView playerView;
    private SimpleExoPlayer player;
    private MediaPreviewActivity activity;
    private ImageView download;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = (MediaPreviewActivity) getActivity();

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mView = inflater.inflate(R.layout.x_fragment_music_preview, container, false);
        Bundle bundle  = this.getArguments();
        if(bundle!=null){
            playPath = bundle.getString("playPath");
        }
        playerView = mView.findViewById(R.id.exo_player);
        download = mView.findViewById(R.id.down_view);
        if(TextUtils.isEmpty(playPath)){
            download.setVisibility(View.GONE);
        }
        download.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!ClickUtil.isFastDoubleClick((long) (500))) {
                    if (v.getId() == R.id.down_view) {
                        activity.download(playPath);
                    }

                }
            }
        });
        initializePlayer();

        return mView;
    }


    private void initializePlayer() {
        if(TextUtils.isEmpty(playPath)){
            return;
        }
        player = new SimpleExoPlayer.Builder(requireContext()).build();
        playerView.setPlayer(player);
        DataSource.Factory dataSourceFactory = new DefaultDataSourceFactory(requireContext(), Util.getUserAgent(requireContext(), "CameraX"));
        MediaSource mediaSource = new ProgressiveMediaSource.Factory(dataSourceFactory)
                .createMediaSource(MediaItem.fromUri(Uri.parse(playPath)) );
        player.prepare(mediaSource);
    }

    @Override
    public void onPause() {
        super.onPause();
        player.setPlayWhenReady(false);
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroy() {
        player.release();
        super.onDestroy();
    }
}
