package com.ink.camera.fragment;

import android.annotation.SuppressLint;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.ink.camera.MediaPreviewActivity;
import com.ink.camera.R;
import com.ink.camera.view.ScaleView;
import com.inks.inkslibrary.Utils.ClickUtil;
import com.inks.inkslibrary.Utils.L;

/**
 * <pre>
 *     author : inks
 *     time   : 2023/04/28
 *     desc   : 图片预览，可缩放
 *     version: 1.0
 * </pre>
 */
public class ImagePreviewFragment extends Fragment {
    private final int HIDE = 1;

    private ScaleView scaleView;
    private ImageView download;

    private String imageUrl;
    private int resId;
    private MediaPreviewActivity activity;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = (MediaPreviewActivity) getActivity();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mView = inflater.inflate(R.layout.x_fragment_image_preview, container, false);
        scaleView = mView.findViewById(R.id.image_canvas);
        scaleView.setTouchListener(new ScaleView.OnTouchListener() {
            @Override
            public void onTouch() {
                mHandler.removeMessages(HIDE);
                mHandler.sendEmptyMessageDelayed(HIDE,3000);
                if(download!=null && !TextUtils.isEmpty(imageUrl)){
                    download.setVisibility(View.VISIBLE);
                }
            }
        });
        download = mView.findViewById(R.id.down_view);
        mHandler.removeMessages(HIDE);
        mHandler.sendEmptyMessageDelayed(HIDE,3000);
        download.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!ClickUtil.isFastDoubleClick((long) (500))) {
                    if (v.getId() == R.id.down_view) {
                        activity.download(imageUrl);
                    }

                }
            }
        });
        initData();
        return mView;
    }

    @Override
    public void onConfigurationChanged(@NonNull Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        L.ee("onConfigurationChanged");
        if (scaleView != null) {
            scaleView.reset();
        }

    }


    @SuppressLint("CheckResult")
    private void initData() {
        scaleView.setScanEnable(false);
        Bundle bundle = this.getArguments();
        if (bundle == null) {
            return;
        }
        imageUrl = bundle.getString("imageUrl");
        resId = bundle.getInt("resId", 0);

        if (!TextUtils.isEmpty(imageUrl)) {
            Glide.with(this)
                    .load(imageUrl)
                    .skipMemoryCache(true)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .addListener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            L.ee("onLoadFailed", isFirstResource);
                            requireActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    scaleView.setImageResource(R.drawable.x_no_image);
                                    scaleView.setScanEnable(false);
                                }
                            });
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {

                            L.ee("图片加载完成", model, isFirstResource);


                            requireActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    scaleView.setImageDrawable(resource);
                                    scaleView.setScanEnable(true);
                                }
                            });

                            return false;
                        }
                    })
                    .submit();

        } else if (resId != 0) {
            scaleView.setImageResource(resId);
            scaleView.setScanEnable(true);
            if(download!=null){
                download.setVisibility(View.GONE);
            }
        }

    }



    @SuppressLint("HandlerLeak")
    private Handler mHandler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (msg.what == HIDE) {
                if(download!=null){
                    download.setVisibility(View.GONE);
                }
            }
        }
    };

    @Override
    public void onDestroy() {
        super.onDestroy();
        mHandler.removeCallbacksAndMessages(null);
    }
}
