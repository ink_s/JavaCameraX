/*
 * Created by inks on  2022/11/14 下午1:50
 * 
 */

package com.ink.camera;

import android.annotation.SuppressLint;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.ink.camera.view.ScaleView;
import com.inks.inkslibrary.Utils.L;

import java.util.ArrayList;
import java.util.List;

public class ImagePreviewActivity extends AppCompatActivity {

  private ScaleView scaleView;

  private String imageUrl;
  private int resId;
  private List<String> imageUrls = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.x_activity_inamge_preview);
        scaleView = findViewById(R.id.image_canvas);


        scaleView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        initData();
    }

   @SuppressLint("CheckResult")
   private void  initData(){
       imageUrl = getIntent().getStringExtra("imageUrl");
       imageUrls = getIntent().getStringArrayListExtra("imageUrls");
       resId =  getIntent().getIntExtra("resId",0);

       if(!TextUtils.isEmpty(imageUrl)){
           Glide.with(this)
                   .load(imageUrl)
                   .skipMemoryCache(true)
                   .diskCacheStrategy(DiskCacheStrategy.ALL)
                   .addListener(new RequestListener<Drawable>() {
                       @Override
                       public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                         L.ee("onLoadFailed",isFirstResource);
                           runOnUiThread(new Runnable() {
                               @Override
                               public void run() {
                                   scaleView.setImageResource(R.drawable.x_no_image);
                               }
                           });
                           return false;
                       }

                       @Override
                       public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {

                           L.ee("图片加载完成",model,isFirstResource);


                           runOnUiThread(new Runnable() {
                               @Override
                               public void run() {
                                   scaleView.setImageDrawable(resource);
                               }
                           });

                           return false;
                       }
                   })
                   .submit();

       }else if(resId!=0){
           scaleView.setImageResource(resId);
       }

    }

}