package com.ink.camera.util;

import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.ink.camera.R;
import com.inks.inkslibrary.Popup.PopupView;
import com.inks.inkslibrary.Popup.ViewSettings;

import iamutkarshtiwari.github.io.ananas.editimage.utils.DensityUtil;


public class PopupViewUtil {


    private PopupView popupView;
    private static volatile  PopupViewUtil mInstance;

    private PopupViewUtil(){
        if(popupView==null){
            popupView = new PopupView();
        }
    }


    public static PopupViewUtil getInstance() {
        if (mInstance == null) {
            synchronized (PopupViewUtil.class) {
                if (mInstance == null) {
                    mInstance = new PopupViewUtil();
                }
            }
        }
        return mInstance;
    }

    public PopupWindow getPopupWindow(){
      return popupView.getpWindow();
    }

    public  void showPopupMsg(Activity activity, String title, String s, PopupView.onClickListener popupMsgClick, boolean showExit, int what) {
        showPopupMsg(activity.getApplicationContext(), activity.getWindow(),activity.getLayoutInflater(), title,s , popupMsgClick, showExit, what);

    }



    public  void showPopupMsg(Context context, Window window, LayoutInflater layoutInflater,String title,String s, PopupView.onClickListener popupMsgClick, boolean showExit, int what) {

        if (popupView.getpWindow() != null && popupView.getpWindow().isShowing()) {
            popupView.miss();
        }
        WindowManager wm1 = window.getWindowManager();
        int width = wm1.getDefaultDisplay().getWidth();

        View v = LayoutInflater.from(context).inflate(R.layout.x_popup_scroll_view_msg, null, false);
        TextView textView = v.findViewById(R.id.TextView);
        textView.setText(s);
        ViewSettings.Builder builder = new ViewSettings.Builder();
        ViewSettings promptSettings =
                builder.clickListener(popupMsgClick).popupWidth((int) (width*0.8f))
                        // .popupHeight(DensityUtil.dip2px(getContext(), 160))
                        .popupHeight(LinearLayout.LayoutParams.WRAP_CONTENT)
                        .buttonPaddings(new int[]{DensityUtil.dip2px(context, 10), DensityUtil.dip2px(context, 10), DensityUtil.dip2px(context, 10), DensityUtil.dip2px(context, 10)})
                        .showTitle(!TextUtils.isEmpty(title))
                        .titleTextStr(title)
                        .titleTextSize(DensityUtil.dip2px(context, 8))
                        .showButton1(showExit)
                        .showTitleIcon(false)
                        .focusable(false)
                        .build();
        popupView.popupView(window, context, layoutInflater, v, promptSettings, what);

    }







}
