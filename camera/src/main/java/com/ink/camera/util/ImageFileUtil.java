package com.ink.camera.util;

import android.graphics.Bitmap;
import android.text.TextUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class ImageFileUtil {
    public static boolean checkFileExist(final String path) {
        if (TextUtils.isEmpty(path))
            return false;

        File file = new File(path);
        return file.exists();
    }

    public static String getExtensionName(String filename) {
        if ((filename != null) && (filename.length() > 0)) {
            int dot = filename.lastIndexOf('.');
            if ((dot > -1) && (dot < (filename.length() - 1))) {
                return filename.substring(dot + 1);
            }
        }
        return "";
    }

    public static boolean isImage(String filename) {
        String extensionName = getExtensionName(filename);
        String[] types = new String[]{"image", "png", "jpg", "jpeg"};
        if (TextUtils.isEmpty(extensionName)) {
            return false;
        } else {
            for (String type : types) {
                if (extensionName.equalsIgnoreCase(type)) {
                    return true;
                }
            }
        }
        return false;
    }


    public static String getEditPath(String filename){
        if ((filename != null) && (filename.length() > 0)) {
            return getNameOutEdit(getNameOutExtension(filename))+ saveName()+"."+ getExtensionName(filename);
        }
        return filename;
    }


    private static String saveName(){
        return "_edit_"+System.currentTimeMillis();
    }



    public static String getNameOutExtension(String filename) {
        if ((filename != null) && (filename.length() > 0)) {
            int dot = filename.lastIndexOf('.');
            if ((dot > -1) && (dot < (filename.length() - 1))) {
                return filename.substring(0,dot);
            }
        }
        return filename;
    }


    public static String getNameOutEdit(String name) {
        if ((name != null) && (name.length() > 0)) {
            int dot = name.lastIndexOf("_edit_");
            if ((dot > -1) && (dot < (name.length() - 1))) {
                return name.substring(0,dot);
            }
        }
        return name;
    }



    public static boolean saveBitmap(Bitmap bitmap, String filename) {
        File file=new File(filename);
        if(file.exists()){
            file.delete();
        }
        try {
            FileOutputStream outputStream=new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.JPEG,100,outputStream);
            outputStream.flush();
            outputStream.close();
            return true;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        bitmap.recycle();
        return false;
    }

}
