package com.ink.camera.util;

import android.location.Location;

import androidx.exifinterface.media.ExifInterface;

import com.inks.inkslibrary.Utils.L;

import java.io.File;
import java.io.IOException;

/**
 * <pre>
 *     author : inks
 *     time   : 2022/07/19
 *     desc   :
 *     version: 1.0
 * </pre>
 */
public class FileLocationUtil {

    /**
     * 读取经纬度
     *
     * @param jpegFile
     * @return
     * @throws IOException
     */
    public static double[] getLatLong(File jpegFile) {
        ExifInterface exifInterface = null;
        try {
            exifInterface = new ExifInterface(jpegFile.getAbsolutePath());
        } catch (IOException e) {
            return null;
        }
        return exifInterface.getLatLong();
    }

    /**
     * 读取高程
     *
     * @param jpegFile
     * @return
     * @throws IOException
     */
    public static double getAltitude(File jpegFile) {
        ExifInterface exifInterface = null;
        try {
            exifInterface = new ExifInterface(jpegFile.getAbsolutePath());
        } catch (IOException e) {
            return 0;
        }
        return exifInterface.getAltitude(0);
    }

    /**
     * 读取经纬度高程
     *
     * @param jpegFile
     * @return
     * @throws IOException
     */
    public static double[] getLatLongAltitude(File jpegFile) {
        ExifInterface exifInterface = null;
        try {
            exifInterface = new ExifInterface(jpegFile.getAbsolutePath());
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
        double[] r = new double[3];
        if (exifInterface.getLatLong() == null) {
            return null;
        }
        r[0] = exifInterface.getLatLong()[0];
        r[1] = exifInterface.getLatLong()[1];
        r[2] = exifInterface.getAltitude(0);
        return r;
    }


    /**
     * 设置经纬度
     *
     * @param latLong
     * @return
     */
    public static void setLatLong(File jpegFile, double[] latLong) {
        ExifInterface exifInterface = null;
        try {
            exifInterface = new ExifInterface(jpegFile.getAbsolutePath());
            exifInterface.setLatLong(latLong[0], latLong[1]);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    /**
     * 设置经纬度高程
     *
     * @param latLongAltitude
     * @return
     */
    public static void setLatLongAltitude(File jpegFile, double[] latLongAltitude) {
        Location location = new Location("location");
        location.setLatitude(latLongAltitude[0]);
        location.setLongitude(latLongAltitude[1]);
        location.setAltitude(latLongAltitude[2]);
        location.setTime(System.currentTimeMillis());

        ExifInterface exifInterface = null;
        try {
            exifInterface = new ExifInterface(jpegFile.getAbsolutePath());
            exifInterface.setGpsInfo(location);
            exifInterface.saveAttributes();
//            exifInterface.setLatLong(latLongAltitude[0], latLongAltitude[1]);
//            exifInterface.setAltitude(latLongAltitude[2]);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    /**
     * 读取时间
     *
     * @param jpegFile
     * @return
     * @throws IOException
     */
    public static String readDateTime(File jpegFile) throws IOException {
        ExifInterface exifInterface = new ExifInterface(jpegFile.getAbsolutePath());
        String datetime = exifInterface.getAttribute(ExifInterface.TAG_DATETIME);// 拍摄时间
        return datetime;
    }


}
