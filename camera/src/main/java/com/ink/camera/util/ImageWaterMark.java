package com.ink.camera.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.view.View;
import android.view.WindowManager;

import com.inks.inkslibrary.Utils.L;
import com.inks.inkslibrary.Utils.ScreenUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * <pre>
 *     author : inks
 *     time   : 2023/11/07
 *     desc   : 图片水印
 *     version: 1.0
 * </pre>
 */
public class ImageWaterMark {


    public static Bitmap WaterMask(String pathName, View view ,Context context) {
        return WaterMask(pathName,viewToBitmap(view,context),1920,0.4f,false);
    }

    public static Bitmap WaterMask(String pathName, View view ,Context context,int imageMaxSie,float maskMaxWidthPro,boolean enlarge) {
        return WaterMask(pathName,viewToBitmap(view,context),imageMaxSie,maskMaxWidthPro,enlarge);
    }


    //imageMaxSie 图片转bitmap 的最大尺寸，传0 默认1920
    //maskMaxWidthPro 水印宽度最大是图片宽高的比例
    //enlarge 水印小于设置宽度，是否放大
    public static Bitmap WaterMask(String pathName, Bitmap watermark,int imageMaxSie,float maskMaxWidthPro,boolean enlarge) {
        Bitmap src = decodeFile(pathName,imageMaxSie);

        if (src == null || watermark == null) {
            return null;
        }
        int maskW = watermark.getWidth();
        int maskH = watermark.getHeight();

        float scale = 1;

        int minSrcSize = Math.min(src.getWidth(), src.getHeight());


        //最大水印是图片尺寸的maskMaxWidthPro 倍
        scale = minSrcSize *maskMaxWidthPro / maskW;

        L.ee("图片尺寸"+minSrcSize,"水印尺寸"+maskW,"缩放"+scale);

        //只缩小，不放大
        if(!enlarge){
            scale = Math.min(scale, 1);
        }
        Matrix matrix1 = new Matrix();
        matrix1.postScale(scale, scale);

        watermark = Bitmap.createBitmap(watermark, 0, 0, maskW, maskH, matrix1, true);

        Bitmap result = Bitmap.createBitmap(src.getWidth(), src.getHeight(), Bitmap.Config.ARGB_8888);// 创建一个新的和SRC长度宽度一样的位图
        Canvas cv = new Canvas(result);
        //在canvas上绘制原图和新的水印图
        cv.drawBitmap(src, 0, 0, null);
        //水印图绘制在画布的左下角，距离左边和底部都为20
        cv.drawBitmap(watermark, 20, src.getHeight() - watermark.getHeight()- 20, null);
        cv.save();
        cv.restore();

        return result;
    }


    //pathName传手机图片的路径,最大宽高1920
    public static Bitmap decodeFile(String pathName,int imageMaxSie) {
        Bitmap bm = null;
        InputStream stream = null;
        try {
            stream = new FileInputStream(pathName);
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(stream, null, options);
            options.inSampleSize = calculateInSampleSize(options, imageMaxSie==0?1920:imageMaxSie, imageMaxSie==0?1920:imageMaxSie);
            options.inJustDecodeBounds = false;
            stream.close();

            stream = new FileInputStream(pathName);
            bm = BitmapFactory.decodeStream(stream, null, options);
        } catch (Exception e) {
            /*  do nothing.
                If the exception happened on open, bm will be null.
            */
        } finally {
            if (stream != null) {
                try {
                    stream.close();
                } catch (IOException e) {
                    // do nothing here
                }
            }
        }
        return bm;
    }


    public static Bitmap viewToBitmap(View view, Context context) {
        //计算设备分辨率
        int width = ScreenUtils.getScreenWidth(context);
        int height = ScreenUtils.getScreenHeight(context);

        L.ee(width, height);

        //测量使得view指定大小
        int measureWidth = View.MeasureSpec.makeMeasureSpec(width, View.MeasureSpec.AT_MOST);
        int measureHeight = View.MeasureSpec.makeMeasureSpec(height, View.MeasureSpec.AT_MOST);

        view.measure(measureWidth, measureHeight);
        //调用layout方法布局后，可以得到view的尺寸
        view.layout(0, 0, view.getMeasuredWidth(), view.getMeasuredHeight());

        L.ee(view.getMeasuredWidth(), view.getMeasuredHeight());

        Bitmap bitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        canvas.drawColor(Color.TRANSPARENT);
        view.draw(canvas);
        return bitmap;
    }


    public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        int inSampleSize = 1;
        int width = options.outWidth;
        int height = options.outHeight;

        if (width > reqWidth || height > reqHeight) {
            int halfWidth = width / 2;
            int halfHeight = height / 2;

            while ((halfWidth / inSampleSize) >= reqWidth && (halfHeight / inSampleSize) >= reqHeight) {
                inSampleSize *= 2;
            }
        }
        return inSampleSize;
    }


    public static boolean saveBitmap(Bitmap bitmap, String filename) {
        File file = new File(filename);
        if (file.exists()) {
            file.delete();
        }
        try {
            FileOutputStream outputStream = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, outputStream);
            outputStream.flush();
            outputStream.close();
            return true;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        bitmap.recycle();
        return false;
    }


}
