package com.ink.camera;

import static android.widget.Toast.makeText;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import com.ink.camera.adapter.ImageListAdapter;
import com.ink.camera.entity.EditImageInfo;
import com.ink.camera.util.ClickUtil;
import com.ink.camera.util.FileLocationUtil;
import com.ink.camera.util.ImageFileUtil;
import com.ink.camera.util.PopupViewUtil;
import com.inks.inkslibrary.Popup.PopupView;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;

import iamutkarshtiwari.github.io.ananas.editimage.EditImageActivity;
import iamutkarshtiwari.github.io.ananas.editimage.ImageEditorIntentBuilder;
import me.kareluo.imaging.TRSPictureEditor;

public class EditImagesActivity extends AppCompatActivity {


    private RecyclerView recyclerView;
    //传入的
    private ArrayList<EditImageInfo> editImageInfos = new ArrayList<>();

    //adapter显示的列表
    private ArrayList<EditImageInfo> pathList = new ArrayList<>();

    private ImageListAdapter adapter;

    //是否可以删除
    private boolean canDelete = false;


    ActivityResultLauncher<Intent> editResultLauncher;

    private Toast toast;

    //正在编辑的Position
    private int editPosition;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.image_activity_edit_images);
        setupActivityResultLaunchers();

        initView();
        initData();
    }

    private void initView() {

        recyclerView = findViewById(R.id.RecyclerView);
        StaggeredGridLayoutManager layoutManager = new StaggeredGridLayoutManager(3, StaggeredGridLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
    }


    private void initData() {
        editImageInfos = getIntent().getParcelableArrayListExtra("paths");
        canDelete = getIntent().getBooleanExtra("canDelete", false);
        if (editImageInfos == null) {
            editImageInfos = new ArrayList<>();
        }
        Collections.addAll(pathList,  new  EditImageInfo[editImageInfos.size()]);
        Collections.copy(pathList, editImageInfos);

        adapter = new ImageListAdapter(this, pathList);
        recyclerView.setAdapter(adapter);
        adapter.setItemClickListener(adapterClick);

    }


    ImageListAdapter.OnItemClickListener adapterClick = new ImageListAdapter.OnItemClickListener() {
        @Override
        public void onAddClick() {

        }

        @SuppressLint("NotifyDataSetChanged")
        @Override
        public void onDeleteClick(EditImageInfo imageInfo, int position) {
            PopupViewUtil.getInstance().showPopupMsg(EditImagesActivity.this,
                    "提示",
                    "确定要删除吗？",
                    new PopupView.onClickListener() {
                        @Override
                        public void onYesBack(int what) {
                            for (int i = 0; i < editImageInfos.size(); i++) {
                                if (editImageInfos.get(i).getOldPath().equals(imageInfo.getOldPath())) {
                                    editImageInfos.get(i).setDeleted(true);
                                    break;
                                }
                            }
                            pathList.remove(position);

                            adapter.notifyDataSetChanged();
                        }

                        @Override
                        public void onCancelBack(int what) {

                        }
                    }, true, 0);

        }

        @Override
        public void onImageClick(String path, int position) {

            if (!ImageFileUtil.isImage(path)) {
                showToast("不是图片文件，不能编辑");
                return;
            }

            if(TextUtils.isEmpty(path) || path.startsWith("http")){
                showToast("不是本地图片，不能编辑");
                return;
            }
            editPosition = position;
            gotoEditImage(path);

//
//            try {
//                editPosition = position;
//                Intent intent = new ImageEditorIntentBuilder(EditImagesActivity.this, path, ImageFileUtil.getEditPath(path))
//                        .withAddText() // Add the features you need
//                        .withPaintFeature()
//                        .withFilterFeature()
//                        .withRotateFeature()
//                        .withCropFeature()
//                        .withBrightnessFeature()
//                        .withSaturationFeature()
//                        //.withBeautyFeature()
//                        //.withStickerFeature()
//                        .forcePortrait(true)  // Add this to force portrait mode (It's set to false by default)
//                        .setSupportActionBarVisibility(false) // To hide app's default action bar
//                        .build();
//                EditImageActivity.start(editResultLauncher, intent, EditImagesActivity.this);
//            } catch (Exception e) {
//                Log.e("Demo App", e.getMessage()); // This could throw if either `sourcePath` or `outputPath` is blank or Null
//            }


        }
    };


    public void click(View view) {
        if (!ClickUtil.isFastDoubleClick((long) 500)) {
            if (view.getId() == R.id.cancel) {
                onBackPressed();
            } else if (view.getId() == R.id.done) {

                Intent intent = getIntent();
                intent.putParcelableArrayListExtra("backPaths", pathList);
                setResult(RESULT_OK, intent);
                finish();

            }
        }
    }


    private void setupActivityResultLaunchers() {
        editResultLauncher = registerForActivityResult(
                new ActivityResultContracts.StartActivityForResult(),
                result -> {
                    if (result.getResultCode() == Activity.RESULT_OK) {
                        Intent data = result.getData();
                        handleEditorImage(data);
                    }
                });
    }


    @SuppressLint("NotifyDataSetChanged")
    private void handleEditorImage(Intent data) {
        String newFilePath = data.getStringExtra(ImageEditorIntentBuilder.OUTPUT_PATH);
        boolean isImageEdit = data.getBooleanExtra(EditImageActivity.IS_IMAGE_EDITED, false);

        if (isImageEdit) {
            pathList.get(editPosition).setEdited(true);
            double[] oldLocation = FileLocationUtil.getLatLongAltitude( new File(pathList.get(editPosition).getOldPath()));
            if(oldLocation!= null){
                FileLocationUtil.setLatLongAltitude(new File(newFilePath),oldLocation);
            }

            pathList.get(editPosition).setNewPath(newFilePath);

            for (int i = 0; i < editImageInfos.size(); i++) {
                if (editImageInfos.get(i).getOldPath().equals( pathList.get(editPosition).getOldPath())) {
                    editImageInfos.get(editPosition).setNewPath(newFilePath);
                    editImageInfos.get(editPosition).setEdited(true);
                    break;
                }
            }

            adapter.notifyDataSetChanged();
            //通知图库刷新
            sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.fromFile(new File(newFilePath))));
        }

    }


    @SuppressLint("ShowToast")
    protected void showToast(String text) {
        if (TextUtils.isEmpty(text)) {
            return;
        }
        if (toast == null) {
            toast = makeText(getApplicationContext(), text, Toast.LENGTH_LONG);
        } else {
            toast.setText(text);
        }
        toast.show();
    }


    @Override
    public void onBackPressed() {
        Intent intent = getIntent();
        setResult(RESULT_CANCELED, intent);
        finish();
        //super.onBackPressed();
    }


    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {

        if (PopupViewUtil.getInstance().getPopupWindow() != null && PopupViewUtil.getInstance().getPopupWindow().isShowing()) {
            return false;
        } else {
            return super.dispatchTouchEvent(event);
        }
    }



    public void gotoEditImage(String path){
        TRSPictureEditor.edit(this, path, new TRSPictureEditor.EditAdapter() {
            @Override
            public void onComplete(Bitmap bitmap,String inPath) {
                //imageView.setImageBitmap(bitmap);
                String savePath =  ImageFileUtil.getEditPath(inPath);

              boolean success =   ImageFileUtil.saveBitmap(bitmap, savePath);
                if(success){

                    pathList.get(editPosition).setEdited(true);
                    double[] oldLocation = FileLocationUtil.getLatLongAltitude( new File(pathList.get(editPosition).getOldPath()));
                    if(oldLocation!= null){
                        FileLocationUtil.setLatLongAltitude(new File(savePath),oldLocation);
                    }
                    pathList.get(editPosition).setNewPath(savePath);
                    for (int i = 0; i < editImageInfos.size(); i++) {
                        if (editImageInfos.get(i).getOldPath().equals( pathList.get(editPosition).getOldPath())) {
                            editImageInfos.get(editPosition).setNewPath(savePath);
                            editImageInfos.get(editPosition).setEdited(true);
                            break;
                        }
                    }
                    adapter.notifyDataSetChanged();
                    //通知图库刷新
                    sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.fromFile(new File(savePath))));



                }

            }
        });
    }

    public static boolean editOneImage(ActivityResultLauncher<Intent> resultLauncher, Context context, String path,boolean deleteOld) {
        if (!ImageFileUtil.isImage(path)) {
            return false;
        }
        try {
        TRSPictureEditor.edit(context,resultLauncher,path,null,deleteOld);
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    public static boolean editOneImage(ActivityResultLauncher<Intent> resultLauncher, Context context, String path,boolean deleteOld,int maxSize) {
        if (!ImageFileUtil.isImage(path)) {
            return false;
        }
        try {
            TRSPictureEditor.edit(context,resultLauncher,path,null,deleteOld,maxSize);
        } catch (Exception e) {
            return false;
        }
        return true;
    }





//    public static boolean editOneImage(ActivityResultLauncher<Intent> resultLauncher, Context context, String path) {
//        if (!ImageFileUtil.isImage(path)) {
//            return false;
//        }
//        try {
//            Intent intent = new ImageEditorIntentBuilder(context, path, ImageFileUtil.getEditPath(path))
//                    .withAddText() // Add the features you need
//                    .withPaintFeature()
//                    .withFilterFeature()
//                    .withRotateFeature()
//                    .withCropFeature()
//                    .withBrightnessFeature()
//                    .withSaturationFeature()
//                    //.withBeautyFeature()
//                    //.withStickerFeature()
//                    .forcePortrait(true)  // Add this to force portrait mode (It's set to false by default)
//                    .setSupportActionBarVisibility(false) // To hide app's default action bar
//                    .build();
//            EditImageActivity.start(resultLauncher, intent, context);
//            return true;
//        } catch (Exception e) {
//            Log.e("Demo App", e.getMessage()); // This could throw if either `sourcePath` or `outputPath` is blank or Null
//            return false;
//        }
//    }




}