package com.ink.camera.popup;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.PopupWindow;

import com.ink.camera.R;
import com.ink.camera.view.ClickEffectText;


public class PopupView {
    public interface OnClickListener {
        public void onYesBack();

        public void onCancelBack();

        public void onAgainBack();
    }

    private OnClickListener mOnClickListener;
    private View contentView = null;
    private PopupWindow pWindow;
    private Context context;
    private Window window;
    private ViewSettings viewSettings;

    private ClickEffectText buttonDone;
    private ClickEffectText buttonAgain;
    private ClickEffectText buttonCancel;

    public void setOnClickListener(OnClickListener mOnClickListener) {
        this.mOnClickListener = mOnClickListener;
    }

    private void   popupView(Window window, Context context){
        popupView(window,context,null,null);
    }
    private void   popupView(Window window, Context context, final ViewSettings viewSettings){
        popupView(window,context,viewSettings,null);
    }

    private void   popupView(Window window, Context context, OnClickListener mOnClickListener){
        popupView(window,context,null,mOnClickListener);
    }
    @SuppressLint("ClickableViewAccessibility")
    public void popupView(Window window, Context context, final ViewSettings viewSettings,OnClickListener mOnClickListener) {

        this.window = window;
        this.context = context;
        this.viewSettings = viewSettings;
        this.mOnClickListener = mOnClickListener;

        if (!(pWindow != null && pWindow.isShowing())) {
            contentView = LayoutInflater.from(context).inflate(R.layout.ink_camera_popup, null);
            pWindow = new PopupWindow(contentView, FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.WRAP_CONTENT);

            buttonDone = contentView.findViewById(R.id.buttonDone);
            buttonAgain = contentView.findViewById(R.id.buttonAgain);
            buttonCancel = contentView.findViewById(R.id.buttonCancel);

            buttonDone.setOnClickListener(clickListener);
            buttonAgain.setOnClickListener(clickListener);
            buttonCancel.setOnClickListener(clickListener);

            pWindow.setAnimationStyle(R.style.popup_bottom_top);
            pWindow.setTouchable(true);
            pWindow.setFocusable(true);
            pWindow.setOutsideTouchable(true);
            pWindow.setBackgroundDrawable(new BitmapDrawable());
            pWindow.setClippingEnabled(true);
            backgroundAlpha(0.5F);
            pWindow.showAtLocation(window.getDecorView(), Gravity.BOTTOM, 0, 0);
            pWindow.setOnDismissListener(new PopupWindow.OnDismissListener() {
                @Override
                public void onDismiss() {
                    backgroundAlpha(1f);

                }
            });

        } else {

        }

    }

    View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            miss();
            if (v.getId() == R.id.buttonDone) {
                if (mOnClickListener != null) {
                    mOnClickListener.onYesBack();
                }
            } else if (v.getId() == R.id.buttonAgain) {
                if (mOnClickListener != null) {
                    mOnClickListener.onAgainBack();
                }
            } else if (v.getId() == R.id.buttonCancel) {
                if (mOnClickListener != null) {
                    mOnClickListener.onCancelBack();
                }
            }
        }
    };


    public void backgroundAlpha(float bgAlpha) {
        WindowManager.LayoutParams lp = window.getAttributes();
        lp.alpha = bgAlpha; // 0.0-1.0
        window.setAttributes(lp);
    }


    public void miss() {
        if (pWindow != null && pWindow.isShowing()) {
            pWindow.dismiss();
        }
    }

    public PopupWindow getPopupWindow() {
        return pWindow;
    }

}
