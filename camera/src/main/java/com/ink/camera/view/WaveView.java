package com.ink.camera.view;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ComposeShader;
import android.graphics.LinearGradient;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.Shader;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.view.animation.LinearInterpolator;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.ink.camera.R;
import com.inks.inkslibrary.Utils.L;

import java.util.ArrayList;
import java.util.List;

/**
 * <pre>
 *     author : inks
 *     time   : 2023/03/20
 *     desc   : 圆形水波纹动画
 *     version: 1.0
 * </pre>
 */
public class WaveView extends View {


    //控件大小
    private int width;
    private int height;

    //圆形或矩形,默认圆形0  ，1 方形，2圆形宽高不强制相等
    private int circular = 0;

    //base 创建基础bitmap时候使用，之后设置数据都是缩放、平移改基础bitmap
    //波浪振幅
    private float baseAmplitude = 0.05f;
    //进度
    private float basePro = 0.5f;
    //一个波形的周期长度
    private float baseCycleLength = 1.0f;


    //当前画面的参数
    //波浪振幅
    private float amplitude = 0.05f;
    //进度
    private float pro = 0.5f;
    //一个波形的周期长度
    private float cycleLength = 1.0f;


    //波形着色器
    private BitmapShader mBitmapShader;

    //文本
    private BitmapShader mBitmapTextShader;
    //画笔
    private Paint mPaint;
    //画笔
    private Paint mTextPaint;
    //背景波形颜色
    private int blackColor = 0X550EA7F7;
    //前景波形颜色
    private int frontColor = 0XFF0EA7F7;


    //背景文本颜色
    private int blackTextColor = 0XFF0EA7F7;
    //前景文本颜色
    private int frontTextColor = 0XFFFFFFFF;

    //Matrix
    private Matrix mMatrix;
    //水平平移的大小，设置动画改变该值,0-1
    private float move = 0.0f;
    //边框宽度
    private float borderWidth = 0;
    //边框颜色
    private int borderColor = 0XFF0EA7F7;
    //边框画笔
    private Paint borderPaint;

    private ObjectAnimator animator;

    private int animatorDuration = 1000;

    private AnimatorSet mAnimatorSet;

    private boolean showBgColor = false;
    private int bgColor = 0X33424858;

    private Paint bgPaint;

    private List<WaveText> waveTextList = new ArrayList<>();
    private float allLineHeight = 0;


    //如LinearGradient，可实现渐变色
    // 创建LinearGradient并设置渐变颜色数组,平铺效果为镜像
    //    LinearGradient mLinearGradient = new LinearGradient(0, height, 0, height-(pro+amplitude)*height, new int[]{
    //            Color.RED, Color.YELLOW}, null,
    //            Shader.TileMode.CLAMP);
    //    shader = mLinearGradient;
    private Shader shader;
    private PorterDuff.Mode mode = PorterDuff.Mode.MULTIPLY;


    public WaveView(@NonNull Context context) {
        this(context, null);
    }

    public WaveView(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public WaveView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        if (attrs != null) {
            TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.XWaveView);
            blackColor = typedArray.getColor(R.styleable.XWaveView_wv_blackColor, 0X550EA7F7);
            frontColor = typedArray.getColor(R.styleable.XWaveView_wv_frontColor, 0XFF0EA7F7);
            blackTextColor = typedArray.getColor(R.styleable.XWaveView_wv_blackColor, 0XFF0EA7F7);
            frontTextColor = typedArray.getColor(R.styleable.XWaveView_wv_frontColor, 0XFFFFFFFF);
            borderColor = typedArray.getColor(R.styleable.XWaveView_wv_borderColor, 0XFF0EA7F7);
            bgColor = typedArray.getColor(R.styleable.XWaveView_wv_bgColor, 0X33424858);
            borderWidth = typedArray.getDimension(R.styleable.XWaveView_wv_borderWidth, 0);
            amplitude = typedArray.getFloat(R.styleable.XWaveView_wv_amplitude, 0.05f);
            pro = typedArray.getFloat(R.styleable.XWaveView_wv_pro, 0.5f);
            cycleLength = typedArray.getFloat(R.styleable.XWaveView_wv_cycleLength, 1.0f);
            circular = typedArray.getInt(R.styleable.XWaveView_wv_circular, 0);
            showBgColor = typedArray.getBoolean(R.styleable.XWaveView_wv_showBg, false);
            typedArray.recycle();
        }

        init();
    }

    private void init() {
        mPaint = new Paint();
        mPaint.setAntiAlias(true);

        mTextPaint = new Paint();
        mTextPaint.setAntiAlias(true);

        mMatrix = new Matrix();
        borderPaint = new Paint();
        borderPaint.setAntiAlias(true);
        borderPaint.setStyle(Paint.Style.STROKE);
        borderPaint.setColor(borderColor);
        borderPaint.setStrokeWidth(borderWidth);
        List<Animator> animators = new ArrayList<>();
        animator = ObjectAnimator.ofFloat(
                this, "move", 0f, cycleLength);
        animator.setRepeatCount(ValueAnimator.INFINITE);
        animator.setDuration(animatorDuration);
        animator.setInterpolator(new LinearInterpolator());
        animators.add(animator);
        mAnimatorSet = new AnimatorSet();
        mAnimatorSet.playTogether(animators);

        bgPaint = new Paint();
        bgPaint.setStyle(Paint.Style.FILL);
        bgPaint.setColor(bgColor);

    }

    public float getMove() {
        return move;
    }

    public void setMove(float move) {
        this.move = move;
        invalidate();
    }

    public void setAmplitude(float amplitude) {
        this.amplitude = amplitude;
        invalidate();
    }

    public void setPro(float pro) {
        this.pro = pro;
        invalidate();
    }

    public void setCircular(int circular) {
        this.circular = circular;
        invalidate();
    }

    public void setCycleLength(float cycleLength) {
        this.cycleLength = cycleLength;
        invalidate();
    }

    public float getCycleLength() {
        return cycleLength;
    }

    public void setBlackColor(int blackColor) {
        this.blackColor = blackColor;
        createShader();
        invalidate();
    }

    public void setFrontColor(int frontColor) {
        this.frontColor = frontColor;
        createShader();
        invalidate();
    }

    public void setBorderWidth(float borderWidth) {
        this.borderWidth = borderWidth;
        invalidate();
    }

    public void setBorderColor(int borderColor) {
        this.borderColor = borderColor;
        invalidate();
    }

    public void setAnimatorDuration(int animatorDuration) {
        this.animatorDuration = animatorDuration;
        if (animator != null) {
            animator.setDuration(animatorDuration);
        }
    }

    public void setAnimatorSet(AnimatorSet set) {
        if (mAnimatorSet != null) {
            mAnimatorSet.cancel();
        }
        this.mAnimatorSet = set;
        if (mAnimatorSet != null) {
            mAnimatorSet.start();
        }
    }


    public void setShader(Shader shader) {
        this.shader = shader;
        invalidate();
    }

    public void setMode(PorterDuff.Mode mode) {
        this.mode = mode;
        invalidate();
    }

    public void setBgColor(int bgColor, boolean showBgColor) {
        this.bgColor = bgColor;
        this.showBgColor = showBgColor;
        if (bgPaint != null) {
            bgPaint.setColor(bgColor);
        }
        invalidate();
    }

    public List<WaveText> getWaveTextList() {
        return waveTextList;
    }

    public void setWaveTextList(List<WaveText> waveTextList) {
        this.waveTextList = waveTextList;
        allLineHeight = getAllLineHeight();
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (mAnimatorSet != null) {
            mAnimatorSet.start();
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int specHeightSize = MeasureSpec.getSize(heightMeasureSpec);//高
        int specWidthSize = MeasureSpec.getSize(widthMeasureSpec);
        width = specWidthSize;
        height = specHeightSize;
        if (circular == 0) {
            //圆形，宽高相等
            height = width = Math.min(specWidthSize, specHeightSize);
            setMeasuredDimension(width, width);
        }
    }


    @Override
    protected void onDraw(Canvas canvas) {
        if (mBitmapShader != null) {

            //画背景
            if (showBgColor) {
                if (circular == 0) {
                    float radius = width / 2f - borderWidth;
                    canvas.drawCircle(width / 2f, height / 2f, radius, bgPaint);

                } else if (circular == 2) {
                    float radius;
                    if (width > height) {
                        radius = width / 2f - borderWidth;
                    } else if (width < height) {
                        radius = height / 2f - borderWidth;
                    } else {
                        radius = width / 2f - borderWidth;
                    }

                    canvas.drawCircle(width / 2f, height / 2f, radius, bgPaint);

                } else {
                    //矩形
                    canvas.drawRect(borderWidth, borderWidth, width - borderWidth,
                            height - borderWidth, bgPaint);
                }


            }


            // X轴波形长度，Y轴振幅,根据设定参数缩放base bitMap
            mMatrix.setScale(
                    cycleLength / baseCycleLength,
                    amplitude / baseAmplitude,
                    0,
                    height * basePro);

            //根据设定参数上下左右平移 bitMap ,水平移动动画效果、上下移动改变进度
            mMatrix.postTranslate(
                    move * width,
                    (basePro - pro) * height);

            mBitmapShader.setLocalMatrix(mMatrix);

            if (mBitmapTextShader != null) {
                mBitmapTextShader.setLocalMatrix(mMatrix);
            }


            if (shader != null) {
                // 混合渲染 将两个效果叠加,使用PorterDuff叠加模式
                ComposeShader mComposeShader = new ComposeShader(mBitmapShader, shader, mode);
                mPaint.setShader(mComposeShader);
                if (mBitmapTextShader != null) {
                    ComposeShader mComposeShader2 = new ComposeShader(mBitmapTextShader, shader, mode);
                    mTextPaint.setShader(mComposeShader2);
                }

            } else {
                mPaint.setShader(mBitmapShader);
                if (mBitmapTextShader != null) {
                    mTextPaint.setShader(mBitmapTextShader);
                }
            }

            if (circular == 0) {
                float radius = width / 2f - borderWidth;

                canvas.drawCircle(width / 2f, height / 2f, radius, mPaint);


                if (borderWidth > 0) {
                    canvas.drawCircle(width / 2f, height / 2f,
                            (width - borderWidth) / 2f - 1f, borderPaint);
                }

            } else if (circular == 2) {
                //如果宽大于高，画上下的线
                //如果宽小于高，画左右的线
                //如果宽等于高，圆
                if (width > height) {
                    float radius = width / 2f - borderWidth;
                    canvas.drawCircle(width / 2f, height / 2f, radius, mPaint);


                    if (borderWidth > 0) {
                        canvas.drawCircle(width / 2f, height / 2f,
                                (width - borderWidth) / 2f - 1f, borderPaint);
                        //x² = r²-(h/2)²
                        float side = (float) Math.sqrt(Math.pow(((width - borderWidth * 2) / 2f - 1f), 2) - Math.pow(((height - borderWidth * 2) / 2f), 2));
                        canvas.drawLine(width / 2.0f - side, borderWidth / 2f, width / 2.0f + side, borderWidth / 2f, borderPaint);
                        canvas.drawLine(width / 2.0f - side, height - borderWidth / 2f, width / 2.0f + side, height - borderWidth / 2f, borderPaint);
                    }

                } else if (width < height) {

                    float radius = height / 2f - borderWidth;
                    canvas.drawCircle(width / 2f, height / 2f, radius, mPaint);


                    if (borderWidth > 0) {
                        canvas.drawCircle(width / 2f, height / 2f,
                                (height - borderWidth) / 2f - 1f, borderPaint);
                        //x² = r²-(h/2)²
                        float side = (float) Math.sqrt(Math.pow(((height - borderWidth * 2) / 2f - 1f), 2) - Math.pow(((width - borderWidth * 2) / 2f), 2));
                        canvas.drawLine(borderWidth / 2f, height / 2.0f - side, borderWidth / 2f, height / 2.0f + side, borderPaint);
                        canvas.drawLine(width - borderWidth / 2f, height / 2.0f - side, width - borderWidth / 2f, height / 2.0f + side, borderPaint);

                    }


                } else {
                    float radius = width / 2f - borderWidth;
                    canvas.drawCircle(width / 2f, height / 2f, radius, mPaint);


                    if (borderWidth > 0) {
                        canvas.drawCircle(width / 2f, height / 2f,
                                (width - borderWidth) / 2f - 1f, borderPaint);
                    }
                }


            } else {
                //矩形
                if (borderWidth > 0) {
                    canvas.drawRect(
                            borderWidth / 2f,
                            borderWidth / 2f,
                            width - borderWidth / 2f - 0.5f,
                            height - borderWidth / 2f - 0.5f,
                            borderPaint);
                }
                canvas.drawRect(borderWidth, borderWidth, width - borderWidth,
                        height - borderWidth, mPaint);
            }


            if (waveTextList != null && waveTextList.size() > 0) {
                for (int i = 0; i < waveTextList.size(); i++) {
                    WaveText waveText = waveTextList.get(i);
                    if (TextUtils.isEmpty(waveText.text)) {
                        continue;
                    }
                    mTextPaint.setTextSize(sp2px(waveText.size));

                    Paint.FontMetrics metrics = mTextPaint.getFontMetrics();
                    float top = metrics.top;//为基线到字体上边框的距离
                    float bottom = metrics.bottom;//为基线到字体下边框的距离



                    float textWidth = mTextPaint.measureText(waveText.text);
                    //默认居中
                    float startX = (width - textWidth) / 2;
                    if (waveText.gravity == Gravity.LEFT || waveText.gravity == Gravity.START ) {
                        startX = width * waveText.leftRightPro;
                    } else if (waveText.gravity == Gravity.RIGHT|| waveText.gravity == Gravity.END) {
                        startX = textWidth + width * waveText.leftRightPro;
                    }
                    float startY = (height - allLineHeight) / 2 + getIndexTextHeight(i) - top / 2 - bottom / 2;
                    canvas.drawText(waveText.text, startX, startY, mTextPaint);
                }
            }




        } else {
            mPaint.setShader(null);
        }
    }


    private float getAllLineHeight() {
        if (waveTextList != null && waveTextList.size() > 0) {
            float h = 0;
            for (WaveText waveText : waveTextList) {
                h = h + sp2px(waveText.lineHeight);
            }
            return h;
        }
        return 0;
    }

    private float getIndexTextHeight(int index) {
        if (waveTextList != null && waveTextList.size() > 0) {
            float h = 0;
            for (int i = 0; i < index; i++) {
                h = h + sp2px(waveTextList.get(i).lineHeight);
            }

            return h + (sp2px(waveTextList.get(index).lineHeight) * 0.5F);
        }
        return 0;
    }


    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        createShader();
        if (mAnimatorSet != null) {
            mAnimatorSet.start();
        }
    }

    private void createShader() {
        //频率
        double f = 2.0f * Math.PI / baseCycleLength / width;
        //波浪振幅
        float amplitude = height * baseAmplitude;
        //进度（高度）
        float pro = height * basePro;

        Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);

        Bitmap textBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas textCanvas = new Canvas(textBitmap);

        Paint backTextPaint = new Paint();
        backTextPaint.setStrokeWidth(2);
        backTextPaint.setAntiAlias(true);
        backTextPaint.setColor(blackTextColor);
        textCanvas.drawRect(0, 0, width, height, backTextPaint);

        Paint frontTextPaint = new Paint();
        frontTextPaint.setStrokeWidth(2);
        frontTextPaint.setAntiAlias(true);
        frontTextPaint.setColor(frontTextColor);


        Paint backPaint = new Paint();
        backPaint.setStrokeWidth(2);
        backPaint.setAntiAlias(true);
        final int endX = width + 1;
        final int endY = height + 1;
        float[] waveY = new float[endX];
        //第一个波浪，后
        backPaint.setColor(blackColor);
        for (int beginX = 0; beginX < endX; beginX++) {
            double wx = beginX * f;
            float beginY = (float) (pro + amplitude * Math.sin(wx));
            canvas.drawLine(beginX, beginY, beginX, endY, backPaint);
            textCanvas.drawLine(beginX, beginY, beginX, endY, frontTextPaint);

            waveY[beginX] = beginY;
        }

        //第二个波浪，前
        Paint frontPaint = new Paint();
        frontPaint.setStrokeWidth(2);
        frontPaint.setAntiAlias(true);
        frontPaint.setColor(frontColor);
        //与第一个错位
        final int move = (int) (baseCycleLength * width / 4);
        for (int beginX = 0; beginX < endX; beginX++) {
            canvas.drawLine(beginX, waveY[(beginX + move) % endX], beginX, endY, frontPaint);
            textCanvas.drawLine(beginX, waveY[(beginX + move) % endX], beginX, endY, frontTextPaint);
        }

        // REPEAT水平方向重复，CLAMP垂直方向边缘拉伸
        mBitmapShader = new BitmapShader(bitmap, Shader.TileMode.REPEAT, Shader.TileMode.CLAMP);
        mBitmapTextShader = new BitmapShader(textBitmap, Shader.TileMode.REPEAT, Shader.TileMode.CLAMP);
        mPaint.setShader(mBitmapShader);
        mTextPaint.setShader(mBitmapTextShader);
    }


    public static class WaveText {
        public String text;
        public int gravity = Gravity.CENTER;
        //gravity 为左或右时，距离2边的宽度占总宽度的百分比 0-1
        public float leftRightPro = 0.1f;
        //sp
        public int size = 14;
        //sp
        public int lineHeight = 16;
        public  WaveText(){
        }
        public  WaveText(String text){
            this.text = text;
        }

        public  WaveText(String text,int size,int lineHeight){
            this.text = text;
            this.size = size;
            this.lineHeight = lineHeight;
        }

        public  WaveText(String text,int size,int lineHeight,int gravity,float leftRightPro){
            this.text = text;
            this.size = size;
            this.lineHeight = lineHeight;
            this.gravity = gravity;
            this.leftRightPro = leftRightPro;
        }
    }

    public int sp2px(final float spValue) {
        final float fontScale = Resources.getSystem().getDisplayMetrics().scaledDensity;
        return (int) (spValue * fontScale + 0.5f);
    }

}
