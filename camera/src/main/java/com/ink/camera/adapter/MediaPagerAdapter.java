package com.ink.camera.adapter;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import com.ink.camera.entity.MediaBean;
import com.ink.camera.fragment.ImagePreviewFragment;
import com.ink.camera.fragment.MusicPreviewFragment;
import com.ink.camera.fragment.VideoPreviewFragment;

import java.util.ArrayList;
import java.util.List;


public class MediaPagerAdapter extends FragmentStateAdapter {

    private List<MediaBean> dataList = new ArrayList<>();

    public MediaPagerAdapter(@NonNull FragmentActivity fragmentActivity) {
        super(fragmentActivity);
    }

    public MediaPagerAdapter(@NonNull FragmentActivity fragmentActivity, List<MediaBean> dataList) {
        super(fragmentActivity);
        this.dataList = dataList;
    }


    @NonNull
    @Override
    public Fragment createFragment(int position) {
        if (MediaBean.TYPE.IMAGE.name().equals(dataList.get(position).type)) {
            ImagePreviewFragment imageFragment = new ImagePreviewFragment();
            Bundle bundle = new Bundle();
            bundle.putString("imageUrl",dataList.get(position).path);
            bundle.putInt("resId",dataList.get(position).resId);
            imageFragment.setArguments(bundle);
            return imageFragment;

        } else if (MediaBean.TYPE.VIDEO.name().equals(dataList.get(position).type)) {
            VideoPreviewFragment videoFragment = new VideoPreviewFragment();
            Bundle bundle = new Bundle();
            bundle.putString("playPath",dataList.get(position).path);
            videoFragment.setArguments(bundle);
            return videoFragment;

        } else if (MediaBean.TYPE.MUSIC.name().equals(dataList.get(position).type)) {
            MusicPreviewFragment musicFragment = new MusicPreviewFragment();
            Bundle bundle = new Bundle();
            bundle.putString("playPath",dataList.get(position).path);
            musicFragment.setArguments(bundle);
            return musicFragment;
        }
        return null;
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }
}
