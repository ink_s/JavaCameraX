package com.ink.camera.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.ink.camera.R;
import com.ink.camera.entity.EditImageInfo;
import com.ink.camera.util.FileLocationUtil;
import com.inks.inkslibrary.Utils.L;

import java.io.File;
import java.util.ArrayList;
import java.util.List;


public class ImageListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<EditImageInfo> dataList = new ArrayList<>();

    protected OnItemClickListener mItemClickListener;

    private boolean canDelete = false;

    //最大图片个数
    private int maxNumber = 8;

    private Context context;

    public ImageListAdapter(Context context, List<EditImageInfo> dataList) {
        this.context = context;
        this.dataList = dataList;
        this.canDelete = false;
    }


    public ImageListAdapter(Context context, List<EditImageInfo> dataList, boolean canDelete) {
        this.context = context;
        this.dataList = dataList;
        this.canDelete = canDelete;
    }

    public void setItemClickListener(OnItemClickListener itemClickListener) {
        mItemClickListener = itemClickListener;
    }


    public interface OnItemClickListener {
        void onAddClick();

        void onDeleteClick(EditImageInfo path, int position);

        void onImageClick(String path, int position);
    }

    public void setData(List<EditImageInfo> dataList) {
        this.dataList = dataList;
        notifyDataSetChanged();
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.image_item_edit_image, parent, false);
        return new RecyclerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        RecyclerViewHolder vh = (RecyclerViewHolder) holder;

        if (position == dataList.size()) {
            //最后一个，添加图标
            vh.delete.setVisibility(View.GONE);

            if (dataList.size() >= maxNumber) {
                //到了最大的个数，不能添加了
                vh.image.setVisibility(View.GONE);
            } else {
                vh.image.setVisibility(View.VISIBLE);
                vh.image.setImageResource(R.drawable.image_image_add_image);
            }
        } else {
            vh.image.setVisibility(View.VISIBLE);
            String path = dataList.get(position).getOldPath();


            if (dataList.get(position).isEdited() && !TextUtils.isEmpty(dataList.get(position).getNewPath())) {
                path = dataList.get(position).getNewPath();
            }
            Glide.with(context)
                    .load(path)
                    //.override(100)
                    .into(vh.image);
            vh.delete.setVisibility(View.VISIBLE);
        }

        if (canDelete) {
            vh.delete.setVisibility(View.VISIBLE);
        } else {
            vh.delete.setVisibility(View.GONE);
        }

        vh.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mItemClickListener != null) {
                    mItemClickListener.onDeleteClick(dataList.get(position), position);
                }

            }
        });
        vh.image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mItemClickListener != null) {
                    if (position == dataList.size()) {
                        mItemClickListener.onAddClick();
                    } else {
                        if (dataList.get(position).isEdited() && !TextUtils.isEmpty(dataList.get(position).getNewPath())) {
                            mItemClickListener.onImageClick(dataList.get(position).getNewPath(), position);
                        } else {
                            mItemClickListener.onImageClick(dataList.get(position).getOldPath(), position);
                        }

                    }

                }
            }
        });


    }

    @Override
    public int getItemCount() {
        //return dataList.size() + 1;

        return dataList.size();
    }

    private class RecyclerViewHolder extends RecyclerView.ViewHolder {
        ImageView image;
        ImageView delete;

        RecyclerViewHolder(View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.image_view);
            delete = itemView.findViewById(R.id.delete_view);
        }
    }
}