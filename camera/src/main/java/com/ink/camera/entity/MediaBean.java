package com.ink.camera.entity;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * <pre>
 *     author : inks
 *     time   : 2023/04/28
 *     desc   :  多媒体文件
 *     version: 1.0
 * </pre>
 */
public class MediaBean implements Parcelable {


    public enum TYPE {IMAGE, VIDEO, MUSIC}

    public String type;
    //地址
    public String path;
    //如果是res图片，本地图片id
    public int resId;

    public String title;

    public MediaBean() {

    }


    public MediaBean(String type, String path) {
        this.type = type;
        this.path = path;

    }

    public MediaBean(String type, int resId) {
        this.type = type;
        this.resId = resId;

    }

    public MediaBean(String type, String path,String title) {
        this.type = type;
        this.path = path;
        this.title = title;

    }

    public MediaBean(String type, int resId,String title) {
        this.type = type;
        this.resId = resId;
        this.title = title;

    }


    protected MediaBean(Parcel in) {
        type = in.readString();
        path = in.readString();
        resId = in.readInt();
        title= in.readString();
    }

    public static final Creator<MediaBean> CREATOR = new Creator<MediaBean>() {
        @Override
        public MediaBean createFromParcel(Parcel in) {
            return new MediaBean(in);
        }

        @Override
        public MediaBean[] newArray(int size) {
            return new MediaBean[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(type);
        dest.writeString(path);
        dest.writeInt(resId);
        dest.writeString(title);
    }


}
