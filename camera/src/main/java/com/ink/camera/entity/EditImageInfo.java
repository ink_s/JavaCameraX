package com.ink.camera.entity;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * <pre>
 *     author : inks
 *     time   : 2022/06/30
 *     desc   : 要编辑的图片信息
 *     version: 1.0
 * </pre>
 */
public class EditImageInfo implements Parcelable {
    //旧的图片地址
    private String oldPath;
    //新的图片地址
    private String newPath;
    //是否编辑过
    private boolean edited;
    //是否删除了
    private boolean deleted;
    //备注信息
    private String jsonStr;

    public EditImageInfo(String oldPath){
        this.oldPath = oldPath;
    }

    public String getOldPath() {
        return oldPath;
    }

    public void setOldPath(String oldPath) {
        this.oldPath = oldPath;
    }

    public String getNewPath() {
        return newPath;
    }

    public void setNewPath(String newPath) {
        this.newPath = newPath;
    }

    public boolean isEdited() {
        return edited;
    }

    public void setEdited(boolean edited) {
        this.edited = edited;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public String getJsonStr() {
        return jsonStr;
    }

    public void setJsonStr(String jsonStr) {
        this.jsonStr = jsonStr;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.oldPath);
        dest.writeString(this.newPath);
        dest.writeByte(this.edited ? (byte) 1 : (byte) 0);
        dest.writeByte(this.deleted ? (byte) 1 : (byte) 0);
        dest.writeString(this.jsonStr);
    }

    public void readFromParcel(Parcel source) {
        this.oldPath = source.readString();
        this.newPath = source.readString();
        this.edited = source.readByte() != 0;
        this.deleted = source.readByte() != 0;
        this.jsonStr = source.readString();
    }

    public EditImageInfo() {
    }

    protected EditImageInfo(Parcel in) {
        this.oldPath = in.readString();
        this.newPath = in.readString();
        this.edited = in.readByte() != 0;
        this.deleted = in.readByte() != 0;
        this.jsonStr = in.readString();
    }

    public static final Parcelable.Creator<EditImageInfo> CREATOR = new Parcelable.Creator<EditImageInfo>() {
        @Override
        public EditImageInfo createFromParcel(Parcel source) {
            return new EditImageInfo(source);
        }

        @Override
        public EditImageInfo[] newArray(int size) {
            return new EditImageInfo[size];
        }
    };

}
