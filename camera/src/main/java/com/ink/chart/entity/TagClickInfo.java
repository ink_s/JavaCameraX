/*
 *  Created by inks on  2023 /3 /23
 *  
 */

package com.ink.chart.entity;

/**
 * <pre>
 *     author : inks
 *     time   : 2023/03/23
 *     desc   : 图表tag点击需要显示的东西
 *     version: 1.0
 * </pre>
 */
public  class TagClickInfo {
    public int color;
    public   String name;
    public float number;
    public float pro;

    public TagClickInfo(){}

    public TagClickInfo(int color, String name, float number,float pro){
        this.color = color;
        this.name = name;
        this.number = number;
        this.pro = pro;
    }
}
