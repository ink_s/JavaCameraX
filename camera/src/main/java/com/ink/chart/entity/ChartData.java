package com.ink.chart.entity;

/**
 * <pre>
 *     author : inks
 *     time   : 2024/01/17
 *     desc   :
 *     version: 1.0
 * </pre>
 */
public class ChartData {
    public String[] xData;
    public YData[] yData;
    public float total;


    public static class YData {
        public OneYData[] data;
        public String code;
        public String name;
    }

    public static class OneYData {

        public float number;
        public float pro;


    }
}
