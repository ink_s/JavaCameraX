package com.ink.chart.entity;

/**
 * <pre>
 *     author : inks
 *     time   : 2023/09/08
 *     desc   : tag
 *     version: 1.0
 * </pre>
 */
public  class TagBean {
 public String code;
 public String name;
 public String number;
 public String pro;
 public float proFloat;
 public String numberUnit;
 public String proUnit;
 public String icon;
 public int color;
 public int numberColor = 0XFF646464;

 public boolean showNumber = true;
 public boolean  showNumberUnit = false;
 public boolean  showPro = false;
 public boolean  showProUnit = false;
 public boolean hideColor = false;

 public boolean selected = false;


 public Object object;
}

