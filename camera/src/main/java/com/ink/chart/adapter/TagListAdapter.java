/*
 * Created by inks on  2022/9/30 上午11:46
 * 
 */

package com.ink.chart.adapter;

import android.content.Context;
import android.graphics.Paint;
import android.util.TypedValue;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.ink.camera.R;
import com.ink.chart.entity.TagBean;
import com.ink.chart.view.XRoundRectView;
import com.inks.inkslibrary.Utils.ClickUtil;

import java.util.List;


/**
 * <pre>
 *     author : inks
 *     time   : 2023/8/17
 *     desc   : tag list adapter
 *     version: 1.0
 * </pre>
 */
public class TagListAdapter extends BaseAdapter<TagBean, TagListAdapter.ItemHolder> {

    public boolean chooseEnable = false;
    //最大tag文本
    private String maxStr = "";

    private int tagLayoutId = 0;

    private int numberSize = 12;
    private int numberUnit = 12;

    public interface OnItemClickListener {
        void onItemClick(View view, TagBean info, int position);

        void onItemChoose(View view, TagBean info, int position);

    }

    public OnItemClickListener mOnItemClickListener;

    public void setOnItemClickListener(OnItemClickListener mOnItemClickListener) {
        if (mOnItemClickListener != null) {
            chooseEnable = true;
        }
        this.mOnItemClickListener = mOnItemClickListener;
    }


    public TagListAdapter(Context c, int tagLayoutId) {

        super(c);
        this.tagLayoutId = tagLayoutId;

    }


    public void setNumberSize(int numberSize) {
        this.numberSize = numberSize;
    }

    public void setNumberUnit(int numberUnit) {
        this.numberUnit = numberUnit;
    }

    @Override
    public int getItemCount() {
        return mList.size();


    }

    @Override
    public TagBean getItem(int position) {
        return position >= 0 && position < mList.size() ? mList.get(position) : null;
    }

    @Override
    public void add(TagBean item) {
        super.add(item);

    }

    public void upDate(List<TagBean> tagList) {
        this.mList = tagList;
        int length = 0;
        int maxIndex = 0;
        for (int i = 0; i < mList.size(); i++) {
            if (getTextLength(mList.get(i).name) > length) {
                length = getTextLength(mList.get(i).name);
                maxIndex = i;
            }
        }
        if(tagList.size()>0){
            maxStr = mList.get(maxIndex).name;
        }


        notifyDataSetChanged();

    }


    @Override
    protected int onBindLayout() {
        if (tagLayoutId == 0) {
            return R.layout.x_chart_tag_layout_numer_pro;
        } else {
            return tagLayoutId;
        }

    }

    @Override
    protected ItemHolder onCreateHolder(View view) {
        return new ItemHolder(view);
    }


    @Override
    protected void onBindData(ItemHolder holder, TagBean info, int position) {

        holder.tagTitleDiv.setText(maxStr);

        holder.tagColor.setColor(info.color);
        holder.tagTitle.setText(info.name);
        holder.tagNumber.setText(info.number);
        holder.tagNumber.setTextSize(TypedValue.COMPLEX_UNIT_SP, numberSize);
        holder.tagNumberTag.setText(info.numberUnit);
        holder.tagNumberTag.setTextSize(TypedValue.COMPLEX_UNIT_SP, numberUnit);
        holder.tagPro.setText(info.pro);
        holder.tagProTag.setText(info.proUnit);
        holder.tagColor.setVisibility(info.hideColor ? View.GONE : View.VISIBLE);
        holder.tagNumber.setVisibility(info.showNumber ? View.VISIBLE : View.GONE);
        holder.tagNumberTag.setVisibility(info.showNumberUnit ? View.VISIBLE : View.GONE);
        holder.tagPro.setVisibility(info.showPro ? View.VISIBLE : View.GONE);
        holder.tagProTag.setVisibility(info.showProUnit ? View.VISIBLE : View.GONE);
        holder.tagNumber.setTextColor(info.numberColor);

        if (chooseEnable) {

            if (info.selected) {
                holder.tagLayout.setBackgroundResource(R.drawable.x_tag_choose_bg_1);
                holder.tagTitle.setTextColor(0XFF3CC18C);
                holder.tagNumber.setTextColor(0XFF3CC18C);
                holder.tagNumberTag.setTextColor(0XFF3CC18C);
                holder.tagPro.setTextColor(0XFF3CC18C);
                holder.tagProTag.setTextColor(0XFF3CC18C);
            } else {
                holder.tagLayout.setBackgroundResource(R.drawable.x_tag_choose_bg_2);
                holder.tagTitle.setTextColor(0XFF646464);
                holder.tagNumber.setTextColor(0XFF646464);
                holder.tagNumberTag.setTextColor(0XFF646464);
                holder.tagPro.setTextColor(0XFF646464);
                holder.tagProTag.setTextColor(0XFF646464);
            }


            holder.tagLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mOnItemClickListener != null) {
                        if (ClickUtil.isFastDoubleClick((long) (500))) {
                            return;
                        }

                        boolean selected = info.selected;

                        if (!selected) {
                            for (int i = 0; i < mList.size(); i++) {
                                mList.get(i).selected = false;
                            }
                        }

                        info.selected = !selected;
                        notifyDataSetChanged();
                        mOnItemClickListener.onItemChoose(v, info, holder.getBindingAdapterPosition());


                    }
                }
            });
        }


    }


    public static class ItemHolder extends RecyclerView.ViewHolder {


        public LinearLayout root;
        public TextView tagTitleDiv;
        public XRoundRectView tagColor;
        public TextView tagTitle;
        public TextView tagNumber;
        public TextView tagNumberTag;
        public TextView tagPro;
        public TextView tagProTag;
        public View tagLayout;

        public ItemHolder(View itemView) {
            super(itemView);
            root = itemView.findViewById(R.id.tag_root);
            tagTitleDiv = itemView.findViewById(R.id.tag_title_div);
            tagColor = itemView.findViewById(R.id.tag_color);
            tagTitle = itemView.findViewById(R.id.tag_title);
            tagNumber = itemView.findViewById(R.id.tag_number);
            tagNumberTag = itemView.findViewById(R.id.tag_number_tag);
            tagPro = itemView.findViewById(R.id.tag_pro);
            tagProTag = itemView.findViewById(R.id.tag_pro_tag);
            tagLayout = itemView.findViewById(R.id.tag_layout);



        }

    }


    private int getTextLength(String text) {
        Paint paint = new Paint();
        paint.setTextSize(sp2px(12));
        //Rect rect = new Rect();
        float width = paint.measureText(text);
        return (int) width;
//        paint.getTextBounds(text, 0, text.length(), rect);
//        return rect.width();
    }


    public int sp2px(float spValue) {
        return (int) (spValue * mContext.getResources().getDisplayMetrics().scaledDensity + 0.5f);
    }

}
