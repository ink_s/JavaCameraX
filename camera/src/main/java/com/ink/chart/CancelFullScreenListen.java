package com.ink.chart;

import android.view.View;

/**
 * <pre>
 *     author : inks
 *     time   : 2024/04/10
 *     desc   :
 *     version: 1.0
 * </pre>
 */
public   interface CancelFullScreenListen {
    void cancel(View view);
}
