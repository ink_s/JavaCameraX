package com.ink.chart.view;

import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.os.Handler;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.core.widget.NestedScrollView;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.google.android.flexbox.FlexboxLayout;
import com.google.gson.Gson;
import com.ink.camera.R;
import com.ink.chart.CancelFullScreenListen;
import com.ink.chart.entity.ChartData;
import com.ink.chart.entity.TagClickInfo;
import com.ink.util.GradientDrawableUtil;
import com.ink.util.StringUtils;
import com.inks.inkslibrary.Utils.ClickUtil;
import com.inks.inkslibrary.Utils.L;

import java.util.ArrayList;
import java.util.List;


public class XLineChartView extends RelativeLayout {

    public String noDataText = "暂无数据";

    public enum Mode {
        LINEAR,
        STEPPED,
        CUBIC_BEZIER,
        HORIZONTAL_BEZIER
    }

    public enum TagNumberShow {
        //整数，小数，小数及百分比，整数及百分比,只有百分比
        NUMBER, FLOAT_NUMBER, FLOAT_PRO, NUMBER_PRO, PRO
    }

    private LineChart chart;
    private FlexboxLayout tagLayout;
    private FrameLayout dataLayout;
    private TextView unitText, unitAxis;
    private TextView noData;
    private NestedScrollView clickTagBg;
    private LinearLayout clickTagLayout;

    private ImageView chartReduceAmplify;

    private Params params = new Params();

    public static class Params {

        //是否可以全屏
        public boolean canFullScreen = true;
        public CancelFullScreenListen cancelListen;
        //0 竖屏，1横屏
        public String fullOrientation = "1";
        //是否在全屏页面
        public boolean fullScreen = false;

        //全屏时始终显示图例，否者与非全屏一致
        public boolean fullAlwaysShowTag = true;

        public void setCancelListen(CancelFullScreenListen cancelListen) {
            this.cancelListen = cancelListen;
        }

        public void setFullScreen(boolean fullScreen) {
            this.fullScreen = fullScreen;
        }

        public ChartData data;
        public float total = 0;
        public TagNumberShow tagNumberShow = TagNumberShow.NUMBER;

        //颜色
        public int[] colorAll;


        //Y轴单位
        public String yUnit = "";
        //X轴单位
        public String xUnit = "";
        public int yUnitColor = 0XFF2B2B2B;
        public int xUnitColor = 0XFF2B2B2B;
        //number 单位
        public String numberUnit = "";
        //pro 单位
        public String proUnit = "%";

        //不显示图例
        public boolean hideTag = false;
        //图例居中
        public int gravity = Gravity.CENTER;
        //x轴角度
        public float labelRotationAngle = 0;
        //画小圆圈
        public boolean drawCircles = true;
        //小圆圈大小
        public float circleRadius = 3.5f;

        //小圆圈半径
        public float circleHoleRadius = 2f;

        //线宽
        public float lineWidth = 1.5f;
        //是否虚线
        public boolean dashedLine = false;
        //是否填充
        public boolean drawFilled = false;
        //填充渐变透明度，255-0
        public int[] filledAlpha = new int[]{180, 90};

        //是否自动移动到有数据的地方
        public boolean autoMove = true;

        public int delayMillis = 0;


        //tab背景颜色
        public int tagBgColor = 0X55BBBBBB;
        public int tagColor = 0XFF2b2b2b;
        public int tagTitleColor = 0XFF2b2b2b;
        //tag 数字格式（是否带分隔符）
        public boolean numberSplit = true;

        public LineDataSet.Mode mMode = LineDataSet.Mode.LINEAR;
        //最大显示X轴条数，大于则放大
        public int maxXsize = 12;
        //全屏时大小
        public int fullMaxXsize = 24;
        public float mExtraTopOffset = 0.f,
                mExtraRightOffset = 10.f,
                mExtraBottomOffset = 5.f,
                mExtraLeftOffset = 0.f;

        public int layoutId = R.layout.x_line_chart;

    }


    private Context context;

    private boolean haveInit = false;


    public XLineChartView(Context context) {
        super(context);
    }

    public XLineChartView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public XLineChartView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        this(context, attrs, defStyleAttr, 0);
    }

    public XLineChartView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);

        this.context = context;


        init(attrs);
        upData();

    }

    public void init(AttributeSet attrs) {
        if (params == null) {
            params = new Params();
        }

        if (params.colorAll == null) {
            params.colorAll = getResources().getIntArray(R.array.x_pie_color);
        }
        if (attrs != null) {
            TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.XLineChartView);
            params.layoutId = typedArray.getResourceId(R.styleable.XLineChartView_layoutId, params.layoutId);
            typedArray.recycle();
        }

        View view = LayoutInflater.from(getContext()).inflate(params.layoutId, null);

        chart = view.findViewById(R.id.chart);
        chart.setNoDataText("");
        tagLayout = view.findViewById(R.id.tag_layout);
        dataLayout = view.findViewById(R.id.data_layout);
        noData = view.findViewById(R.id.no_data);
        unitText = view.findViewById(R.id.chart_unit);
        unitAxis = view.findViewById(R.id.chart_axis);
        clickTagBg = view.findViewById(R.id.chart_click_bg);
        clickTagLayout = view.findViewById(R.id.chart_click_layout);
        chartReduceAmplify = view.findViewById(R.id.chart_reduce_amplify);
        chartReduceAmplify.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!ClickUtil.isFastDoubleClick((long) (300))) {
                    if (params.fullScreen) {
                        if (params.cancelListen != null) {
                            params.cancelListen.cancel(v);
                        }
                    } else {
                        Intent intent = new Intent(getContext(), XChartFullScreenActivity.class);
                        intent.putExtra("type", XChartFullScreenActivity.ChartType.LINE.name());
                        intent.putExtra("paramsJson", new Gson().toJson(params));
                        intent.putExtra("orientation", params.fullOrientation);
                        getContext().startActivity(intent);
                    }
                }
            }
        });
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
        removeAllViews();
        addView(view, params);

        haveInit = true;
    }


    public void upData() {

        if (!haveInit) {
            return;
        }

        chartReduceAmplify.setVisibility(params.canFullScreen ? VISIBLE : GONE);
        if (params.fullScreen) {
            chartReduceAmplify.setImageResource(R.drawable.x_chart_reduce);
        } else {
            chartReduceAmplify.setImageResource(R.drawable.x_chart_amplify);
        }


        clickTagBg.setVisibility(View.GONE);
        clickTagBg.setBackground(GradientDrawableUtil.getGradientDrawable(params.tagBgColor));
        if (params.data == null || params.data.xData == null || params.data.xData.length == 0 || params.data.yData == null || params.data.yData.length == 0) {
            noData.setVisibility(View.VISIBLE);
            noData.setText(noDataText);
            dataLayout.setVisibility(View.GONE);
            return;
        } else {
            noData.setVisibility(View.GONE);
            dataLayout.setVisibility(View.VISIBLE);
        }

        if (TextUtils.isEmpty(params.yUnit)) {
            unitText.setText("");
        } else {
            unitText.setText(params.yUnit);
        }


        if (TextUtils.isEmpty(params.xUnit)) {
            unitAxis.setText("");
        } else {
            unitAxis.setText(params.xUnit);
        }

        unitText.setTextColor(params.yUnitColor);
        unitAxis.setTextColor(params.xUnitColor);

        if (params.fullScreen && params.fullAlwaysShowTag) {
            tagLayout.setVisibility(View.VISIBLE);
        } else {
            tagLayout.setVisibility(params.hideTag ? View.GONE : View.VISIBLE);
        }
        tagLayout.removeAllViews();

        FrameLayout.LayoutParams p = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        p.gravity = params.gravity;
        tagLayout.setLayoutParams(p);


        //自动滚动到第一个有数值的数据
        int haveDataIndex = -1;

        //x轴值
        List<String> xAxle = new ArrayList<>();
        for (int i = 0; i < params.data.xData.length; i++) {
            xAxle.add(TextUtils.isEmpty(params.data.xData[i]) ? "未知" : params.data.xData[i]);
        }


        List<ArrayList<Entry>> values = new ArrayList<>();
        //点击显示标签用
        List<String> tags = new ArrayList<>();


        for (int i = 0; i < params.data.yData.length; i++) {
            ArrayList<Entry> entries = new ArrayList<>();
            for (int j = 0; j < params.data.xData.length; j++) {
                entries.add(new Entry(j, params.data.yData[i].data[j].number));
                if (params.data.yData[i].data[j].number > 0) {
                    if (haveDataIndex == -1) {
                        haveDataIndex = j;
                    } else {
                        haveDataIndex = Math.min(haveDataIndex, j);
                    }
                }
            }
            values.add(entries);


            //添加图例
            View layout = LayoutInflater.from(getContext()).inflate(R.layout.x_chart_one_tag_no_number, null);
            layout.setPadding(0, 0, 30, 0);
            View colorView = (View) layout.findViewById(R.id.color_view);
            TextView nameTextView = (TextView) layout.findViewById(R.id.name);
            colorView.setBackgroundColor(params.colorAll[i % params.colorAll.length]);
            nameTextView.setText(TextUtils.isEmpty(params.data.yData[i].name) ? "未知" : params.data.yData[i].name);
            tagLayout.addView(layout);
            //点击显示标签用
            tags.add(TextUtils.isEmpty(params.data.yData[i].name) ? "未知" : params.data.yData[i].name);

        }


        chart.clear();
        chart.highlightValues(null);
        chart.zoom(0.0f, 0.0f, 0f, 0f);

        chart.setGridBackgroundColor(Color.WHITE);
        chart.getDescription().setText("");
        chart.setMaxVisibleValueCount(50);
        chart.setDrawGridBackground(false);


        XAxis xAxis = chart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setTypeface(Typeface.DEFAULT);
        xAxis.setDrawGridLines(false);
        xAxis.setLabelCount(xAxle.size(), false);
        xAxis.setGranularity(1f);
        xAxis.setLabelRotationAngle(params.labelRotationAngle);
        xAxis.setGranularityEnabled(true);
        xAxis.setValueFormatter(new ValueFormatter() {
            @Override
            public String getFormattedValue(float value) {
                int x = (int) (value);
                if (x >= 0 && x < xAxle.size()) {
                    return xAxle.get(x);
                } else {
                    return "";
                }
            }
        });


        YAxis leftAxis = chart.getAxisLeft();
        leftAxis.setTypeface(Typeface.DEFAULT);
        leftAxis.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART);
        leftAxis.setAxisMinimum(0f);
        leftAxis.setGranularity(1f);
        leftAxis.setGridColor(0X88AFAFAF);


        YAxis rightAxis = chart.getAxisRight();
        rightAxis.setEnabled(false);
        Legend l = chart.getLegend();
        l.setEnabled(false);


        ArrayList<ILineDataSet> dataSets = new ArrayList<>();
        for (int i = 0; i < values.size(); i++) {
            LineDataSet dataSet = new LineDataSet(values.get(i), "DataSet");
            dataSet.setDrawValues(false); // 不显示值
            dataSet.setColor(params.colorAll[i % params.colorAll.length]);//两个点之间的距离连接线
            dataSet.setCircleColor(params.colorAll[i % params.colorAll.length]);//数据展示的圆点颜色
            dataSet.setLineWidth(params.lineWidth);//设置线宽

            if (params.dashedLine) {
                dataSet.enableDashedLine(20f, 5f, 0f);//虚线
            } else {
                dataSet.disableDashedLine();
            }

            dataSet.setCircleRadius(params.circleRadius);//设置焦点圆心的大小
            dataSet.setCircleHoleRadius(params.circleHoleRadius);
            dataSet.setDrawCircles(params.drawCircles);
            dataSet.setMode(params.mMode);
            dataSet.setDrawFilled(params.drawFilled);

            if (params.drawFilled) {
                //背景色及圆角
                GradientDrawable drawable = new GradientDrawable();
                //形状（矩形）
                drawable.setShape(GradientDrawable.RECTANGLE);
                //渐变样式
                drawable.setGradientType(GradientDrawable.LINEAR_GRADIENT);
                //渐变方向（左到右）
                drawable.setOrientation(GradientDrawable.Orientation.TOP_BOTTOM);
                //圆角
                // drawable.setCornerRadii(selectSettings.getPopupRadius());
                //颜色
                int color = params.colorAll[i % params.colorAll.length];
                int transparentColor1 = Color.argb(params.filledAlpha[0], Color.red(color), Color.green(color), Color.blue(color));
                int transparentColor2 = Color.argb(params.filledAlpha[1], Color.red(color), Color.green(color), Color.blue(color));
                drawable.setColors(new int[]{transparentColor1, transparentColor2});
                dataSet.setFillDrawable(drawable);
            }


            dataSet.setValueFormatter(new ValueFormatter() {
                @Override
                public String getBarLabel(BarEntry barEntry) {
                    return (int) barEntry.getY() + "";
                }

                @Override
                public String getBarStackedLabel(float value, BarEntry stackedEntry) {
                    return super.getBarStackedLabel(value, stackedEntry);
                }
            });
            dataSets.add(dataSet); // 添加数据集
        }

        //创建LineData对象 属于LineChart折线图的数据集合
        LineData lineData = new LineData(dataSets);
        chart.setData(lineData);
        chart.getDescription().
                setEnabled(false); // 不显示描述
        chart.setExtraBottomOffset(params.mExtraBottomOffset);
        chart.setExtraRightOffset(params.mExtraRightOffset);
        chart.setExtraLeftOffset(params.mExtraLeftOffset);
        chart.setExtraTopOffset(params.mExtraTopOffset);

        chart.animateY(300);
        chart.invalidate();
        int finalHaveDataIndex = haveDataIndex - 1;


        float scaleX = xAxle.size() / (float) (params.fullScreen ? params.fullMaxXsize : params.maxXsize);
        float moveX = -9999f;
        if (params.autoMove) {
            moveX = finalHaveDataIndex;
        }


        if (params.delayMillis <= 0) {
            zoomMove(scaleX, moveX);
        } else {
            float finalMoveX = moveX;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    zoomMove(scaleX, finalMoveX);
                }
            }, params.delayMillis);
        }


        chart.notifyDataSetChanged();
        chart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
            @Override
            public void onValueSelected(Entry e, Highlight h) {
                int x = (int) (e.getX());
                if (x >= 0 && x < xAxle.size()) {
                    List<TagClickInfo> map = new ArrayList<>();
                    try {
                        if (params.data.yData.length == tags.size()) {
                            for (int i = 0; i < values.size(); i++) {
                                //外层循环，
                                //没有当前月的值

                                float a = params.data.yData[i].data[x].number;
                                if (a > 0) {
                                    float pro = params.data.yData[i].data[x].pro;
                                    map.add(new TagClickInfo(params.colorAll[i % params.colorAll.length], tags.get(i), a, pro));
                                }

                            }
                        }
                        showBarClickTag(xAxle.get(x), map, clickTagLayout, clickTagBg, true);

                    } catch (Exception exception) {
                        showBarClickTag(xAxle.get(x), null, clickTagLayout, clickTagBg, false);
                    }

                } else {
                    showBarClickTag(xAxle.get(x), null, clickTagLayout, clickTagBg, false);
                }

            }

            @Override
            public void onNothingSelected() {
                showBarClickTag("", null, clickTagLayout, clickTagBg, false);
            }
        });


    }


    public void zoomMove(float scaleX, float moveX) {
        chart.zoom((float) scaleX < 1 ? 1 : scaleX, 1.0f, 0f, 0f);
        chart.moveViewTo(moveX, 9999f, YAxis.AxisDependency.LEFT);
    }


    private void showBarClickTag(String tag, List<TagClickInfo> map, LinearLayout linearLayout, NestedScrollView bgView, boolean show) {

        //    L.ee(tag, map);


        if (!show || map == null || map.size() == 0) {
            bgView.setVisibility(View.GONE);
            return;
        }
        bgView.setVisibility(View.VISIBLE);
        linearLayout.removeAllViews();


        //添加标题
        View tagLayout = LayoutInflater.from(getContext()).inflate(R.layout.x_text_tag, null);
        TextView tagView = tagLayout.findViewById(R.id.tag_title);
        tagView.setText(tag);
        tagView.setTextColor(params.tagTitleColor);

        linearLayout.addView(tagLayout);

        for (int i = 0; i < map.size(); i++) {
            View layout = LayoutInflater.from(getContext()).inflate(R.layout.x_chart_one_tag_numer_pro, null);
            XRoundRectView colorView = layout.findViewById(R.id.tag_color);
            TextView titleView = layout.findViewById(R.id.tag_title);
            TextView numberView = layout.findViewById(R.id.tag_number);
            TextView proView = layout.findViewById(R.id.tag_pro);
            TextView numberTag = layout.findViewById(R.id.tag_number_tag);
            TextView proTag = layout.findViewById(R.id.tag_pro_tag);
            colorView.setColor(map.get(i).color);
            titleView.setText(map.get(i).name);
            titleView.setTextColor(params.tagColor);
            numberView.setTextColor(params.tagColor);
            numberTag.setTextColor(params.tagColor);
            proView.setTextColor(params.tagColor);
            proTag.setTextColor(params.tagColor);

            numberTag.setText(params.numberUnit);
            proView.setText(StringUtils.formatStr(map.get(i).pro + "", 2, false, params.numberSplit));
            proTag.setText(params.proUnit);

            if (params.tagNumberShow == TagNumberShow.NUMBER) {
                //整数
                numberView.setText(StringUtils.formatStr(map.get(i).number + "", 0, false, params.numberSplit));
                proView.setVisibility(View.GONE);
                proTag.setVisibility(View.GONE);
                numberView.setVisibility(View.VISIBLE);
                numberTag.setVisibility(View.VISIBLE);

            } else if (params.tagNumberShow == TagNumberShow.FLOAT_NUMBER) {
                numberView.setText(StringUtils.formatStr(map.get(i).number + "", 2, false, params.numberSplit));
                proView.setVisibility(View.GONE);
                proTag.setVisibility(View.GONE);
                numberView.setVisibility(View.VISIBLE);
                numberTag.setVisibility(View.VISIBLE);

            } else if (params.tagNumberShow == TagNumberShow.FLOAT_PRO) {
                numberView.setText(StringUtils.formatStr(map.get(i).number + "", 2, false, params.numberSplit));
                proView.setVisibility(View.VISIBLE);
                proTag.setVisibility(View.VISIBLE);
                numberView.setVisibility(View.VISIBLE);
                numberTag.setVisibility(View.VISIBLE);

            } else if (params.tagNumberShow == TagNumberShow.NUMBER_PRO) {
                numberView.setText(StringUtils.formatStr(map.get(i).number + "", 0, false, params.numberSplit));
                proView.setVisibility(View.VISIBLE);
                proTag.setVisibility(View.VISIBLE);
                numberView.setVisibility(View.VISIBLE);
                numberTag.setVisibility(View.VISIBLE);

            } else if (params.tagNumberShow == TagNumberShow.PRO) {
                proView.setVisibility(View.VISIBLE);
                proTag.setVisibility(View.VISIBLE);
                numberView.setVisibility(View.GONE);
                numberTag.setVisibility(View.GONE);
            }

            linearLayout.addView(layout);
        }
    }


    public void setData(ChartData data, TagNumberShow tagNumberShow) {
        this.params.data = data;
        this.params.tagNumberShow = tagNumberShow;
    }

    public void setAutoMove(boolean autoMove) {
        this.params.autoMove = autoMove;
    }

    public void setTotal(float total) {
        this.params.total = total;
    }

    public void setColorAll(int[] colorAll) {
        this.params.colorAll = colorAll;
    }


    public void setyUnit(String yUnit) {
        this.params.yUnit = yUnit;
    }

    public void setxUnit(String xUnit) {
        this.params.xUnit = xUnit;
    }

    public void setyUnitColor(int yUnitColor) {
        this.params.yUnitColor = yUnitColor;
    }

    public void setxUnitColor(int xUnitColor) {
        this.params.xUnitColor = xUnitColor;
    }

    public void setNumberUnit(String numberUnit) {
        this.params.numberUnit = numberUnit;
    }

    public void setProUnit(String proUnit) {
        this.params.proUnit = proUnit;
    }

    public void setHideTag(boolean hideTag) {
        this.params.hideTag = hideTag;
    }

    public void setTagGravity(int gravity) {
        this.params.gravity = gravity;
    }

    public void setLabelRotationAngle(float labelRotationAngle) {
        this.params.labelRotationAngle = labelRotationAngle;
    }

    public void setDrawCircles(boolean drawCircles) {
        this.params.drawCircles = drawCircles;
    }

    public void setCircleRadius(float circleRadius) {
        this.params.circleRadius = circleRadius;
    }

    public void setCircleHoleRadius(float circleHoleRadius) {
        this.params.circleHoleRadius = circleHoleRadius;
    }

    public void setLineWidth(float lineWidth) {
        this.params.lineWidth = lineWidth;
    }

    public void setDashedLine(boolean dashedLine) {
        this.params.dashedLine = dashedLine;
    }

    public void setMode(Mode mMode) {
        if (mMode == Mode.LINEAR) {
            this.params.mMode = LineDataSet.Mode.LINEAR;
        } else if (mMode == Mode.STEPPED) {
            this.params.mMode = LineDataSet.Mode.STEPPED;
        } else if (mMode == Mode.CUBIC_BEZIER) {
            this.params.mMode = LineDataSet.Mode.CUBIC_BEZIER;
        } else if (mMode == Mode.HORIZONTAL_BEZIER) {
            this.params.mMode = LineDataSet.Mode.HORIZONTAL_BEZIER;
        }

    }

    public void setMaxXsize(int maxXsize) {
        this.params.maxXsize = maxXsize;
    }


    public void setFullMaxXsize(int fullMaxXsize) {
        this.params.fullMaxXsize = fullMaxXsize;
    }


    public void setmExtraTopOffset(float mExtraTopOffset) {
        this.params.mExtraTopOffset = mExtraTopOffset;
    }

    public void setmExtraRightOffset(float mExtraRightOffset) {
        this.params.mExtraRightOffset = mExtraRightOffset;
    }

    public void setmExtraBottomOffset(float mExtraBottomOffset) {
        this.params.mExtraBottomOffset = mExtraBottomOffset;
    }

    public void setmExtraLeftOffset(float mExtraLeftOffset) {
        this.params.mExtraLeftOffset = mExtraLeftOffset;
    }

    public void setDrawFilled(boolean drawFilled) {
        this.params.drawFilled = drawFilled;
    }

    public void setFilledAlpha(int[] filledAlpha) {
        this.params.filledAlpha = filledAlpha;
    }

    public void setTagBgColor(int tagBgColor) {
        this.params.tagBgColor = tagBgColor;
    }

    public void setTagColor(int tagColor) {
        this.params.tagColor = tagColor;
    }

    public void setTagTitleColor(int tagTitleColor) {
        this.params.tagTitleColor = tagTitleColor;
    }

    public void setCanFullScreen(boolean canFullScreen) {
        this.params.canFullScreen = canFullScreen;
    }

    public void setFullOrientation(String fullOrientation) {
        this.params.fullOrientation = fullOrientation;
    }

    public void setFullAlwaysShowTag(boolean fullAlwaysShowTag) {
        this.params.fullAlwaysShowTag = fullAlwaysShowTag;
    }


    public void setParams(Params params) {
        this.params = params;
    }

    public Params getParams() {
        return params;
    }
    public void setNoDataText(String text){
        this.noDataText = text;
        if(noData!=null){
            noData.setText(text);
        }
    }


}
