package com.ink.chart.view;

import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import com.google.gson.Gson;
import com.ink.camera.R;
import com.ink.chart.CancelFullScreenListen;
import com.ink.view.InterceptRelativeLayout;
import com.inks.inkslibrary.Utils.ClickUtil;
import com.inks.inkslibrary.Utils.L;


/**
 * <pre>
 *     author : inks
 *     time   : 2022/7/18
 *     desc   : 调用系统相机，选择拍照或录视频
 *     version: 1.0
 * </pre>
 */
public class XChartFullScreenActivity extends Activity {

    private CardView layout;

    public enum ChartType {
        H_BAR, BAR,LINE
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        String type = getIntent().getStringExtra("type");
        String paramsJson = getIntent().getStringExtra("paramsJson");
        String orientation = getIntent().getStringExtra("orientation");
        if("1".equals(orientation)){
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        }else{
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }


        setContentView(R.layout.x_chart_full_screen);
        layout = findViewById(R.id.content);
        findViewById(R.id.root_layout).setOnClickListener(click);
       // findViewById(R.id.root_view).setOnClickListener(click);
        layout.setOnClickListener(click);

        if (TextUtils.isEmpty(type) || TextUtils.isEmpty(paramsJson)) {
            finish();
            return;
        }

        if (ChartType.H_BAR.name().equals(type)) {
            XBarChartHorizontalView view = new XBarChartHorizontalView(this);
            XBarChartHorizontalView.Params params = new Gson().fromJson(paramsJson, XBarChartHorizontalView.Params.class);
            params.setFullScreen(true);
            params.setCancelListen(new CancelFullScreenListen() {
                @Override
                public void cancel(View view) {
                    onBackPressed();
                }
            });

            view.setParams(params);
            view.init(null);
            LinearLayout.LayoutParams p = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            p.gravity = params.gravity;
            layout.addView(view,p);
            view.upData();

        } else if (ChartType.BAR.name().equals(type)) {
            XBarChartView view = new XBarChartView(this);
            XBarChartView.Params params = new Gson().fromJson(paramsJson, XBarChartView.Params.class);
            params.setFullScreen(true);
            params.setCancelListen(new CancelFullScreenListen() {
                @Override
                public void cancel(View view) {
                    onBackPressed();
                }
            });

            view.setParams(params);
            view.init(null);
            LinearLayout.LayoutParams p = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            p.gravity = params.gravity;
            layout.addView(view,p);
            view.upData();

        } else if (ChartType.LINE.name().equals(type)) {
            XLineChartView view = new XLineChartView(this);
            XLineChartView.Params params = new Gson().fromJson(paramsJson, XLineChartView.Params.class);
            params.setFullScreen(true);
            params.setCancelListen(new CancelFullScreenListen() {
                @Override
                public void cancel(View view) {
                    onBackPressed();
                }
            });

            view.setParams(params);
            view.init(null);
            LinearLayout.LayoutParams p = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            p.gravity = params.gravity;
            layout.addView(view,p);
            view.upData();

        } else {
            finish();
        }


    }



    View.OnClickListener click = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (!ClickUtil.isFastDoubleClick((long) (300))) {
                if (v.getId() == R.id.root_layout) {
                   finish();
                }
            }

        }
    };


}