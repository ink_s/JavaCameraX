package com.ink.chart.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.ink.camera.R;
import com.ink.chart.adapter.TagListAdapter;
import com.ink.chart.entity.TagBean;
import com.ink.util.StringUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;


public class XPieChartView extends RelativeLayout {

    public String noDataText = "暂无数据";
    public enum AllNumberShow {
        //整数，小数，小数及百分比，整数及百分比
        NUMBER, FLOAT_NUMBER, FLOAT_PRO, NUMBER_PRO
    }


    private List<TagBean> data = new ArrayList<>();
    private float total = 0;
    private AllNumberShow allNumberShow = AllNumberShow.NUMBER;

    private int allNumberColor = 0XFF7B7B7B;
    private int allTextColor = 0XFF7B7B7B;
    private int allUnitColor = 0XFF7B7B7B;


    private int allNumberDp = 12;
    private int allTextDp = 12;
    private int allUnitDp = 12;


    //圆环中间显示非百分比是数据单位
    private String allNumberUnit = "";
    //是否显示中间的数字
    private boolean showCenter = true;
    //自定义得tag
    private int tagLayoutId;
    //颜色
    private int[] colorAll;
    //空洞大小
    private float holeRadius = 80f;
    ////透明边缘大小
    private float transparentCircleRadius = 85f;
    //显示空洞
    private boolean drawHoleEnabled = true;
    //空隙
    private float sliceSpace = 0f;
    //不显示图例
    private boolean hideTag = false;
    //饼图点击中间是否变化
    private boolean changeCenter = true;


    //图例的文字
    private int numberSize = 12;
    private int numberUnit = 12;

    //
    private int imagePadding = 0;
    private int imageId = 0;

    //外边的圆环
    private boolean hideRound = false;
    private int roundColor = 0XFFE6F3FA;
    private int roundWidth = 5;

    //tag 数字格式（是否带分隔符）
    private boolean numberSplit = true;


    private PieChart chart;
    private LinearLayout centerLayout;
    private TextView allNumber;
    private TextView allText;
    private TextView allUnit;
    private RecyclerView tagLayout;
    private XCircularOuterView roundView;

    private LinearLayout dataLayout;
    private TextView noData;
    private LinearLayout tagLayoutBg;

    private ImageView image;

    private Context context;

    private boolean haveInit = false;

    int layoutId = R.layout.x_pie_chart_left;

    private TagListAdapter adapter;

    int direction = 0;

    private TagListAdapter.OnItemClickListener mOnItemClickListener;

    public void setOnItemClickListener(TagListAdapter.OnItemClickListener mOnItemClickListener) {
        this.mOnItemClickListener = mOnItemClickListener;
    }


    public XPieChartView(Context context) {
        this(context, null);
    }

    public XPieChartView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public XPieChartView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        this(context, attrs, defStyleAttr, 0);
    }

    public XPieChartView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);

        this.context = context;


        init(attrs);
        upData();

    }

    private void init(AttributeSet attrs) {
        colorAll = getResources().getIntArray(R.array.x_pie_color);

        if (attrs != null) {
            TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.XPieChartView);
            //方向 0 左   其他右   默认左
            direction  = typedArray.getInt(R.styleable.XPieChartView_direction, 0);
            if (direction == 0) {
                layoutId = R.layout.x_pie_chart_left;
            } else {
                layoutId = R.layout.x_pie_chart_right;
            }

            layoutId = typedArray.getResourceId(R.styleable.XPieChartView_layoutId, layoutId);
            tagLayoutId = typedArray.getResourceId(R.styleable.XPieChartView_tagId, 0);

            imagePadding = typedArray.getDimensionPixelSize(R.styleable.XPieChartView_xpie_padding, 0);
            imageId = typedArray.getResourceId(R.styleable.XPieChartView_xpie_iamge, 0);
            typedArray.recycle();
        }

        View view = LayoutInflater.from(getContext()).inflate(layoutId, null);

        image = view.findViewById(R.id.image);
        image.setPadding(imagePadding, imagePadding, imagePadding, imagePadding);

        if (imageId != 0) {
            image.setImageResource(imageId);
        }


        chart = view.findViewById(R.id.chart);
        chart.setNoDataText("");
        centerLayout = view.findViewById(R.id.center_layout);
        allNumber = view.findViewById(R.id.all_number);
        allText = view.findViewById(R.id.all_title);
        allUnit = view.findViewById(R.id.all_unit);
        tagLayout = view.findViewById(R.id.tag_layout);
        dataLayout = view.findViewById(R.id.data_layout);
        noData = view.findViewById(R.id.no_data);
        tagLayoutBg = view.findViewById(R.id.tag_layout_bg);
        roundView = view.findViewById(R.id.round_view);
        GridLayoutManager layoutManager = new GridLayoutManager(context, 1);
        tagLayout.setLayoutManager(layoutManager);

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
        addView(view, params);

        haveInit = true;
    }


    public void upData() {

        if (!haveInit) {
            return;
        }

        if (data == null || data.size() == 0) {
            noData.setVisibility(View.VISIBLE);
            noData.setText(noDataText);
            dataLayout.setVisibility(View.GONE);
        } else {
            noData.setVisibility(View.GONE);
            dataLayout.setVisibility(View.VISIBLE);
        }

        roundView.setVisibility(hideRound ? GONE : VISIBLE);
        roundView.setBgColor(roundColor);
        roundView.setRingWidth(roundWidth);
        roundView.init(null);
        roundView.invalidate();


        for (int i = 0; i < data.size(); i++) {
            data.get(i).color = colorAll[i % colorAll.length];
            if (allNumberShow == AllNumberShow.FLOAT_NUMBER || allNumberShow == AllNumberShow.FLOAT_PRO) {
                data.get(i).number = StringUtils.formatStr(data.get(i).number, 2, false,numberSplit);
            } else {
                data.get(i).number = StringUtils.formatStr(data.get(i).number, 0, false,numberSplit);
            }


        }


        tagLayoutBg.setVisibility(hideTag ? View.GONE : View.VISIBLE);

        centerLayout.setVisibility(showCenter ? View.VISIBLE : View.GONE);
        allNumber.setTextColor(allNumberColor);
        allText.setTextColor(allTextColor);
        allUnit.setTextColor(allUnitColor);

        allNumber.setTextSize(TypedValue.COMPLEX_UNIT_DIP, allNumberDp);
        allText.setTextSize(TypedValue.COMPLEX_UNIT_DIP, allTextDp);
        allUnit.setTextSize(TypedValue.COMPLEX_UNIT_DIP, allUnitDp);


        chart.clear();
        chart.highlightValues(null);

        chart.setHoleRadius(holeRadius);//空洞大小
        chart.setTransparentCircleRadius(transparentCircleRadius);//透明边缘大小
        chart.setDrawHoleEnabled(drawHoleEnabled); // 显示饼图中间的空洞


        List<PieEntry> pies = new ArrayList<>();
        for (TagBean tagBean : data) {
            pies.add(new PieEntry(Float.parseFloat(tagBean.pro), tagBean.name));
        }
        PieDataSet dataSet = new PieDataSet(pies, "");
        ArrayList<Integer> colors = new ArrayList<Integer>();
        for (int i = 0; i < colorAll.length; i++) {
            colors.add(colorAll[i]);
        }
        dataSet.setColors(colors);
        dataSet.setValueTextSize(0);
        dataSet.setSliceSpace(sliceSpace);

        PieData pieData = new PieData(dataSet);
        pieData.setDrawValues(false);
        chart.setDrawCenterText(false);//显示中间文字
        chart.setEntryLabelTextSize(0);//标签不显示
        Description description = new Description();//设置描述
        description.setText("");
        chart.setDescription(description);
        Legend legend = chart.getLegend();
        legend.setEnabled(false);
        chart.setData(pieData);
        chart.invalidate();
        chart.animateXY(500, 500);


        if (allNumberShow == AllNumberShow.FLOAT_NUMBER || allNumberShow == AllNumberShow.FLOAT_PRO) {
            allNumber.setText(StringUtils.formatStr(total+"", 2, false,numberSplit));
        } else {
            allNumber.setText(StringUtils.formatStr(total+"", 0, false,numberSplit));
        }

        allUnit.setText(allNumberUnit);

        allText.setText("总数");


        chart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
            @Override
            public void onValueSelected(Entry e, Highlight h) {

                if (!changeCenter) {
                    return;
                }

                PieEntry pieEntry = (PieEntry) e;
                String label = pieEntry.getLabel();
                allText.setText(label);
                for (int i = 0; i < data.size(); i++) {
                    if (!TextUtils.isEmpty(label) && label.equals(data.get(i).name)) {
                        if (allNumberShow == AllNumberShow.FLOAT_NUMBER) {
                            allNumber.setText(StringUtils.formatStr(data.get(i).number, 2, false,numberSplit));
                            allUnit.setText(allNumberUnit);
                        } else if (allNumberShow == AllNumberShow.FLOAT_PRO || allNumberShow == AllNumberShow.NUMBER_PRO) {
                            allNumber.setText(StringUtils.formatStr(data.get(i).pro, 2, false,numberSplit));
                            allUnit.setText("%");
                        } else {
                            allNumber.setText(StringUtils.formatStr(data.get(i).number, 0, false,numberSplit));
                            allUnit.setText(allNumberUnit);
                        }
                        break;
                    }
                }
            }

            @Override
            public void onNothingSelected() {
                if (!changeCenter) {
                    return;
                }
                if (allNumberShow == AllNumberShow.FLOAT_NUMBER || allNumberShow == AllNumberShow.FLOAT_PRO) {
                    allNumber.setText(StringUtils.formatStr(total+"", 2, false, numberSplit));
                } else {
                    allNumber.setText(StringUtils.formatStr(total+"", 0, false,numberSplit));
                }
                allUnit.setText(allNumberUnit);
                allText.setText("总数");
            }
        });


        adapter = new TagListAdapter(context, tagLayoutId);
        adapter.setNumberSize(numberSize);
        adapter.setNumberUnit(numberUnit);
        tagLayout.setAdapter(adapter);
        adapter.setOnItemClickListener(mOnItemClickListener);
        adapter.upDate(data);

    }


    public void setData(List<TagBean> data, float total, AllNumberShow allNumberShow) {
        this.data = data;
        this.total = total;
        this.allNumberShow = allNumberShow;
    }


    public void setAllNumberColor(int allNumberColor) {
        this.allNumberColor = allNumberColor;
    }

    public void setAllTextColor(int allTextColor) {
        this.allTextColor = allTextColor;
    }

    public void setShowCenter(boolean showCenter) {
        this.showCenter = showCenter;
    }

    public void setTagLayoutId(int tagLayoutId) {
        this.tagLayoutId = tagLayoutId;
    }

    public void setColorAll(int[] colorAll) {
        this.colorAll = colorAll;
    }

    public void setHoleRadius(float holeRadius) {
        this.holeRadius = holeRadius;
    }

    public void setTransparentCircleRadius(float transparentCircleRadius) {
        this.transparentCircleRadius = transparentCircleRadius;
    }

    public void setDrawHoleEnabled(boolean drawHoleEnabled) {
        this.drawHoleEnabled = drawHoleEnabled;
    }

    public void setHideTag(boolean hideTag) {
        this.hideTag = hideTag;
    }

    public void setChangeCenter(boolean changeCenter) {
        this.changeCenter = changeCenter;
    }

    public void setSliceSpace(float sliceSpace) {
        this.sliceSpace = sliceSpace;
    }

    public void setNumberSize(int numberSize) {
        this.numberSize = numberSize;
    }

    public void setNumberUnit(int numberUnit) {
        this.numberUnit = numberUnit;
    }

    public void setAllNumberUnit(String allNumberUnit) {
        this.allNumberUnit = allNumberUnit;
    }

    public void setAllUnitColor(int allUnitColor) {
        this.allUnitColor = allUnitColor;
    }

    public void setAllNumberDp(int allNumberDp) {
        this.allNumberDp = allNumberDp;
    }

    public void setAllTextDp(int allTextDp) {
        this.allTextDp = allTextDp;
    }

    public void setAllUnitDp(int allUnitDp) {
        this.allUnitDp = allUnitDp;
    }

    public void setHideRound(boolean hideRound) {
        this.hideRound = hideRound;
    }

    public void setRoundColor(int roundColor) {
        this.roundColor = roundColor;
    }

    public void setRoundWidth(int roundWidth) {
        this.roundWidth = roundWidth;
    }

    public void setNoDataText(String text){
        this.noDataText = text;
        if(noData!=null){
            noData.setText(text);
        }
    }



}
