/*
 * Created by inks on  2022/12/1 下午5:17
 * 
 */

package com.ink.chart.view;


import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;

import androidx.annotation.Nullable;


/**
 * <pre>
 *     author : inks
 *     time   : 2023/1/9
 *     desc   : 圆环进度条
 *     version: 1.0
 * </pre>
 */
public class XCircularOuterView extends View {
    private Context context;
    private int width;

    //圆环宽度
    private int ringWidth = 5;
    //圆环背景色
    private int bgColor = 0XFFE6F3FA;

    //进度颜色
    private int proColor = 0XFF00C13A;
    //开始角度
    private int startAngle = 270;
    //cap
    private Paint.Cap cap = Paint.Cap.ROUND;

    //数据0-100
    private float progress;

    //背景画笔
    private Paint bgPaint;
    //圆环画笔
    private Paint ringPaint;


    public XCircularOuterView(Context context) {
        this(context, null);
    }

    public XCircularOuterView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public XCircularOuterView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        this(context, attrs, defStyleAttr, 0);
    }

    public XCircularOuterView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        this.context = context;
        init(attrs);
    }

    public void init(AttributeSet attrs) {


        bgPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        bgPaint.setStrokeWidth(ringWidth);
        bgPaint.setStyle(Paint.Style.STROKE);
        bgPaint.setColor(bgColor);


        ringPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        ringPaint.setStrokeWidth(ringWidth);
        ringPaint.setStyle(Paint.Style.STROKE);
        ringPaint.setStrokeCap(cap);
        ringPaint.setColor(proColor);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int specHeightSize = MeasureSpec.getSize(heightMeasureSpec);//高
        int specWidthSize = MeasureSpec.getSize(widthMeasureSpec);
        width = Math.min(specHeightSize,specWidthSize);
        setMeasuredDimension(width, width);
    }


    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        RectF rectBlackBg = new RectF(ringWidth, ringWidth, width - ringWidth, width - ringWidth);
        canvas.drawArc(rectBlackBg, 0, 360, false, bgPaint);

        canvas.drawArc(rectBlackBg, startAngle, progress*360/100, false, ringPaint);

    }

    public int getRingWidth() {
        return ringWidth;
    }

    public void setRingWidth(int ringWidth) {
        this.ringWidth = ringWidth;
    }

    public int getBgColor() {
        return bgColor;
    }

    public void setBgColor(int bgColor) {
        this.bgColor = bgColor;
    }

    public int getProColor() {
        return proColor;
    }

    public void setProColor(int proColor) {
        this.proColor = proColor;
    }

    public int getStartAngle() {
        return startAngle;
    }

    public void setStartAngle(int startAngle) {
        this.startAngle = startAngle;
    }

    public Paint.Cap getCap() {
        return cap;
    }

    public void setCap(Paint.Cap cap) {
        this.cap = cap;
    }

    public float getProgress() {
        return progress;
    }

    public void setProgress(float progress) {
        this.progress = progress;
        init(null);
        invalidate();
    }




}