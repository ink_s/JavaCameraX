/*
 *  Created by inks on  2023 /2 /17
 *  
 */

package com.ink.chart.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;

import androidx.annotation.Nullable;

import com.ink.camera.R;


/**
 * 圆角背景view
 */

public class XRoundRectView extends View {

    private Paint mPaint; //画笔

    private int mRadius = 50; //半径

    private int color = 0XFFFF0000;

    int specHeightSize;
    int specWidthSize;


    public XRoundRectView(Context context) {
        this(context, null);
    }

    public XRoundRectView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public XRoundRectView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        this(context, attrs, defStyleAttr, 0);
    }

    public XRoundRectView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(attrs);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        specHeightSize = MeasureSpec.getSize(heightMeasureSpec);//高
        specWidthSize = MeasureSpec.getSize(widthMeasureSpec);
    }

    public void init(AttributeSet attrs) {
        if (attrs != null) {
            TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.XRoundRectView);
            mRadius = (int) typedArray.getDimension(R.styleable.XRoundRectView_rrwRadius, 50);
            color = typedArray.getColor(R.styleable.XRoundRectView_rrwColor, 0XFFFF0000);

            typedArray.recycle();
        }

        mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mPaint.setColor(color);
        mPaint.setStyle(Paint.Style.FILL);
    }


    @Override
    protected void onDraw(Canvas canvas) {
        mPaint.setColor(color);

        canvas.drawRoundRect(new RectF(0, 0, specWidthSize, specHeightSize), mRadius, mRadius, mPaint);

    }

    public void setRadius(int mRadius) {
        this.mRadius = mRadius;
        invalidate();
    }

    public void setColor(int color) {
        this.color = color;
        invalidate();
    }

    //drawable转BitMap的方法
    private Bitmap drawableToBitmap(Drawable drawable) {
        if (drawable instanceof BitmapDrawable) {
            BitmapDrawable bd = (BitmapDrawable) drawable;
            return bd.getBitmap();
        }
        if (drawable != null) {
            int w = drawable.getIntrinsicWidth();
            int h = drawable.getIntrinsicHeight();
            Bitmap bitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(bitmap);
            drawable.setBounds(0, 0, w, h);
            drawable.draw(canvas);
            return bitmap;
        } else {
            return null;
        }

    }
}