/*
 * Created by inks on  2022/11/16 上午9:42
 * 
 */

package com.ink.chart.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.widget.TextView;

import androidx.annotation.Nullable;

@SuppressLint("AppCompatCustomView")
public class XMarqueeTextView extends TextView {

    public XMarqueeTextView(Context context) {

        this(context,null);
    }

    public XMarqueeTextView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs,0);
    }

    public XMarqueeTextView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        this(context, attrs, defStyleAttr,0);
    }

    public XMarqueeTextView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        setEllipsize(TextUtils.TruncateAt.MARQUEE);
        setSingleLine(true);
        setMarqueeRepeatLimit(-1);
    }




    @Override
    public boolean isFocused() {
        return true;
    }
}

