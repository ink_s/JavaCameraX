package com.ink.record;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.view.Window;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;

import com.ink.camera.R;
import com.inks.inkslibrary.Utils.T;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;


/**
 * <pre>
 *     author : inks
 *     time   : 2022/7/18
 *     desc   : 调用系统录音
 *     version: 1.0
 * </pre>
 */
public class SystemRecordActivity extends AppCompatActivity {



    //设置保存的文件夹，不带文件名
    private String saveDir;

    public static String MAX_FILE_SIZE  = "MaxFileSize";
    //大小限制（KB）
    private long maxFileSize =200*1024;


    //回调
    ActivityResultLauncher<Intent> resultLauncher;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.x_activity_system_record);
        setupActivityResultLaunchers();
        saveDir = getIntent().getStringExtra("saveDir");
        maxFileSize = getIntent().getLongExtra(MAX_FILE_SIZE,200*1024);
        Intent intentRecord = new Intent();
        intentRecord.setAction(MediaStore.Audio.Media.RECORD_SOUND_ACTION);
        resultLauncher.launch(intentRecord);

    }


    /**
     * @throws
     * @Description: 设置回调监听
     * @param:
     * @return:
     * @date: 2022/7/18
     */
    private void setupActivityResultLaunchers() {
        resultLauncher = registerForActivityResult(
                new ActivityResultContracts.StartActivityForResult(),
                result -> {
                    if (result.getResultCode() == Activity.RESULT_OK) {
                        Intent data = result.getData();
                        if (data != null) {
                            Uri uri = data.getData();
                            if (uri != null) {
                                String srcPath = getAudioPathFromUri(uri);  //  从MediaStore查到音频文件存放位置

                                File file = new File(srcPath);
                                if(!file.exists() || !file.isFile()){
                                    finish();
                                    return;
                                }
                                if(file.length()>maxFileSize*1024){
                                    T.showShort(this, "文件大小超过限制！");
                                    finish();
                                    return;
                                }

                                if (!TextUtils.isEmpty(saveDir)) {
                                    File folder = new File(saveDir);
                                    if (!folder.exists() && !folder.isDirectory()) {
                                        folder.mkdirs();
                                    }
                                    String path = saveDir + File.separator + getFileName(srcPath);
                                    if (copyFile(new File(srcPath), new File(path))) {
                                        getIntent().putExtra("savePath", path);
                                    } else {
                                        getIntent().putExtra("savePath", srcPath);
                                    }


                                } else {
                                    getIntent().putExtra("savePath", srcPath);
                                }
                                setResult(RESULT_OK, getIntent());
                            }
                        }
                    }
                    finish();

                });

    }


    String getAudioPathFromUri(Uri uri) {
        //  根据uri，查询MediaStore
        Cursor cursor = this.getContentResolver().query(uri, new String[]{MediaStore.Audio.Media.DATA}, null, null, null);
        if (cursor != null) {
            cursor.moveToFirst();   //  取查询结果的第一条记录
            int index = cursor.getColumnIndex(MediaStore.Audio.Media.DATA);
            String path = cursor.getString(index);  //  取该记录的MediaStore.Audio.Media.DATA字段，即为文件路径
            return path;
        }
        return null;
    }


    public static String getFileName(String path) {
        try {
            File file = new File(path);
            return file.getName();
        } catch (Exception e) {
            return path;
        }

    }


    /**
     * 文件的复制操作方法
     *
     * @param fromFile 准备复制的文件
     * @param toFile   要复制的文件的目录
     */
    public static boolean copyFile(File fromFile, File toFile) {
        if (!fromFile.exists()) {
            return false;
        }
        if (!fromFile.isFile()) {
            return false;
        }
        if (!fromFile.canRead()) {
            return false;
        }
        if (!toFile.getParentFile().exists()) {
            toFile.getParentFile().mkdirs();
        }
        if (toFile.exists()) {
            toFile.delete();
        }
        try {
            FileInputStream fosfrom = new FileInputStream(fromFile);
            FileOutputStream fosto = new FileOutputStream(toFile);
            byte[] bt = new byte[1024];
            int c;
            while ((c = fosfrom.read(bt)) > 0) {
                fosto.write(bt, 0, c);
            }
            //关闭输入、输出流
            fosfrom.close();
            fosto.close();
            return true;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return false;

        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

}