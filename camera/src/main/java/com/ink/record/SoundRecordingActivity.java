package com.ink.record;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.ink.camera.R;
import com.inks.inkslibrary.Utils.ClickUtil;
import com.inks.inkslibrary.Utils.L;
import com.inks.inkslibrary.Utils.T;
import com.inks.inkslibrary.view.ClickEffectImageColorMatrix;
import com.inks.inkslibrary.view.ClickEffectLinearLayout;

import java.io.File;
import java.io.IOException;

import jaygoo.widget.wlv.WaveLineView;
import tech.oom.idealrecorder.IdealRecorder;
import tech.oom.idealrecorder.StatusListener;

public class SoundRecordingActivity extends AppCompatActivity {

    public static String RECORD_FILE_PATH  = "FilePath";

    public static String MAX_FILE_SIZE  = "MaxFileSize";
    //大小限制（KB）
    private long maxFileSize =200*1024;

    private ClickEffectLinearLayout playView;
    private ClickEffectLinearLayout doneView;
    private ClickEffectLinearLayout startOrStopView;
    private ClickEffectImageColorMatrix closeButton;

    private ImageView startOrStopImageView;
    private TextView startOrStopTextView;
    //计时
    private TextView recordTimeText;

    private WaveLineView waveLineView;

    //录音保存的文件地址（包含文件名）
    private String mFilePath;

    private IdealRecorder idealRecorder;

    private boolean isRecorder = false;

    //现在已经录制了的时长
    private int nowRecordTime = 0;

    public static final int CODE_UP_TIME = 100;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        setFinishOnTouchOutside(false);
        setContentView(R.layout.record_activity_sound_recording);
        IdealRecorder.getInstance().init(this);
        idealRecorder = IdealRecorder.getInstance();
        mFilePath = getIntent().getStringExtra(RECORD_FILE_PATH);
        maxFileSize = getIntent().getLongExtra(MAX_FILE_SIZE,200*1024);

        initView();
        initIdealRecorder();
    }


    private void initView() {
        playView = findViewById(R.id.play);
        doneView = findViewById(R.id.done);
        startOrStopView = findViewById(R.id.start_or_stop);
        closeButton = findViewById(R.id.close);
        startOrStopImageView = findViewById(R.id.start_or_stop_image);
        startOrStopTextView = findViewById(R.id.start_or_stop_text);
        recordTimeText = findViewById(R.id.record_time);
        waveLineView = findViewById(R.id.waveLineView);
        playView.setOnClickListener(click);
        doneView.setOnClickListener(click);
        startOrStopView.setOnClickListener(click);
        closeButton.setOnClickListener(click);

    }


    private void  initIdealRecorder(){
        IdealRecorder.RecordConfig recordConfig = new IdealRecorder.RecordConfig();
        idealRecorder.setRecordConfig(recordConfig).setMaxRecordTime(120*1000).setVolumeInterval(200);
        idealRecorder.setStatusListener(statusListener);
    }


    View.OnClickListener click = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (!ClickUtil.isFastDoubleClick((long) (200))) {
                if (v.getId() == R.id.close) {
                    finish();
                } else if (v.getId() == R.id.play) {

                }
                if (v.getId() == R.id.done) {


                    File file = new File(mFilePath);
                    if(!file.exists() || !file.isFile()){
                        finish();
                        return;
                    }
                    if(file.length()>maxFileSize*1024){
                        T.showShort(SoundRecordingActivity.this, "文件大小超过限制！");
                        finish();
                        return;
                    }


                    Intent intent =new Intent();
                    intent.putExtra(RECORD_FILE_PATH,mFilePath);
                    setResult(RESULT_OK,intent);
                    finish();

                }
                if (v.getId() == R.id.start_or_stop) {
                    startOrStopRecording();

                }
            }
        }
    };


    private void startOrStopRecording(){
        if(!isRecorder){
            setFileName();
            idealRecorder.setRecordFilePath(mFilePath);
            idealRecorder.start();
        }else{
            idealRecorder.stop();
        }
    }




    StatusListener statusListener = new StatusListener(){
        @Override
        public void onStartRecording() {
            super.onStartRecording();
            isRecorder = true;
            startOrStopImageView.setImageResource(R.drawable.record_music_stop_record);
            startOrStopTextView.setText("停止");
            nowRecordTime = 0;
            mHandler.removeMessages(CODE_UP_TIME);
            mHandler.sendEmptyMessage(CODE_UP_TIME);
            doneView.setVisibility(View.INVISIBLE);
            waveLineView.startAnim();
        }

        @Override
        public void onVoiceVolume(int volume) {
            super.onVoiceVolume(volume);
            waveLineView.setVolume(volume);
        }

        @Override
        public void onRecordError(int code, String errorMsg) {
            super.onRecordError(code, errorMsg);
            isRecorder = false;
            startOrStopImageView.setImageResource(R.drawable.record_music_start_record);
            startOrStopTextView.setText("开始");
            mHandler.removeMessages(CODE_UP_TIME);
            waveLineView.stopAnim();
        }

        @Override
        public void onFileSaveSuccess(String fileUri) {
            super.onFileSaveSuccess(fileUri);
            L.ee("保存地址，"+fileUri);
            doneView.setVisibility(View.VISIBLE);
        }

        @Override
        public void onStopRecording() {
            super.onStopRecording();
            isRecorder = false;
            startOrStopImageView.setImageResource(R.drawable.record_music_start_record);
            startOrStopTextView.setText("开始");
            mHandler.removeMessages(CODE_UP_TIME);
            waveLineView.stopAnim();
        }
    };











    // 设置录音文件的名字和保存路径
    public void setFileName() {

        if (TextUtils.isEmpty(mFilePath)) {
            String path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM) + File.separator + "CameraX";
            mFilePath = path + File.separator + System.currentTimeMillis() +"_rc"+ ".wav";
        }
        File file = new File(mFilePath);
        if (file.getParentFile() != null && !file.getParentFile().exists()) {
            file.getParentFile().mkdirs();
        }
        if (file.exists()) {
            file.delete();
        }
    }



    @SuppressLint("HandlerLeak")
    private Handler mHandler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (msg.what == CODE_UP_TIME) {

                recordTimeText.setText(millisecondsToTime(nowRecordTime));
                nowRecordTime = nowRecordTime+1000;
                if(isRecorder){
                    mHandler.removeMessages(CODE_UP_TIME);
                    mHandler.sendEmptyMessageDelayed(CODE_UP_TIME,1000);
                }

            }
        }
    };

    @Override
    protected void onResume() {
        super.onResume();
        waveLineView.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        waveLineView.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        waveLineView.release();
        mHandler.removeCallbacksAndMessages(null);
       if(isRecorder){
           idealRecorder.stop();
       }
    }


    private String millisecondsToTime(int t){
        return getTimeString(t / 3600000)+":"+getTimeString((t % 3600000)/60000)+":"+getTimeString((t % 60000 )/1000);
    }


    private  String getTimeString(int t){
        String m="";
        if(t>0){
            if(t<10){
                m="0"+t;
            }else{
                m=t+"";
            }
        }else{
            m="00";
        }
        return m;
    }







}