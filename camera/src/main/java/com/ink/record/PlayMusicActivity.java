package com.ink.record;

import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.exoplayer2.MediaItem;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.ProgressiveMediaSource;
import com.google.android.exoplayer2.ui.PlayerControlView;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.ink.camera.R;
import com.inks.inkslibrary.Utils.ClickUtil;

public class PlayMusicActivity extends AppCompatActivity {
    public static String PLAY_FILE_PATH  = "FilePath";
    private String playPath ;
    private PlayerControlView playerView;
    private SimpleExoPlayer player;
    private ImageView close;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        setFinishOnTouchOutside(false);
        setContentView(R.layout.exo_activity_play_music);
        playerView = findViewById(R.id.exo_player);

        close = findViewById(R.id.close);
        close.setOnClickListener(click);
        playPath = getIntent().getStringExtra(PLAY_FILE_PATH);
        initializePlayer();

    }

    View.OnClickListener click = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (!ClickUtil.isFastDoubleClick((long) (500))) {
                if (v.getId() == R.id.close) {
                    onBackPressed();
                }

            }
        }
    };





    private void initializePlayer() {
        if(TextUtils.isEmpty(playPath)){
            finish();
        }
        player = new SimpleExoPlayer.Builder(this).build();
        playerView.setPlayer(player);
        DataSource.Factory dataSourceFactory = new DefaultDataSourceFactory(this, Util.getUserAgent(this, "CameraX"));
        MediaSource mediaSource = new ProgressiveMediaSource.Factory(dataSourceFactory)
                .createMediaSource(MediaItem.fromUri(Uri.parse(playPath)) );
        player.prepare(mediaSource);
    }



    @Override
    protected void onDestroy() {
        super.onDestroy();
        player.release();
    }
}