package com.ink.download;

import com.google.gson.annotations.SerializedName;

/**
 * $
 *
 * @author fcp
 * @time 2022/1/25
 */
public class HttpResponse<T> {

    public int code;
    public String msg;
    @SerializedName(value = "data", alternate = {"rows","user"})
    public T data;
    public int total;

    public HttpResponse() {
    }

    public HttpResponse(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public HttpResponse(int code, String msg, T data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }


    public boolean isSuccess() {
        return code == 200;
    }




}
