package com.ink.download;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.webkit.MimeTypeMap;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;

import com.ink.camera.ExoPlayerActivity;
import com.ink.camera.ImagePreviewActivity;
import com.ink.camera.R;
import com.ink.camera.view.WaveView;
import com.ink.record.PlayMusicActivity;
import com.ink.util.InstallApk;
import com.ink.util.ZipUtils;
import com.inks.inkslibrary.Utils.ClickUtil;
import com.inks.inkslibrary.Utils.L;
import com.liulishuo.okdownload.DownloadTask;
import com.liulishuo.okdownload.SpeedCalculator;
import com.liulishuo.okdownload.core.Util;
import com.liulishuo.okdownload.core.breakpoint.BlockInfo;
import com.liulishuo.okdownload.core.breakpoint.BreakpointInfo;
import com.liulishuo.okdownload.core.cause.EndCause;
import com.liulishuo.okdownload.core.listener.DownloadListener4WithSpeed;
import com.liulishuo.okdownload.core.listener.assist.Listener4SpeedAssistExtend;

import java.io.File;
import java.util.List;
import java.util.Map;

public class BaseDownloadActivity extends AppCompatActivity {
    /**
     * 存储路径
     */
    public static String LOAD_FILE_SAVE_PARENT = "LoadFileSaveParent";
    /**
     * 下载地址
     */
    public static String LOAD_FILE_URL = "LoadFileUrl";
    /**
     * 文件名称
     */
    public static String LOAD_FILE_NAME = "LoadFileName";
    /**
     * 如果已存在，是否重新下
     */
    public static String LOAD_FILE_RE = "LoadFileRe";

    /**
     * 下载完成后是否打开
     */
    public static String LOAD_OPEN = "LoadOpen";


    /**
     * 解压缩路径
     */
    public static String LOAD_ZIP_PATH = "LoadZipPath";
    /**
     * 解压要删除的文件路径
     */
    public static String LOAD_ZIP_DELETE = "LoadZipDelete";

    //下载完成后是否显示文件名称
    public static String LOAD_SHOW_PATH = "LoadShowPath";


    private String parentPath;
    private String url;
    private String filename;
    private String zipPath;
    private String[] deleteFiles;
    private boolean loadRe = false;
    private boolean loadOpen = false;
    private boolean showPath = false;

    private boolean isApk = false;
    private String filePath;

    private LinearLayout tipsLayout;
    private TextView tipsView, backView;
    private RelativeLayout proLayout;
    private WaveView waveView;
    private TextView proText, sizeText, speedText;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        setFinishOnTouchOutside(false);
        setContentView(R.layout.x_activity_dialog_download);

        tipsLayout = findViewById(R.id.tips_layout);
        tipsView = findViewById(R.id.tips);
        backView = findViewById(R.id.back);
        proLayout = findViewById(R.id.pro_layout);
        waveView = findViewById(R.id.wave_view);
        proText = findViewById(R.id.pro_text);
        sizeText = findViewById(R.id.size_text);
        speedText = findViewById(R.id.speed_text);

        backView.setOnClickListener(click);

        tipsLayout.setVisibility(View.GONE);
        proLayout.setVisibility(View.VISIBLE);
        waveView.setPro(0);
        proText.setText("0");
        sizeText.setText("--/--");
        speedText.setText("");


        parentPath = getIntent().getStringExtra(LOAD_FILE_SAVE_PARENT);
        url = getIntent().getStringExtra(LOAD_FILE_URL);
        filename = getIntent().getStringExtra(LOAD_FILE_NAME);
        zipPath = getIntent().getStringExtra(LOAD_ZIP_PATH);
        deleteFiles = getIntent().getStringArrayExtra(LOAD_ZIP_DELETE);
        loadRe = getIntent().getBooleanExtra(LOAD_FILE_RE, false);
        loadOpen = getIntent().getBooleanExtra(LOAD_OPEN, false);
        showPath = getIntent().getBooleanExtra(LOAD_SHOW_PATH, false);
        if (TextUtils.isEmpty(parentPath) || TextUtils.isEmpty(url)) {
            return;
        }


        if (!url.startsWith("http")) {
            File file = new File(url);
            if (file.exists()) {
                //本地文件，复制
                boolean b = ZipUtils.copyFile(file, new File(parentPath, filename));
                if(b){
                    if (showPath) {
                        showMsg("已保存到 " + parentPath + " 文件夹下!");
                    } else {
                        showMsg("下载完成！");
                    }
                }else{
                    showMsg("下载失败！");
                }


            } else {
                startTask();
            }

            return;
        }


        startTask();

    }


    private void startTask() {

        DownloadTask task = createTasK(url, parentPath, filename);

        final long[] totalLength = new long[1];
        final String[] readableTotalLength = new String[1];

        task.enqueue(new DownloadListener4WithSpeed() {
            @Override
            public void taskStart(@NonNull DownloadTask task) {
                L.ee("taskStart");

            }

            @Override
            public void connectStart(@NonNull DownloadTask task, int blockIndex, @NonNull Map<String, List<String>> requestHeaderFields) {

            }

            @Override
            public void connectEnd(@NonNull DownloadTask task, int blockIndex, int responseCode, @NonNull Map<String, List<String>> responseHeaderFields) {

            }

            @Override
            public void infoReady(@NonNull DownloadTask task, @NonNull BreakpointInfo info, boolean fromBreakpoint, @NonNull Listener4SpeedAssistExtend.Listener4SpeedModel model) {
                totalLength[0] = info.getTotalLength();
                readableTotalLength[0] = Util.humanReadableBytes(totalLength[0], true);
                float pro = (float) info.getTotalOffset() / info.getTotalLength();

                L.ee("【2、infoReady】当前进度" + (float) pro * 100 + "%" + "，" + info.toString());


                waveView.setPro(pro);
                proText.setText((int) Math.floor(pro * 100) + "");
                sizeText.setText("--/" + readableTotalLength[0]);
                speedText.setText("");

            }

            @Override
            public void progressBlock(@NonNull DownloadTask task, int blockIndex, long currentBlockOffset, @NonNull SpeedCalculator blockSpeed) {

            }

            @Override
            public void progress(@NonNull DownloadTask task, long currentOffset, @NonNull SpeedCalculator taskSpeed) {

                String readableOffset = Util.humanReadableBytes(currentOffset, true);
                String progressStatus = readableOffset + "/" + readableTotalLength[0];
                String speed = taskSpeed.speed();
                float percent = (float) currentOffset / totalLength[0];
                L.ee("【6、progress】" + currentOffset + "[" + progressStatus + "]，速度：" + speed + "，进度：" + percent * 100 + "%");

                waveView.setPro(percent);
                proText.setText((int) Math.floor(percent * 100) + "");
                sizeText.setText(readableOffset + "/" + readableTotalLength[0]);
                speedText.setText(speed);

            }

            @Override
            public void blockEnd(@NonNull DownloadTask task, int blockIndex, BlockInfo info, @NonNull SpeedCalculator blockSpeed) {

            }

            @Override
            public void taskEnd(@NonNull DownloadTask task, @NonNull EndCause cause, @Nullable Exception realCause, @NonNull SpeedCalculator taskSpeed) {

                L.ee("【8、taskEnd】" + cause.name() + "：" + (realCause != null ? realCause.getMessage() : "无异常"));

                if (cause == EndCause.COMPLETED) {

                    if (loadOpen) {
                        if (filename.endsWith(".apk")) {

                            filePath = task.getFile().getPath();

                            showMsg("下载完成！", false, true);
                            return;
                        }

                        try {
                            preview(task.getFile().getPath(), BaseDownloadActivity.this);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        finish();

                    } else {

                        if (!TextUtils.isEmpty(zipPath)) {
                            showMsg("正在解压中,请稍后...", true);
                            unZip(zipPath, task.getFile().getPath(), deleteFiles);

                        } else {
                            if (showPath) {
                                showMsg("已保存到 " + parentPath + " 文件夹下!");
                            } else {
                                showMsg("下载完成！");
                            }

                        }


                    }


                } else {
                    showMsg("下载失败！");
                }


            }
        });


    }


    private void unZip(String srcFilePath, String zipFilePath, String[] deleteFiles) {
        try {
            L.e("开始解压");

            //zipFilePath =   zipFilePath.replace(".apk",".zip");

            File pathFile = new File(srcFilePath);
            if (!pathFile.exists()) {
                pathFile.mkdirs();
            }

            L.e(zipFilePath, srcFilePath);
            if (ZipUtils.unzip(zipFilePath, srcFilePath)) {
                deleteFilePath(zipFilePath);
                L.e("解压成功");
                //删除旧数据
                if (deleteFiles != null && deleteFiles.length > 0) {
                    for (String file : deleteFiles) {
                        deleteFilePath(file);
                    }
                }

                showMsg("解压完成!");
            } else {
                L.e("解压失败");
                deleteFilePath(zipFilePath);
                showMsg("解压失败！");

            }


        } catch (Exception e) {
            e.printStackTrace();
            L.e("解压失败");
            deleteFilePath(zipFilePath);
            showMsg("解压失败！");
        }
    }

    /**
     * 创建下载任务实例
     *
     * @param url
     * @param parentPath
     * @param fileName
     * @return
     */
    public DownloadTask createTasK(String url, String parentPath, String fileName) {

        return new DownloadTask.Builder(url, parentPath, fileName)
                .setFilenameFromResponse(false)//是否使用 response header or url path 作为文件名，此时会忽略指定的文件名，默认false
                .setPassIfAlreadyCompleted(!loadRe)//如果文件已经下载完成，再次下载时，是否忽略下载，默认为true(忽略)，设为false会从头下载
                .setConnectionCount(1)  //需要用几个线程来下载文件，默认根据文件大小确定；如果文件已经 split block，则设置后无效
                .setPreAllocateLength(false) //在获取资源长度后，设置是否需要为文件预分配长度，默认false
                .setMinIntervalMillisCallbackProcess(1000) //通知调用者的频率，避免anr，默认3000
                .setWifiRequired(false)//是否只允许wifi下载，默认为false
                .setAutoCallbackToUIThread(true) //是否在主线程通知调用者，默认为true
                //.setHeaderMapFields(new HashMap<String, List<String>>())//设置请求头
                //.addHeader(String key, String value)//追加请求头
                .setPriority(0)//设置优先级，默认值是0，值越大下载优先级越高
                .setReadBufferSize(4096)//设置读取缓存区大小，默认4096
                .setFlushBufferSize(16384)//设置写入缓存区大小，默认16384
                .setSyncBufferSize(65536)//写入到文件的缓冲区大小，默认65536
                .setSyncBufferIntervalMillis(2000) //写入文件的最小时间间隔，默认2000
                .build();

    }


    View.OnClickListener click = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (!ClickUtil.isFastDoubleClick((long) (500))) {
                if (v.getId() == R.id.back) {
                    if (isApk) {

                        InstallApk.installApkO(BaseDownloadActivity.this, filePath);
                        //  finish();
                    } else {
                        finish();
                    }


                }
            }
        }
    };


    private void showMsg(String msg) {
        showMsg(msg, false);
    }

    private void showMsg(String msg, boolean hideButton) {
        showMsg(msg, hideButton, false);
    }

    private void showMsg(String msg, boolean hideButton, boolean apk) {
        tipsLayout.setVisibility(View.VISIBLE);
        proLayout.setVisibility(View.GONE);
        tipsView.setText(msg);
        backView.setVisibility(hideButton ? View.GONE : View.VISIBLE);

        if (apk) {
            backView.setText("安装");
            isApk = true;
        } else {
            backView.setText("返回");
            isApk = false;
        }
    }


    private void deleteFilePath(String path) {

        File file = new File(path);

        if (!file.exists()) return;

        if (!file.isFile() && file.list() != null) {
            File[] files = file.listFiles();
            for (File a : files) {
                deleteFile(a.getPath());
            }
        }
        file.delete();

    }

    //预览文件
    public static void preview(String filePath, Context context) {
        if (TextUtils.isEmpty(filePath)) {
            return;
        }

        if (isVideo(filePath)) {
            //视频
            Intent intent = new Intent(context, ExoPlayerActivity.class);
            intent.putExtra("playPath", filePath);
            context.startActivity(intent);
        } else if (isAudio(filePath)) {
            //音乐
            Intent intent = new Intent(context, PlayMusicActivity.class);
            intent.putExtra(PlayMusicActivity.PLAY_FILE_PATH, filePath);
            context.startActivity(intent);

        } else if (isImage(filePath)) {
            //图片
            Intent intent = new Intent(context, ImagePreviewActivity.class);
            intent.putExtra("imageUrl", filePath);
            context.startActivity(intent);
        } else {
            L.ee(filePath);

            if (filePath.startsWith("http")) {
                Uri uri = Uri.parse(filePath);
                Intent intent = new Intent();
                intent.setAction("android.intent.action.VIEW");
                intent.setData(uri);
                Intent chooserIntent = Intent.createChooser(intent, "查看");
                chooserIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);//设置标记
                context.startActivity(chooserIntent);
            } else {
                openLocalFile(filePath, context);
            }


        }

    }


    public static boolean isVideo(String path) {
        if (TextUtils.isEmpty(path)) {
            return false;
        }
        String extensionName = getExtensionName(path);
        String[] types = new String[]{"3gp", "mp4", "mpeg", "avi", "mov"};
        if (TextUtils.isEmpty(extensionName)) {
            return false;
        } else {
            for (String type : types) {
                if (extensionName.equalsIgnoreCase(type)) {
                    return true;
                }
            }
        }
        return false;
    }

    public static boolean isAudio(String path) {
        if (TextUtils.isEmpty(path)) {
            return false;
        }
        String extensionName = getExtensionName(path);
        String[] types = new String[]{"mp3", "wav", "amr"};
        if (TextUtils.isEmpty(extensionName)) {
            return false;
        } else {
            for (String type : types) {
                if (extensionName.equalsIgnoreCase(type)) {
                    return true;
                }
            }
        }
        return false;
    }


    public static boolean isImage(String filename) {
        String extensionName = getExtensionName(filename);
        String[] types = new String[]{"image", "png", "jpg", "jpeg"};
        if (TextUtils.isEmpty(extensionName)) {
            return false;
        } else {
            for (String type : types) {
                if (extensionName.equalsIgnoreCase(type)) {
                    return true;
                }
            }
        }
        return false;
    }

    public static String getExtensionName(String filename) {
        if ((filename != null) && (filename.length() > 0)) {
            int dot = filename.lastIndexOf('.');
            if ((dot > -1) && (dot < (filename.length() - 1))) {
                return filename.substring(dot + 1);
            }
        }
        return "";
    }


    public static void openLocalFile(String path, Context context) {
        Intent intent = new Intent();
        File file = new File(path);

        if (!file.exists()) {
            return;
        }


        intent.setAction(Intent.ACTION_VIEW);//动作，查看
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            Uri uri = FileProvider.getUriForFile(context, context.getPackageName() + ".fileProvider", file);
            intent.setDataAndType(uri, getMimeType(path));
        } else {
            intent.setDataAndType(Uri.fromFile(file), getMimeType(path));
        }
        L.ee(getMimeType(path));
        Intent chooserIntent = Intent.createChooser(intent, "查看");
        chooserIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);//设置标记
        context.startActivity(chooserIntent);
        // startActivity(intent);
    }


    public static String getMimeType(String filePath) {
        String mime = "*/*";
        //使用系统API，获取URL路径中文件的后缀名（扩展名）

        //不支持中文
        //String extension = MimeTypeMap.getFileExtensionFromUrl(filePath);

        String extension = parseFormat2(filePath);
        L.ee(filePath, extension);
        if (!TextUtils.isEmpty(extension)) {
            //使用系统API，获取MimeTypeMap的单例实例，然后调用其内部方法获取文件后缀名（扩展名）所对应的MIME类型
            mime = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension.toLowerCase());
        }
        return mime;
    }


    //获取后缀名,不带点
    public static String parseFormat2(String filepath) {
        return filepath.substring(filepath.lastIndexOf(".") + 1);
    }


    @Override
    public void onBackPressed() {
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 10086) {
            L.i("设置了安装未知应用后的回调。。。");

            InstallApk.installApkO(BaseDownloadActivity.this, filePath);
        }
    }


}