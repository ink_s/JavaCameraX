package com.ink.download;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.os.Environment;
import android.text.TextUtils;

import androidx.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.inks.inkslibrary.Utils.L;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Type;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Iterator;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * <pre>
 *     author : inks
 *     time   : 2024/02/05
 *     desc   :
 *     version: 1.0
 * </pre>
 */
public class UpData {
    private Context mContext;
    private HashMap<String, String> params;
    private String mUrl;
    private boolean toast;
    private String mCurrentVersionCode;
    private String baseUrl;
    private String downloadBaseUrl;
    private String appPath;
    private boolean zip;
    private String zipPath;
    private String[] deleteFiles;


    public interface BackToastListener {
        void onBackToast(String msg);
    }

    private BackToastListener mListener;

    public static UpData get(Application application) {
        return   new UpData(application);
    }

    private UpData(Context context) {
        this.mContext = context;
    }

    public UpData setParams(HashMap<String, String> params) {
        this.params = params;
        return this;
    }

    public UpData setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
        return this;
    }

    public UpData setDownloadBaseUrl(String downloadBaseUrl) {
        this.downloadBaseUrl = downloadBaseUrl;
        return this;
    }

    public UpData setUrl(String mUrl) {
        this.mUrl = mUrl;
        return this;
    }

    public UpData setToast(boolean toast) {
        this.toast = toast;
        return this;
    }

    public UpData setAppPath(String appPath) {
        this.appPath = appPath;
        return this;
    }

    public UpData setCurrentVersionCode(String mCurrentVersionCode) {
        this.mCurrentVersionCode = mCurrentVersionCode;
        return this;
    }

    public UpData setListener(BackToastListener mListener) {
        this.mListener = mListener;
        return this;
    }

    public UpData setZip(boolean zip,String zipPath,String[] deleteFiles) {
        this.zip = zip;
        this.zipPath = zipPath;
        this.deleteFiles = deleteFiles;
        return this;
    }

    /**
     * 为HttpGet 的 url 方便的添加多个name value 参数。
     * @param url
     * @param params
     * @return
     */
    public static String attachHttpGetParams(String url, HashMap<String,String> params){

        Iterator<String> keys = params.keySet().iterator();
        Iterator<String> values = params.values().iterator();
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("?");

        for (int i=0;i<params.size();i++ ) {
            String value=null;
            try {
                value= URLEncoder.encode(values.next(),"utf-8");
            }catch (Exception e){
                e.printStackTrace();
            }

            stringBuffer.append(keys.next()+"="+value);
            if (i!=params.size()-1) {
                stringBuffer.append("&");
            }
        }

        return url + stringBuffer.toString();
    }

    public void start() {
        OkHttpClient client = new OkHttpClient();

        Request request = new Request.Builder()
                .url(attachHttpGetParams(baseUrl+ mUrl,params))
                .get()
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(@NonNull Call call, @NonNull IOException e) {
                if(toast && mListener!=null){
                    mListener.onBackToast("获取信息失败");
                }
            }

            @Override
            public void onResponse(@NonNull Call call, @NonNull Response response) {

                try {
                    if (response.body() != null) {
                        String json = response.body().string();
                        if (TextUtils.isEmpty(json)) {
                            return ;
                        }

                        Gson gson = new Gson();
                        Type type = new TypeToken<HttpResponse<AppVersion>>() {
                        }.getType();
                        HttpResponse<AppVersion> httpResponse = gson.fromJson(json, type);


                        if (httpResponse.isSuccess() && httpResponse.data != null && httpResponse.data.versionNumber != null) {
                            AppVersion onlineAppInfo = httpResponse.data;

                            if (TextUtils.isEmpty(onlineAppInfo.app)) {
                                if(toast && mListener!=null){
                                    mListener.onBackToast("获取信息失败");
                                }
                                return ;
                            }
                            if (TextUtils.isEmpty(onlineAppInfo.versionNumber)) {
                                if(toast && mListener!=null){
                                    mListener.onBackToast("获取信息失败");
                                }
                                return ;
                            }

                            String versionNumber = onlineAppInfo.versionNumber
                                    .replace(".", "")
                                    .replace(" ", "")
                                    .replace("v","")
                                    .replace("V","");

                            String locationNumber = mCurrentVersionCode
                                    .replace(".", "")
                                    .replace(" ", "")
                                    .replace("v","")
                                    .replace("V","");
                            long newVersion;
                            long locationVersion;

                            try {
                                newVersion = Long.parseLong(versionNumber);
                                locationVersion = Long.parseLong(locationNumber);
                            } catch (Exception | Error e) {
                                e.printStackTrace();
                                return ;
                            }

                            if(newVersion>locationVersion){

                                L.ee("需要下载新版本");

                                if(zip){

                                    Intent intent = new Intent(mContext, BaseDownloadActivity.class);
                                    intent.putExtra(BaseDownloadActivity.LOAD_FILE_URL, downloadBaseUrl+onlineAppInfo.app);
                                    intent.putExtra(BaseDownloadActivity.LOAD_FILE_NAME, onlineAppInfo.versionNumber+".zip");
                                    intent.putExtra(BaseDownloadActivity.LOAD_FILE_SAVE_PARENT, appPath);
                                    intent.putExtra(BaseDownloadActivity.LOAD_OPEN, false);
                                    intent.putExtra(BaseDownloadActivity.LOAD_FILE_RE, false);
                                    intent.putExtra(BaseDownloadActivity.LOAD_ZIP_PATH, zipPath+"/"+onlineAppInfo.versionNumber);
                                    intent.putExtra(BaseDownloadActivity.LOAD_ZIP_DELETE, deleteFiles);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    mContext.startActivity(intent);
                                }else{
                                    Intent intent = new Intent(mContext, AppDownloadActivity.class);
                                    intent.putExtra(AppDownloadActivity.APP_URL, onlineAppInfo.app);
                                    intent.putExtra(AppDownloadActivity.APP_BASEURL, downloadBaseUrl);
                                    intent.putExtra(AppDownloadActivity.APP_VERSION, onlineAppInfo.versionNumber);
                                    intent.putExtra(AppDownloadActivity.APP_CONTENT, onlineAppInfo.updateContent);
                                    intent.putExtra(AppDownloadActivity.APP_PATH, appPath);
                                    intent.putExtra(AppDownloadActivity.APP_CLOSE_ENABLE, !"1".equalsIgnoreCase(onlineAppInfo.isForceUpdate));
                                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    mContext.startActivity(intent);
                                }





                            }else{
                                if(toast && mListener!=null){
                                    mListener.onBackToast("当前已是最新版本");
                                }
                            }

                        }else{
                            if(toast && mListener!=null){
                                mListener.onBackToast("获取信息失败");
                            }
                        }



                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }


            }
        });

    }

}
