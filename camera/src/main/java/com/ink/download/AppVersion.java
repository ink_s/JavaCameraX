package com.ink.download;

/**
 * $
 *
 * @author fcp
 * @time 2022/4/11
 */
public class AppVersion {


    public long id;
    public String versionNumber;
    public String isForceUpdate;
    public String updateTitle;
    public String updateContent;
    public String versionName;
    public long applyId;
    public String app;
    public String versionType;
    public String applyName;
}
