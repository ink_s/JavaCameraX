package com.ink.util;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;

import androidx.activity.ComponentActivity;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;

import com.ink.download.AppDownloadActivity;
import com.inks.inkslibrary.Utils.L;

import java.io.File;

/**
 * <pre>
 *     author : inks
 *     time   : 2024/02/05
 *     desc   :
 *     version: 1.0
 * </pre>
 */
public class InstallApk {

    public static void installApkO(ComponentActivity activity, String filePath) {
        String   downloadApkPath = filePath;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            //是否有安装位置来源的权限
            boolean haveInstallPermission = activity.getPackageManager().canRequestPackageInstalls();
            if (haveInstallPermission) {
                L.i("8.0手机已经拥有安装未知来源应用的权限，直接安装！");
                installApk(activity, downloadApkPath);
            } else {
                AlertDialog alertDialog= new AlertDialog.Builder(activity)
                        .setTitle("提示")
                        .setMessage("安装应用需要打开安装未知来源应用权限，请去设置中开启权限")
                        .setPositiveButton("确定", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                Uri packageUri = Uri.parse("package:"+ activity.getPackageName());
                                Intent intent = new Intent(Settings.ACTION_MANAGE_UNKNOWN_APP_SOURCES,packageUri);
                                activity.startActivityForResult(intent,10086);
                            }
                        })

                        .setCancelable(false)
                        .create();
                alertDialog.show();
            }
        } else {
            installApk(activity, downloadApkPath);
        }
    }



    public static   void installApk(Activity context, String downloadApk) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        File file = new File(downloadApk);
        L.i("安装路径=="+downloadApk);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            Uri apkUri = FileProvider.getUriForFile(context, context.getPackageName()+".fileProvider", file);
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            intent.setDataAndType(apkUri, "application/vnd.android.package-archive");
        } else {
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            Uri uri = Uri.fromFile(file);
            intent.setDataAndType(uri, "application/vnd.android.package-archive");
        }
        context.startActivity(intent);

        context.finish();
        // ActivityManager.getInstance().finishAllActivity();

    }


}
