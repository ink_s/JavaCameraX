/*
 * Created by inks on  2024/4/10 上午10:02
 * Copyright (c) 2022 北京天恒昕业科技发展有限公司. All rights reserved.
 */

package com.ink.util;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.view.View;
import android.view.animation.OvershootInterpolator;

import com.inks.inkslibrary.Utils.L;

import java.util.ArrayList;
import java.util.List;

/**
 * 翻转动画
 */
public class AnimRotateUtil {
    public enum Direction {
        LEFT_RIGHT, RIGHT_LEFT, TOP_BOTTOM, BOTTOM_TOP
    }

    public static void FlipAnimatorXViewShow(View oldView, View newView, long time, Direction direction) {
        FlipAnimatorXViewShow(oldView, newView, time, direction,1920);
    }

    public static void FlipAnimatorXViewShow(View oldView, View newView, long time, Direction direction,int distance) {
        if (direction == null) {
            direction = Direction.LEFT_RIGHT;
        }

        float scale = oldView.getResources().getDisplayMetrics().density * distance;
        oldView.setCameraDistance(scale);
        newView.setCameraDistance(scale);

        ObjectAnimator animator1;
        ObjectAnimator animator2;
        switch (direction) {
            case LEFT_RIGHT:
                animator1 = ObjectAnimator.ofFloat(oldView, "rotationY", 0, 90);
                animator2 = ObjectAnimator.ofFloat(newView, "rotationY", -90, 0);
                break;
            case RIGHT_LEFT:
                animator1 = ObjectAnimator.ofFloat(oldView, "rotationY", 0, -90);
                animator2 = ObjectAnimator.ofFloat(newView, "rotationY", 90, 0);
                break;
            case TOP_BOTTOM:
                animator1 = ObjectAnimator.ofFloat(oldView, "rotationX", 0, 90);
                animator2 = ObjectAnimator.ofFloat(newView, "rotationX", -90, 0);
                break;
            case BOTTOM_TOP:
            default:
                animator1 = ObjectAnimator.ofFloat(oldView, "rotationX", 0, -90);
                animator2 = ObjectAnimator.ofFloat(newView, "rotationX", 90, 0);
                break;
        }


        animator2.setInterpolator(new OvershootInterpolator(1.0f));

        animator1.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                oldView.setVisibility(View.GONE);
                animator2.setDuration(time).start();
                newView.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
        animator1.setDuration(time).start();
    }






}