package com.ink.util;

import android.graphics.drawable.GradientDrawable;

/**
 * <pre>
 *     author : inks
 *     time   : 2024/04/10
 *     desc   :
 *     version: 1.0
 * </pre>
 */
public class  GradientDrawableUtil {

    public static GradientDrawable getGradientDrawable(int color){
        return  getGradientDrawable(color, 5);
    }

    public static GradientDrawable getGradientDrawable(int color,float radii){
        return getGradientDrawable(new int[]{color,color},
                new float[]{radii,radii,radii,radii,radii,radii,radii,radii},
                GradientDrawable.RECTANGLE,GradientDrawable.LINEAR_GRADIENT,
                GradientDrawable.Orientation.LEFT_RIGHT);
    }


    public static GradientDrawable getGradientDrawable(int[] colors, float[] radii,
                                                       int shape,int type,
                                                       GradientDrawable.Orientation orientation ) {

        //背景色及圆角
        GradientDrawable drawable = new GradientDrawable();
        //形状（矩形）
        drawable.setShape(shape);
        //渐变样式
        drawable.setGradientType(type);
        //渐变方向（左到右）
        drawable.setOrientation(orientation);
        //圆角
        drawable.setCornerRadii(radii);
        //颜色
        drawable.setColors(colors);

        return drawable;

    }
}
