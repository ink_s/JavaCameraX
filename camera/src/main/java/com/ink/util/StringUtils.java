
package com.ink.util;

import android.annotation.SuppressLint;
import android.text.TextUtils;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * <pre>
 *     author : inks
 *     time   : 2023/01/12
 *     desc   :String Utils
 *     version: 1.0
 * </pre>
 */
public class StringUtils {

    //经费格式化  万元，2位小数
    public static String fundsFormat(String funds) {
        if (TextUtils.isEmpty(funds)) {
            return "";
        }
        try {
            float f = Float.parseFloat(funds);

            if (f < 100) {
                return String.format("%.2f", f) + "元";
            }

            return String.format("%.2f", f / 10000) + "万元";
        } catch (Exception exception) {
            exception.printStackTrace();
        }
        return funds;
    }


    //经费格式化  万元，2位小数
    public static String fundsFormatNoText(String funds) {
        if (TextUtils.isEmpty(funds)) {
            return "";
        }
        try {
            float f = Float.parseFloat(funds);
            return String.format("%.2f", f / 10000);
        } catch (Exception exception) {
            exception.printStackTrace();
        }
        return funds;
    }


    //小数格式化，保留2位小数
    public static String percentageFormat(String percentage) {
        if (TextUtils.isEmpty(percentage)) {
            return "0.00";
        }
        try {
            float f = Float.parseFloat(percentage);
            return String.format("%.2f", f);
        } catch (Exception exception) {
            exception.printStackTrace();
        }
        return percentage;
    }


    public static String percentage(String f, String total) {
        try {
            float f1 = Float.parseFloat(f);
            float f2 = Float.parseFloat(total);
            return percentage(f1, f2);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return "0.00";
    }


    public static String formatDouble(String f, int scale) {
        return formatStr(f, scale, false);
//        try {
//            String[] split = f.split("\\.");
//            if(split!=null && split.length==2 && split[split.length-1].length()>scale){
//                return split[0]+"."+split[1].substring(0, scale);
//            }else{
//                return f;
//            }
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return f;
    }


    @SuppressLint("DefaultLocale")
    public static String percentage(float f, float total) {
        if (total == 0) {
            return "0.00";
        }

        return String.format("%.2f", f / total * 100);

    }


    public static String stripTrailingZeros(String s) {
        if (TextUtils.isEmpty(s)) {
            return "";
        }
        try {
            BigDecimal value = new BigDecimal(s);
            BigDecimal noZeros = value.stripTrailingZeros();
            //  L.ee(noZeros.toPlainString());
            return noZeros.toPlainString();
        } catch (Exception e) {
            return s;
        }
    }


    public static String formatStr(double d, int scale, boolean tz) {
        String dStr = String.format("%.0" + scale + "f", d);
        if (tz) {
            //  L.ee(dStr);

            return stripTrailingZeros(dStr);
        } else {
            return dStr;
        }

    }

    //小数转整数
    public static String formatStr(String str, int scale, boolean tz) {
        if (TextUtils.isEmpty(str)) {
            str = "0";
        }
        try {
            double d = Double.parseDouble(str);

            return formatStr(d, scale, tz);

        } catch (Exception exception) {
            exception.printStackTrace();
        }
        return str;
    }



    //小数转整数,大于一万后返回完
    public static String formatStrTenThousand(String str, int scale, boolean tz) {
        if (TextUtils.isEmpty(str)) {
            str = "0";
        }
        try {
            double d = Double.parseDouble(str);
            if(d>=10000){
                return formatStr(d/10000d, 2, false)+"万";
            }else{
                return formatStr(d, scale, tz);
            }



        } catch (Exception exception) {
            exception.printStackTrace();
        }
        return str;
    }


    public static String formatStr(String d, int scale, boolean tz,boolean split) {
        if(split){
            return formatSplitStr(d,scale,tz);
        }else{
            return formatStr(d,scale,tz);
        }

    }



    //带逗号分割符的
    public static String formatSplitStr(String str, int scale, boolean tz) {
        if (TextUtils.isEmpty(str)) {
            str = "0";
        }
        try {
            double d = Double.parseDouble(str);
            NumberFormat numberFormat = NumberFormat.getInstance();
            numberFormat.setMaximumFractionDigits(scale); // 设置小数点后保留两位
            numberFormat.setMinimumFractionDigits(tz ? 0 :scale); // 设置小数点后至少两位
            // 格式化数字
            return numberFormat.format(d);
        } catch (Exception exception) {
            exception.printStackTrace();
        }
        return str;
    }

    //大于一万后返回万
    public static String formatSplitStrTenThousand(String str, int scale, boolean tz) {
        if (TextUtils.isEmpty(str)) {
            str = "0";
        }
        try {
            double d = Double.parseDouble(str);
            NumberFormat numberFormat = NumberFormat.getInstance();
            // 格式化数字
            if(d>=10000){
                numberFormat.setMaximumFractionDigits(2); // 设置小数点后保留两位
                numberFormat.setMinimumFractionDigits(2); // 设置小数点后至少两位
                return numberFormat.format(d/10000D)+"万";
            }else{
                numberFormat.setMaximumFractionDigits(scale); // 设置小数点后保留两位
                numberFormat.setMinimumFractionDigits(tz ? 0 :scale); // 设置小数点后至少两位
                return numberFormat.format(d);
            }

        } catch (Exception exception) {
            exception.printStackTrace();
        }
        return str;
    }




    //小数转整数
    public static String formatStrZeroEmpty(String str, int scale, boolean tz) {
        if (TextUtils.isEmpty(str)) {
            str = "--";
        }
        try {
            double d = Double.parseDouble(str);
            if(d==0){
                return "--";
            }

            return formatStr(d, scale, tz);

        } catch (Exception exception) {
            exception.printStackTrace();
        }
        return str;
    }



    //小数转整数
    public static String formatInt(String str) {
        if (TextUtils.isEmpty(str)) {
            return "";
        }
        try {
            return (int) Double.parseDouble(str) + "";

        } catch (Exception exception) {
            exception.printStackTrace();
        }
        return str;
    }

    //小数转整数
    public static String formatInt(double str) {
        return (int) str + "";
    }

    //小数转整数
    public static String formatInt(float str) {
        return String.format("%.0f", str);
    }


    public static boolean sameIds(String id1, String id2) {
        try {
            if (TextUtils.isEmpty(id1) || TextUtils.isEmpty(id2)) {
                return false;
            }

            if (id1.equals(id2)) {
                return true;
            }
            List<String> ids1 = Arrays.asList(id1.split(","));
            List<String> ids2 = Arrays.asList(id2.split(","));
            Collections.sort(ids1);
            Collections.sort(ids2);
            if (ids1.size() != ids2.size()) {
                return false;
            }

            for (int i = 0; i < ids1.size(); i++) {
                if (!ids1.get(i).equals(ids2.get(i)))
                    return false;
            }
            return true;
        } catch (Exception e) {
            return false;
        }


    }


    public static String Str2Null(String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        return str;
    }


    public static String Str2NullStr(String str) {
        if (TextUtils.isEmpty(str)) {
            return "";
        }
        return str;
    }

}
