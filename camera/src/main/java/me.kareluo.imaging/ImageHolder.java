package me.kareluo.imaging;

import android.graphics.Bitmap;

import java.util.concurrent.locks.ReentrantLock;

 class ImageHolder {
    private static ImageHolder instance;
    private Bitmap bitmap;
    private String imagePath;
    private Bitmap editedBitmap;
    //是否删除原图
    private boolean deleteOld;
    //是否是Launcher 启动
    private boolean isLauncher;
    private TRSPictureEditor.EditListener editListener;
    private final static ReentrantLock lock = new ReentrantLock();

    private ImageHolder() {
    }

    public static ImageHolder getInstance() {
        if (instance == null) {
            lock.lock();
            try {
                if (instance == null) {
                    instance = new ImageHolder();
                }
            } finally {
                lock.unlock();
            }
        }
        return instance;
    }

    public Bitmap getBitmap() {
        return bitmap;
    }

    public void setBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;
    }

     public String getImagePath() {
         return imagePath;
     }

     public void setImagePath(String imagePath) {
         this.imagePath = imagePath;
     }

     public boolean isDeleteOld() {
         return deleteOld;
     }

     public void setDeleteOld(boolean deleteOld) {
         this.deleteOld = deleteOld;
     }

     public boolean isLauncher() {
         return isLauncher;
     }

     public void setLauncher(boolean launcher) {
         isLauncher = launcher;
     }

     public void setEditedBitmap(Bitmap editedBitmap) {
        this.editedBitmap = editedBitmap;
        if (editListener != null) {
            editListener.onComplete(editedBitmap,imagePath);
        }
    }

    public TRSPictureEditor.EditListener getEditListener() {
        return editListener;
    }

    public void setEditListener(TRSPictureEditor.EditListener editListener) {
        this.editListener = editListener;
    }

    public void reset() {
        this.bitmap = null;
        this.editedBitmap = null;
        this.editListener=null;
    }
}